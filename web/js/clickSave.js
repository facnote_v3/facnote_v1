var _lastValue = '';
var _currentValue = '';

function getEditable(_obj, _url, _urlSelect) {
	$(_obj).unbind('click');
	$(_obj).unbind('dblclick');

	_lastValue = $.trim($(_obj).html());

	var _checkCellContent = _lastValue.substring(0,6);
	if(_checkCellContent == '<input')
		return;

	var _objRef = $(_obj).attr('rel');
	var _objInfo = _objRef.split("-");
	var _objID = _objInfo[0];
	var _entity = _objInfo[1];
	var _key = _objInfo[2];
	var _field = _objInfo[3];
	var _isRequired = _objInfo[4];
	var _fieldType = _objInfo[5];
	var _fieldLabel = _objInfo[6];
	var _selectField = _objInfo[7];
	if(_fieldLabel === undefined) _fieldLabel = '';
	if(_selectField === undefined) _selectField = '';



	if(_fieldType == 'date' && _lastValue == '--') _lastValue = '';
	if(_fieldType == 'date' && _lastValue != '') _lastValue = formatDateToFlat(_lastValue);
	
	var _requiredStr = '';
	if(_isRequired == 'required') _requiredStr = 'required="required"';

	   if(_selectField != '' && _fieldType == 'select'  ) { 

			$.ajax({
				type: "POST",
				url: _urlSelect,
				data: { tableSelect: _fieldLabel, entity: _entity, value: _lastValue, field: _field, selectField: _selectField, key: _key, objID: _objID  },
				success: function(msg) {
					_output = msg;
					$(_obj).html(_output);
				}
			});

	   } else {
	   	   styleWithPx = eval( _lastValue.length * 8 );
	   	   if(styleWithPx < 150 ) styleWithPx = 150;
	   	   styleWith = styleWithPx+"px";
	   	  // alert(styleWith);
	   	   var _output = '<input type="'+_fieldType+'" style="width:'+styleWith+'"  rel="'+_fieldType+'" value="'+_lastValue.replace('"', ' ')+'" id="tmpCurrentID" onBlur="setValue(this, \''+_url+'\', \''+_entity+'\', \''+_key+'\', \''+_field+'\', \''+_objID+'\', \''+_fieldLabel+'\', \''+_urlSelect+'\');" onkeydown="checkKeyHit(event.keyCode, this, \''+_url+'\', \''+_entity+'\', \''+_key+'\', \''+_field+'\', \''+_objID+'\', \''+_fieldLabel+'\');" '+_requiredStr+' />';
	   }


	$(_obj).html(_output);
	$('#tmpCurrentID').focus();
}

function setValue(_obj, _url, _entity, _key, _field, _objID, _fieldLabel, _urlSelect  ) {
	_currentValue = $.trim($(_obj).val());
   
	/*var_customFieldID = ''
	if(_entity == 'orderFieldsValue')*/
	var _isRequired = $(_obj).attr('required');
	var _fieldType = $(_obj).attr('rel');


	if(_lastValue == _currentValue) {

		if(_fieldType == 'date') {
			if(_lastValue != '')
				_currentValue = formatFlatDateDisp(_lastValue);
			else 
				_currentValue = '';
		}

		getHtml(_obj, _url, _entity, _fieldLabel, _urlSelect);
		$('.flash-notice').html('');
		return;
	} else {


		if(_isRequired == 'required' &&  _currentValue == '')  {
			_currentValue = _lastValue;
			//$('.flash-notice').html('Erreur: Ce champ est requis');
			alert('Erreur: Ce champ est requis');
			//$('.flash-notice').html('Erreur: Ce champ doit avoir une valeur numérique');
		} else if( ((_fieldType == 'numeric' && !$.isNumeric(_currentValue)) || (_fieldType == 'int' && !isInt(_currentValue))) && _currentValue != '' ) {
			_currentValue = _lastValue;
			//$('.flash-notice').html('Erreur: Ce champ doit avoir une valeur numérique');
			alert('Erreur: Ce champ doit avoir une valeur numérique');
		} else if(_fieldType == 'date' && !isValidFlatDate(_currentValue) && _currentValue != '') {
			if(_lastValue != '')
				_currentValue = formatFlatDateDisp(_lastValue);
			else 
				_currentValue = '';
			//$('.flash-notice').html('Erreur: Date erronée (jjmmyyyy)');
			alert('Erreur: Date erronée (jjmmyyyy)');
		} else if( _fieldType == 'email' &&  validateEmail(_currentValue)==false   ) {
			_currentValue = _lastValue;
			//$('.flash-notice').html('Erreur: Ce champ doit être une adresse émail valide ');
			alert('Erreur: Ce champ doit être une adresse émail valide');
		} else {
			var _currentValueQS = _currentValue;
			if(_fieldType == 'date') {
				_currentValueQS = formatFlatDate(_currentValueQS);
				_currentValue = formatFlatDateDisp(_currentValue);
			}
			//var DATA = 'entity='+_entity+'&key='+_key+'&field='+_field+'+&id='+_objID+'&value='+_currentValueQS+'&label='+_fieldLabel+'&fieldType='+_fieldType;
			$.ajax({
				type: "POST",
				url: _url,
				data: { entity: _entity, key: _key, field: _field, id: _objID, value: _currentValueQS, label: _fieldLabel, fieldType: _fieldType },
				success: function(msg) {
					_returnDate = JSON.parse(msg);

					var _errCode = _returnDate.errCode;
					var _data = _returnDate.data;

					switch($.trim(_errCode)) {
						case '1': $('.flash-notice').html('Une erreur est survenue'); _currentValue = _lastValue; break;
						case '2': $('.flash-notice').html('Erreur: Ce N° CAR existe déjà'); _currentValue = _lastValue; break;
						case '3': $('.flash-notice').html('Erreur: Ce N° CHASSIS existe déjà'); _currentValue = _lastValue; break;
						default: $('.flash-notice').html('Modification effectuée avec succès');
					}

					if($.trim(_errCode) == 2) {
						$('#'+_objID+'-carNumber--text-').html(_currentValue);
						return;
					}

					if($.trim(_errCode) == 3) {
						$('#'+_objID+'-chassisNumber--text-').html(_currentValue);
						return;
					}

				}
			});
		}

		getHtml(_obj, _url, _entity, _fieldLabel, _urlSelect);
	}

}

function getHtml(_obj, _url, _entity, _fieldType, _urlSelect) {
	var _objParent = $(_obj).parent();
    
    if(_fieldType == 'select') _currentValue =  $("#tmpCurrentID option:selected").text();
	$(_objParent).html(_currentValue);

	var _event = 'click';
    $(_objParent).bind(_event,function() {
    	getEditable(this, _url, _urlSelect);
    });

}

function checkKeyHit(kc, _obj, _url, _entity, _key, _field, _objID, _fieldLabel) {
	if(kc == 13 || kc == 9) {
		setValue(_obj, _url, _entity, _key, _field, _objID, _fieldLabel);
	} else if(kc == 27) {
		var _fieldType = $(_obj).attr('rel');
		if(_fieldType == 'date') {
			if(_lastValue != '')
				_currentValue = formatFlatDateDisp(_lastValue);
			else 
				_currentValue = '';
		} else {
			_currentValue = _lastValue;
		}
        _urlSelect = '';
		getHtml(_obj, _url, _entity, _fieldLabel, _urlSelect);
	}
}
