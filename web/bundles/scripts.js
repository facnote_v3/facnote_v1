$(function() {
    "use strict";

	var addConstraint = function(e, value) {
		var type = $(this).attr('data-type'),
			value = (value) ? value : '',
			readonly  = '';
		
		if(type == 'digit' || value == '[[:digit:]]') {
			readonly  = 'readonly';
			value = 'Chiffre';
		} else if(type == 'space' || value == '[[:space:]]') {
			readonly  = 'readonly';
			value = 'Espace';
		}
			

        $("#wrap_mcf").append('<div class="row_constraint"><span class="form-and">ET</span><input type="text" class="form-control form-next" name="contraintes[]" value="' + value + '" ' + readonly + '/><span class="btn btn-default btn-constraint" data-event="remove-constraint"><span class="fa fa-minus"></span></span></div>');
    }

    $("[data-event=add-constraint]").on('click', addConstraint);
	
    $("form").on('click', '[data-event=remove-constraint]', function(e){
        $(this).parent().remove();
    });

    var main_values = $("#banqueecriturebundle_libelle_contient").val().replace('^','').replace('$','').split('&&');
    main_values = main_values.filter(item => item.length > 0)
    //$("#wrap_mcf").append('<input type="text" class="form-control" name="contraintes[]" value="' + main_values[0] + '"/>');
    $.each(main_values, function(index, value){
        //if(index)
            addConstraint(false, value);
    });
    
});