$(function(){
    var baseUrlApi = "https://api.insee.fr/entreprises/sirene/V3/siret"
    var TAILLE_QUERY = 3; //nombres de caractéres avant de lancer la recherche vers l'API
    //var TOKEN = '65aa9de4-359d-334d-a22a-b54e1c0e710b';
    
    var TABLE_CORRESPONDANCE = {
        "id-champ-nom" : "#crm_societebundle_societe_raisonSocial", //champs de recherche
        "id-champ-siret" : "#crm_societebundle_societe_siret",
		"id-champ-siren" : "#crm_societebundle_societe_siren",
        "id-champ-ape" : "#crm_societebundle_societe_codeApe",
		"id-champ-societe-resp" : "#crm_societebundle_societe_responsable",
		"id-champ-societe-tva" : "#crm_societebundle_societe_numeroTva",
		"id-champ-societe-rcs" : "#crm_societebundle_societe_rcs",
        "id-champ-adresse" : "[name='adresseSiege']",
        "id-champ-ville" : "[name='villeSiege']",
		"id-champ-codepostal" : "[name='codePostaleSiege']",
        "id-champ-ul" : "#liste-societe"  //liste déroulante
    };

    var TOKEN = $(TABLE_CORRESPONDANCE['id-champ-ul']).data('token');

    //liste des champs retournés par l'API
    var FIELDS_FROM_API = "codeCommuneEtablissement,"
        + "codePostalEtablissement,"
        + "libelleCommuneEtablissement,"
        + "libelleVoieEtablissement,"
        + "denominationUniteLegale,"
        + "activitePrincipaleUniteLegale,"
        + "siren,siret,"
        + "denominationUsuelle1UniteLegale,"
        + "denominationUsuelleEtablissement,"
        + "denominationUsuelle2UniteLegale,"
        + "denominationUsuelle3UniteLegale,"
        + "sigleUniteLegale,"
        + "enseigne1Etablissement,"
        + "enseigne2Etablissement,"
        + "enseigne3Etablissement,"
        + "libelleVoieEtablissement,"
        + "numeroVoieEtablissement,"
        + "typeVoieEtablissement";
        
    var today = getCurrentDate();
    var ul = $(TABLE_CORRESPONDANCE["id-champ-ul"]);
    var siret = $(TABLE_CORRESPONDANCE["id-champ-siret"]);
	var siren = $(TABLE_CORRESPONDANCE["id-champ-siren"]);
    var nom = $(TABLE_CORRESPONDANCE["id-champ-nom"]);
    var ape = $(TABLE_CORRESPONDANCE["id-champ-ape"]);
	var societe_resp = $(TABLE_CORRESPONDANCE["id-champ-societe-resp"]);
	var societe_tva = $(TABLE_CORRESPONDANCE["id-champ-societe-tva"]);
	var societe_rcs = $(TABLE_CORRESPONDANCE["id-champ-societe-rcs"]);
    var ville = $(TABLE_CORRESPONDANCE["id-champ-ville"]);
	var codepostal = $(TABLE_CORRESPONDANCE["id-champ-codepostal"]);
    var adresse = $(TABLE_CORRESPONDANCE["id-champ-adresse"]);
    
    $(TABLE_CORRESPONDANCE["id-champ-nom"]).on("keyup", function(event){
        var $this = $(this);
        var criteres = $this.val();
		$("#closeSuggestions").show();
        if($this.val().length >= TAILLE_QUERY){
            callApi(ul, criteres);
        }
    });

      //click sur la liste déroulante
      ul.on("click",".info-list", function() {
			$("#closeSuggestions").hide();
            $this = $(this);
            var infoSociete = $this.data("info");
            infoSociete = JSON.parse(decodeURIComponent(infoSociete));
            //remplissage des chmaps du formulaire avec les ids
            siret.val(infoSociete.siret);
			siren.val(infoSociete.siren);
            nom.val(infoSociete.nom);
            ape.val(infoSociete.ape);
            adresse.val(infoSociete.adresse);
            ville.val(infoSociete.ville);
			codepostal.val(infoSociete.codepostal);
			societe_resp.val(infoSociete.resp);
			societe_tva.val();
			societe_rcs.val(infoSociete.ville);
            //vidage de la iste déroulante
            ul.empty();
      })
      
      function callApi(ul, criteres){
          $.ajax({
            type: "GET",
            url: baseUrlApi ,
            cache: true,
            data: "q=raisonSociale:" + criteres + "* OR siret:" + criteres + "*&champs=" + FIELDS_FROM_API + "&date=" + today, 
            headers: { "Authorization": 'Bearer ' + TOKEN },
    
            success: function(data){
                ul.empty();
                var listeSocietes = [];
                listeSocietes = (data.etablissements) ? data.etablissements : [];

                $.each(listeSocietes, function(id, societe) {
                    var nomSoc = (societe.uniteLegale.denominationUniteLegale) ? societe.uniteLegale.denominationUniteLegale : societe.uniteLegale.denominationUsuelle1UniteLegale;
					var nom = (societe.uniteLegale.nomUniteLegale) ? societe.uniteLegale.nomUniteLegale : societe.uniteLegale.nomUsageUniteLegale;
					var prenom = (societe.uniteLegale.prenom1UniteLegale) ? societe.uniteLegale.prenom1UniteLegale : societe.uniteLegale.prenomUsuelUniteLegale;
                    var infoSociete = {
                        "nom": nomSoc,
                        "siret": societe.siret,
						"siren": societe.siren,
                        "ville": societe.adresseEtablissement.libelleCommuneEtablissement,
                        "ape": societe.uniteLegale.activitePrincipaleUniteLegale,
                        "adresse": societe.adresseEtablissement.numeroVoieEtablissement + "  " 
                                + societe.adresseEtablissement.typeVoieEtablissement + " " 
                                + societe.adresseEtablissement.libelleVoieEtablissement + ", "
                                + societe.adresseEtablissement.libelleCommuneEtablissement,
						"codepostal": societe.adresseEtablissement.codePostalEtablissement,
						"resp": ((nom != undefined) ? nom : "") + " " + ((prenom != undefined) ? prenom : "")
                    };

                    var li = $('<li><a href="#">' + infoSociete.nom + ' ' + infoSociete.siret + ' ' + infoSociete.ville +'</a></li>');
                    li.addClass('info-list');
                    infoSociete = encodeURIComponent(JSON.stringify(infoSociete));
                    li.attr('data-info', infoSociete);
                    
                    ul.append(li);  
                })
                
                
                

            },
            error: function(error){
                console.log("error: " + JSON.stringify(error))
            }
        });
      }

      function getCurrentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        if(dd<10) {
            dd = '0'+dd
        } 
        
        if(mm<10) {
            mm = '0'+mm
        } 
        
        today = yyyy + '-' + mm + '-' + dd;

        return today;
      }
        
    });
