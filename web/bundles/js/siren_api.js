$(function(){
    var baseUrlApi = "https://api.insee.fr/entreprises/sirene/V3/siret"
    var TAILLE_QUERY = 3; //nombres de caractéres avant de lancer la recherche vers l'API
    //var TOKEN = '65aa9de4-359d-334d-a22a-b54e1c0e710b';
    
    var TABLE_CORRESPONDANCE = {
        "id-champ-nom" : "#crm_prospectbundle_prospect_nom", //champs de recherche
        "id-champ-siret" : "#crm_prospectbundle_prospect_siret",
        "id-champ-ape" : "#crm_prospectbundle_prospect_codeApe",
        "id-champ-adresse" : "#crm_prospectbundle_prospect_adresse",
        "id-champ-ville" : "#crm_prospectbundle_prospect_ville",
        "id-champ-ul" : "#liste-societe"  //liste déroulante
    };

    var TOKEN = $(TABLE_CORRESPONDANCE['id-champ-ul']).data('token');

    //liste des champs retournés par l'API
    var FIELDS_FROM_API = "codeCommuneEtablissement,"
        + "codePostalEtablissement,"
        + "libelleCommuneEtablissement,"
        + "libelleVoieEtablissement,"
        + "denominationUniteLegale,"
        + "activitePrincipaleUniteLegale,"
        + "siren,siret,"
        + "denominationUsuelle1UniteLegale,"
        + "denominationUsuelleEtablissement,"
        + "denominationUsuelle2UniteLegale,"
        + "denominationUsuelle3UniteLegale,"
        + "sigleUniteLegale,"
        + "enseigne1Etablissement,"
        + "enseigne2Etablissement,"
        + "enseigne3Etablissement,"
        + "libelleVoieEtablissement,"
        + "numeroVoieEtablissement,"
        + "typeVoieEtablissement";
        
    var today = getCurrentDate();
    var ul = $(TABLE_CORRESPONDANCE["id-champ-ul"]);
    var siret = $(TABLE_CORRESPONDANCE["id-champ-siret"]);
    var nom = $(TABLE_CORRESPONDANCE["id-champ-nom"]);
    var ape = $(TABLE_CORRESPONDANCE["id-champ-ape"]);
    var ville = $(TABLE_CORRESPONDANCE["id-champ-ville"]);
    var adresse = $(TABLE_CORRESPONDANCE["id-champ-adresse"]);
    
    $(TABLE_CORRESPONDANCE["id-champ-nom"]).on("keyup", function(event){
        var $this = $(this);
        var criteres = $this.val();

        if($this.val().length >= TAILLE_QUERY){
            callApi(ul, criteres);
        }
    });

      //click sur la liste déroulante
      ul.on("click",".info-list", function() {
            $this = $(this);
            var infoSociete = $this.data("info");
            infoSociete = JSON.parse(decodeURIComponent(infoSociete));
            //remplissage des chmaps du formulaire avec les ids
            siret.val(infoSociete.siret);
            nom.val(infoSociete.nom);
            ape.val(infoSociete.ape);
            adresse.val(infoSociete.adresse);
            ville.val(infoSociete.ville);

            //vidage de la iste déroulante
            ul.empty();
      })
      
      function callApi(ul, criteres){
          $.ajax({
            type: "GET",
            url: baseUrlApi ,
            cache: true,
            data: "q=raisonSociale:" + criteres + "* OR siret:" + criteres + "*&champs=" + FIELDS_FROM_API + "&date=" + today, 
            headers: { "Authorization": 'Bearer ' + TOKEN },
    
            success: function(data){
                ul.empty();
                var listeSocietes = [];
                listeSocietes = (data.etablissements) ? data.etablissements : [];

                $.each(listeSocietes, function(id, societe) {
                    var nomSoc = (societe.uniteLegale.denominationUniteLegale) ? societe.uniteLegale.denominationUniteLegale : societe.uniteLegale.denominationUsuelle1UniteLegale;
                    var infoSociete = {
                        "nom": nomSoc,
                        "siret": societe.siret,
                        "ville": societe.adresseEtablissement.libelleCommuneEtablissement,
                        "ape": societe.uniteLegale.activitePrincipaleUniteLegale,
                        "adresse": societe.adresseEtablissement.numeroVoieEtablissement + "  " 
                                + societe.adresseEtablissement.typeVoieEtablissement + " " 
                                + societe.adresseEtablissement.libelleVoieEtablissement + ", "
                                + societe.adresseEtablissement.libelleCommuneEtablissement
                                

                    };

                    var li = $('<li><a href="#">' + infoSociete.nom + ' ' + infoSociete.siret + ' ' + infoSociete.ville +'</a></li>');
                    li.addClass('info-list');
                    infoSociete = encodeURIComponent(JSON.stringify(infoSociete));
                    li.attr('data-info', infoSociete);
                    
                    ul.append(li);  
                })
                
                
                

            },
            error: function(error){
                console.log("error: " + JSON.stringify(error))
            }
        });
      }

      function getCurrentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        if(dd<10) {
            dd = '0'+dd
        } 
        
        if(mm<10) {
            mm = '0'+mm
        } 
        
        today = yyyy + '-' + mm + '-' + dd;

        return today;
      }
        
    });
