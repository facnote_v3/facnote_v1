$(function() {
    "use strict";

	var addConstraint = function(event, value, wrapClass) {
		var type = $(this).attr('data-type'),
			value = (value) ? value : '',
			readonly  = '';
		
		if(type == 'digit' || value == '[[:digit:]]') {
			readonly  = 'readonly';
			value = 'Chiffre';
		} else if(type == 'space' || value == '[[:space:]]') {
			readonly  = 'readonly';
			value = 'Espace';
        }
        
        if(event) {
            if( $(event.target).closest(".wrap_mcf_child").length)
                $(event.target).closest(".wrap_mcf").append('<div class="row_constraint"><span class="form-and">ET</span><input type="text" class="form-control form-next" name="et_contraintes[]" value="' + value + '" ' + readonly + '/><span class="btn btn-default btn-constraint" data-event="remove-constraint"><span class="fa fa-minus"></span></span></div>');
            else
                $(event.target).closest(".wrap_mcf").append('<div class="row_constraint"><span class="form-and">ET</span><input type="text" class="form-control form-next" name="contraintes[]" value="' + value + '" ' + readonly + '/><span class="btn btn-default btn-constraint" data-event="remove-constraint"><span class="fa fa-minus"></span></span></div>');
        } else {
            if( wrapClass == '.wrap_mcf_child' )
                $(wrapClass).append('<div class="row_constraint"><span class="form-and">ET</span><input type="text" class="form-control form-next" name="et_contraintes[]" value="' + value + '" ' + readonly + '/><span class="btn btn-default btn-constraint" data-event="remove-constraint"><span class="fa fa-minus"></span></span></div>');
            
            if(wrapClass == '.wrap_mcf:not(.wrap_mcf_child)' )
                $(wrapClass).append('<div class="row_constraint"><span class="form-and">ET</span><input type="text" class="form-control form-next" name="contraintes[]" value="' + value + '" ' + readonly + '/><span class="btn btn-default btn-constraint" data-event="remove-constraint"><span class="fa fa-minus"></span></span></div>');
        }
    }

    $("[data-event=add-constraint]").on('click', addConstraint);
	
    $("form").on('click', '[data-event=remove-constraint]', function(e){
        $(this).parent().remove();
    });

    $("#banqueecriturebundle_libelle_et").on('change',function(){
        if( $("#banqueecriturebundle_libelle_et").val() == 'pourcentage' )
            $(".pourcentage").show();
        else{
            $(".pourcentage").hide();
            $("#banqueecriturebundle_libelle_etPourcentage").val("");
        }
    });

    if( $("#banqueecriturebundle_libelle_etPourcentage").length && $("#banqueecriturebundle_libelle_etPourcentage").val().length ){
        $(".pourcentage").show();
    }

    var main_values = $("#banqueecriturebundle_libelle_contient").val().replace('^','').replace('$','').split('&&');
    main_values = main_values.filter(item => item.length > 0);
    $.each(main_values, function(index, value){
        addConstraint(false, value, '.wrap_mcf:not(.wrap_mcf_child)');
    });

    if( $("#banqueecriturebundle_libelle_etContient").length ){
        var et_main_values = $("#banqueecriturebundle_libelle_etContient").val().replace('^','').replace('$','').split('&&');
        et_main_values = et_main_values.filter(item => item.length > 0);
        $.each(et_main_values, function(index, value){
            addConstraint(false, value, '.wrap_mcf_child');
        });
    }
    
    
});