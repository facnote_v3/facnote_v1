// Drag and drop
$(function () {
  $('#sortable')
    .sortable({
      placeholder: 'ui-state-highlight',
      stop: function (event, ui) {
        sauvegarder();
      }
    });
  $('#sortable')
    .disableSelection();
});


//Ajout des widget dans le dashboard
function getAllWidget() {
  // recuperation des valeur depuis le modal
  var InputProductStatistique = document.getElementById('counterProductStatistique').innerHTML;
  var InputProductList = document.getElementById('counterProductList').innerHTML;
  var InputProductStatus = document.getElementById('counterProductStatuts').innerHTML;


  // Savoir combien d'element d'element ajouter pour avoir des id unique
  var sortable1 = document.getElementById('sortable').childElementCount;

  // Creation de Produit statistique
  for (var i = 0; i < InputProductStatistique; i++) {
    var node = document.createElement('div');


    node.className = 'ui-state-default col-md-6';

    var elmentid = 'barchart_material' + (i + sortable1);
    var card = 'card' + (i + sortable1);
    node.id = card;
    var panel = '<div class="card" >' +
      '<div class="card-header">' +
      'Product Statistique' +

      '<button type="button" class="close" aria-label="Close"  onclick="closeDiv(this)"  data-string="' + card + '">' +
      '<span aria-hidden="true">&times;</span>' +
      '</button>' +
      '</div>' +
      '<div class="card-body">' +
      '<canvas id="' + elmentid + '" class="chart"></canvas>' +

      ' </div>' +
      '</div>';


    node.innerHTML = panel;
    document.getElementById('sortable')
      .appendChild(node);
    chartCreator(elmentid);

  }
// Creation de Produit List
  for (var i = 0; i < InputProductList; i++) {
    var node = document.createElement('div');
    node.className = 'ui-state-default col-md-12';

    var card = 'List' + (i + sortable1 + InputProductStatistique);
    node.id = card;
    var panel = '<div class="card" >' +
      '<div class="card-header">' +
      ' Product List' +

      '<button type="button" class="close" aria-label="Close"  onclick="closeDiv(this)"  data-string="' + card + '">' +
      '<span aria-hidden="true">&times;</span>' +
      '</button>' +
      '</div>' +
      '<div class="card-body">' +
      '<table class="table table-hover">' +
      '<thead>' +
      ' <tr>' +
      '<th scope="col">#</th>' +
      ' <th scope="col">Product</th>' +
      '<th scope="col">Amount</th>' +
      '<th scope="col">Status</th>' +
      '</tr>' +
      '</thead>' +
      '<tbody>' +
      '<tr>' +
      '<th scope="row">1</th>' +
      '<td>Iphone 5</td>' +
      '<td>$ 1250.00</td>' +
      '<td>Pending</td>' +
      '</tr>' +
      '<tr>' +
      '<th scope="row">2</th>' +
      '<td>Earphone GL</td>' +
      '<td>$ 1500.00</td>' +
      '<td>Pending</td>' +
      '</tr>' +
      '<tr>' +
      '<th scope="row">3</th>' +
      '<td >HD Hand Camera</td>' +
      '<td>$ 1400.00</td>' +
      '<td>Failed</td>' +
      '</tr>' +

      '<tr>' +
      '<th scope="row">4</th>' +
      '<td >Hand Watch</td>' +
      '<td>$ 1800.00</td>' +
      '<td>Paid</td>' +
      '</tr>' +
      '</tbody>' +
      '</table>' +

      ' </div>' +
      '</div>';

    node.innerHTML = panel;
    document.getElementById('sortable')
      .appendChild(node);

  }
// Creation de Produit status
  for (var i = 0; i < InputProductStatus; i++) {
    var node = document.createElement('div');
    node.className = 'ui-state-default col-md-3';
    var card = 'status' + (i + sortable1 + InputProductStatistique + InputProductList);
    node.id = card;
    var panel = '<div class="card" >' +
      '<div class="card-header">' +
      ' Product Status' +

      '<button type="button" class="close" aria-label="Close"  onclick="closeDiv(this)"  data-string="' + card + '">' +
      '<span aria-hidden="true">&times;</span>' +
      '</button>' +
      '</div>' +
      '<div class="card-body">' +
      '<div class="media-body text-left">' +
      '<h4 class="text-primary">45,85,240</h4>' +
      '<span><strong>Total Sales</strong></span>' +
      '</div>' +
      '<div class="align-self-center w-circle-icon rounded gradient-violet">' +
      '<span class="glyphicon glyphicon-shopping-cart"></span></div>' +

      ' </div>' +
      '</div>';

    node.innerHTML = panel;
    document.getElementById('sortable')
      .appendChild(node);

  }
  // Reinitialiser les valeurs des inputs
  var InputProductStatistique = document.getElementById('counterProductStatistique').innerHTML = '';
  var InputProductList = document.getElementById('counterProductList').innerHTML = '';
  var InputProductStatus = document.getElementById('counterProductStatuts').innerHTML = '';

  // Empty remove
  if (document.getElementById('empty') && InputProductStatistique + InputProductList + InputProductStatus == 0) {
    deleteEmpty();
  }
// sauvegarder les positions avec la methode sauvegarder 
  sauvegarder();
}

//chart methode get id return chart

function chartCreator(val) {
  var ctx = document.getElementById(val)
    .getContext('2d');

  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Iphone5', 'Earphone', 'Camera', 'Watch', 'SamsungS9', 'Dell'],
      datasets: [{
        label: '# of Product sales',
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });


}

// Enlever widget du dashboard

function closeDiv(val) {
  var mystring = $(val)
    .data('string');
 
  var element = document.getElementById(mystring);
  element.parentNode.removeChild(element);

  sauvegarder();
}

// Enlever Empty  element
function deleteEmpty() {


  var empty = document.getElementById('empty');
  empty.parentNode.removeChild(empty);

}

// counter pour les add widget inceremente a chaque click
function counterProductStatistique() {

  var val = document.getElementById('counterProductStatistique');
 
  if (val.innerHTML == '') {
    val.innerHTML = '1';
  } else {
    var inc = val.innerHTML;
    inc = parseInt(inc);
    val.innerHTML = inc + 1;
  }
}

function counterProductList() {

  var val = document.getElementById('counterProductList');
 
  if (val.innerHTML == '') {
    val.innerHTML = '1';
  } else {
    var inc = val.innerHTML;
    inc = parseInt(inc);
    val.innerHTML = inc + 1;
  }
}

function counterProductStatuts() {

  var val = document.getElementById('counterProductStatuts');
 
  if (val.innerHTML == '') {
    val.innerHTML = '1';
  } else {
    var inc = val.innerHTML;
    inc = parseInt(inc);
    val.innerHTML = inc + 1;
  }
}


//  initialisation des counter dans le modal
function setToNull() {

  var InputProductStatistique = document.getElementById('counterProductStatistique').innerHTML = '';
  var InputProductList = document.getElementById('counterProductList').innerHTML = '';
  var InputProductStatus = document.getElementById('counterProductStatuts').innerHTML = '';


}

// Elever Empty show element
function deleteEmpty() {


  var empty = document.getElementById('empty');
  empty.parentNode.removeChild(empty);

}

