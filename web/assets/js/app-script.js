
$(function() {
    "use strict";

//$("#wrapper").addClass("toggled");
     	 
//sidebar menu js
$.sidebarMenu($('.sidebar-menu'));

// === toggle-menu js
$(".toggle-menu").on("click", function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });	 
});  
	
	   
// === sidebar menu activation js

$(function() {
        for (var i = window.location, o = $(".sidebar-menu a").filter(function() {
            return this.href == i;
        }).addClass("active").parent().addClass("active"); ;) {
            if (!o.is("li")) break;
            o = o.parent().addClass("in").parent().addClass("active");
        }
    });   
	   
/* Back To Top */

$(document).ready(function(){ 

  $('html').css('zoom','92%'); /* Webkit browsers */
  $('html').css('zoom','0.92'); /* Other non-webkit browsers */
  $('html').css('-moz-transform',scale(0.93, 0.93)); /* Moz-browsers */


    $(window).on("scroll", function(){ 
        if ($(this).scrollTop() > 300) { 
            $('.back-to-top').fadeIn(); 
        } else { 
            $('.back-to-top').fadeOut(); 
        } 
    }); 
    $('.back-to-top').on("click", function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});	   
	   
$(function () {
  $('[data-toggle="popover"]').popover()
});


$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle-second="tooltip"]').tooltip();
});