
function goToUrl(_url) {
    document.location.href = _url;
}

function manageDispBloc(_op, _bloc) {
    if(_op == 1)
        $("#"+_bloc).show();
    else
        $("#"+_bloc).hide();
}

function closeBloc(_bloc) {
    $("#"+_bloc).html('');
    $("#"+_bloc).hide();
}

function isInt(n) {
    if(Math.floor(n) == n && $.isNumeric(n))
        return true;
    else return false;
}

function makeFieldEmpty(_fieldId) {
    $('#'+_fieldId).val('');
}

function getSort(_field, _op, _url) {
    $(".loading").show();
    var DATA = 'field=' + _field + '&op=' + _op;
    $.ajax({
        type: "GET",
        url: _url,
        data: DATA,
        success: function(msg) {
            $("#bloc-listing").html(msg);
            $(".loading").hide();
        }
    });
    return false;
}

function simpleAjaxCall(_url, _block, _dateField) {
    $(".loading").show();
    var DATA = '';
    $.ajax({
        type: "GET",
        url: _url,
        data: DATA,
        success: function(msg) {
            $("#" + _block).html(msg);
            $("#" + _block).show();
            $(".loading").hide();
            if(true === _dateField) {
                $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' })
            }
        }
    });
    return false;
}

function simpleAjaxCallLoading(_url, _data, _block, _dateField) {
    $("#" + _block).show();
    $("#" + _block).html('<li>Loading ...</li>');
    var DATA = _data;
    $.ajax({
        type: "GET",
        url: _url,
        data: DATA,
        success: function(msg) {
            $("#" + _block).html(msg);
            $("#" + _block).show();
            $(".loading").hide();
            if(true === _dateField) {
                $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' })
            }
        }
    });
    return false;
}


function confirmDelete(_url) {
    if(confirm('Êtes-vous sûr de vouloir supprimer cet enregistrement ?')) {
        goToUrl(_url);
    }
}

function confirmDelete_ajax(_url, _block, _dateField) {
    if(confirm('Êtes-vous sûr de vouloir supprimer cet enregistrement ?')) {
        $(".loading").show();
        var DATA = '';
        $.ajax({
            type: "GET",
            url: _url,
            data: DATA,
            success: function(msg) {
                if(msg == -1) {
                    alert('Erreur: Cet enregistrement est lié à d\'autres enregistrements dans la base de données');
                    $(".loading").hide();
                } else {
                    $("#" + _block).html(msg);
                    $("#" + _block).show();
                    $(".loading").hide();
                    if(true === _dateField) {
                        $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' })
                    }
                }
            }
        });
    }
}

function confirmDelete_pagin(_url, _msg, _integrityErrMsg) {
    if(confirm(_msg)) {
        $(".loading").show();
        var DATA = '';
        $.ajax({
            type: "GET",
            url: _url,
            data: DATA,
            success: function(msg) {
                if(msg == -1) {
                    alert(_integrityErrMsg);
                    $(".loading").hide();
                } else {
                    var msgAry = msg.split(",");
                    initPagination(msgAry[0], msgAry[1]);
                }
            },
        });
        return false;
    }
}

function validateEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function dispLanguageManage(_blocs, _activeBlocLang, _activeLang) {
    $('.' + _blocs).hide();
    $('.language-tab').removeClass('selectedTab');

    $('#' + _activeBlocLang).show();
    $('#tab_' + _activeLang).addClass('selectedTab');
}

function tabDisplayManagement(_allBlocs, _activeBloc, _allTabs, _activeTab, _selectedClass) {
    $('.' + _allBlocs).hide();
    $('#' + _activeBloc).show();

    $('.' + _allTabs).removeClass(_selectedClass);
    $('#' + _activeTab).addClass(_selectedClass);
}