$(function() {
    "use strict";

    var removeConstraint = function(e){
        e.preventDefault();
        $(this).prev(".form-control").eq(0).remove();
        $(this).prev(".form-and").eq(0).remove();
        $(this).remove();
        return false;
    }

    var addConstraint = function(e, value){
        
        if (value == undefined)
            value = "";

        if(e)
            e.preventDefault();

        $("#wrap_mcf").append('<span class="form-and">ET</span><input type="text" class="form-control form-next" name="contraintes[]" value="' + value + '"/><a class="btn btn-default btn-constraint" data-event="remove-constraint"><span class="fa fa-minus"></span></a>');

        $("a[data-event=remove-constraint]").unbind('click').click(removeConstraint);

        return false;
    };

    $("a[data-event=add-constraint]").click(addConstraint);
    $("a[data-event=remove-constraint]").unbind('click').click(removeConstraint);

    var main_values = $("#banqueecriturebundle_libelle_contient").val().split('&&');
    $("#wrap_mcf").append('<input type="text" class="form-control" name="contraintes[]" value="' + main_values[0] + '" required="required"/>');
    $.each(main_values, function(index, value){
        if(index)
            addConstraint(false, value);
    });
    
});