<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/click-save')) {
            // facnote_click_save
            if ('/click-save' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\ClickSaveController::clickSaveAction',  '_route' => 'facnote_click_save',);
            }

            // facnote_click_select
            if ('/click-save-select' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\ClickSaveController::clickSaveSelectAction',  '_route' => 'facnote_click_select',);
            }

        }

        elseif (0 === strpos($pathinfo, '/coming-soon-')) {
            // coming_soon_devis_homepage
            if ('/coming-soon-devis' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_devis_homepage',);
            }

            if (0 === strpos($pathinfo, '/coming-soon-c')) {
                // coming_soon_commande_homepage
                if ('/coming-soon-commande' === $pathinfo) {
                    return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_commande_homepage',);
                }

                // coming_soon_configuration_homepage
                if ('/coming-soon-configuration' === $pathinfo) {
                    return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_configuration_homepage',);
                }

                // coming_soon_clients_homepage
                if ('/coming-soon-clients' === $pathinfo) {
                    return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_clients_homepage',);
                }

            }

            // coming_soon_avoir_homepage
            if ('/coming-soon-avoir' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_avoir_homepage',);
            }

            // coming_soon_bord_homepage
            if ('/coming-soon-bord' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_bord_homepage',);
            }

            // coming_soon_fournisseurs_homepage
            if ('/coming-soon-fournisseurs' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_fournisseurs_homepage',);
            }

            // coming_soon_reglement_homepage
            if ('/coming-soon-reglement' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_reglement_homepage',);
            }

            // coming_soon_ged_homepage
            if ('/coming-soon-ged' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_ged_homepage',);
            }

            // coming_soon_indicateur_homepage
            if ('/coming-soon-indicateurs' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::comingSoonAction',  '_route' => 'coming_soon_indicateur_homepage',);
            }

        }

        elseif (0 === strpos($pathinfo, '/login')) {
            // login_homepage
            if ('/login' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::indexAction',  '_route' => 'login_homepage',);
            }

            // login_check
            if ('/loginCheck' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::loginCheckAction',  '_route' => 'login_check',);
            }

            if (0 === strpos($pathinfo, '/login-')) {
                if (0 === strpos($pathinfo, '/login-reset')) {
                    // login_reset
                    if ('/login-reset' === $pathinfo) {
                        return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::resetPasswordAction',  '_route' => 'login_reset',);
                    }

                    // login_reset_check
                    if ('/login-reset-check' === $pathinfo) {
                        return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::resetPasswordAction',  '_route' => 'login_reset_check',);
                    }

                    // login_reset_get
                    if (0 === strpos($pathinfo, '/login-reset-get') && preg_match('#^/login\\-reset\\-get/(?P<username>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'login_reset_get')), array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::getNewPasswordAction',));
                    }

                }

                elseif (0 === strpos($pathinfo, '/login-expert')) {
                    // login_check_app_base
                    if (preg_match('#^/login\\-expert/(?P<appBase>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'login_check_app_base')), array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::checkAppBaseAction',));
                    }

                    // login_back_app_expert
                    if ('/login-expert-back' === $pathinfo) {
                        return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::backExpertAction',  '_route' => 'login_back_app_expert',);
                    }

                }

                // login_back_app_admin
                if ('/login-admin-back' === $pathinfo) {
                    return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::backAdminAction',  '_route' => 'login_back_app_admin',);
                }

            }

        }

        // login_logout
        if ('/logout' === $pathinfo) {
            return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\LoginController::logoutAction',  '_route' => 'login_logout',);
        }

        // utilisateurs_user_homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::indexAction',  '_route' => 'utilisateurs_user_homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_utilisateurs_user_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'utilisateurs_user_homepage'));
            }

            return $ret;
        }
        not_utilisateurs_user_homepage:

        if (0 === strpos($pathinfo, '/add')) {
            // utilisateurs_user_add
            if ('/add' === $pathinfo) {
                return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::addAction',  '_route' => 'utilisateurs_user_add',);
            }

            // utilisateurs_user_edit
            if (preg_match('#^/add/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'utilisateurs_user_edit')), array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::addAction',));
            }

        }

        // utilisateurs_user_delete
        if (0 === strpos($pathinfo, '/delete') && preg_match('#^/delete/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'utilisateurs_user_delete')), array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::deleteAction',));
        }

        // utilisateurs_user_export_csv
        if ('/export/csv' === $pathinfo) {
            return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::CSVExportAction',  '_route' => 'utilisateurs_user_export_csv',);
        }

        // utilisateurs_user_export_pdf
        if ('/export/pdf' === $pathinfo) {
            return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\UserController::PDFExportAction',  '_route' => 'utilisateurs_user_export_pdf',);
        }

        // utilisateurs_moncompte_homepage
        if ('/moncompte' === $pathinfo) {
            return array (  '_controller' => 'Utilisateurs\\UserBundle\\Controller\\MonCompteController::indexAction',  '_route' => 'utilisateurs_moncompte_homepage',);
        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
