<?php

/* @UtilisateursUser/Users/users_pdf.html.twig */
class __TwigTemplate_7a7205d7ba7b1a93b61135c65ba816123a59ecdfe4ae0257a6e03dc8e7cca5f8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Users/users_pdf.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Users/users_pdf.html.twig"));

        // line 1
        echo "<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    </head>
    <body style=\"font-size:16px\">
        <table border=\"0\" width=\"100%\" align=\"center\" id=\"listing\">
            <tr style=\"height: 20px; text-align: center; font-weight: bold; border-bottom: 1px #CCCCCC solid;border-left: 1px #CCCCCC solid; background: #E6E6E6;\">
                <td>NOM</td>
                <td>PRENOM</td>
                <td>EMAIL</td>
                <td>TELEPHONE</td>
                <td>ROLE</td>
                <td>ACTIVE</td>
            </tr>
            ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 15, $this->source); })()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 16
            echo "                <tr style=\"border-bottom: 1px #CCC solid; vertical-align: top; ";
            if ((0 == twig_get_attribute($this->env, $this->source, $context["loop"], "index", array()) % 2)) {
                echo " background-color: #FFFFFF; ";
            } else {
                echo " background-color: #F5F5F5;  ";
            }
            echo "\">
                    <td>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "prenom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "email", array()), "html", null, true);
            echo "</td>
                    <td align=\"center\">";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "telephonne", array()), "html", null, true);
            echo "</td>
                    <td align=\"center\">";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "roleUser", array()), "html", null, true);
            echo "</td>
                    <td align=\"center\">
                        ";
            // line 23
            if ((twig_get_attribute($this->env, $this->source, $context["item"], "active", array()) == 1)) {
                // line 24
                echo "                            OUI
                        ";
            } else {
                // line 26
                echo "                            NON
                        ";
            }
            // line 28
            echo "                    </td>
                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </table>
    </body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@UtilisateursUser/Users/users_pdf.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 31,  102 => 28,  98 => 26,  94 => 24,  92 => 23,  87 => 21,  83 => 20,  79 => 19,  75 => 18,  71 => 17,  62 => 16,  45 => 15,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    </head>
    <body style=\"font-size:16px\">
        <table border=\"0\" width=\"100%\" align=\"center\" id=\"listing\">
            <tr style=\"height: 20px; text-align: center; font-weight: bold; border-bottom: 1px #CCCCCC solid;border-left: 1px #CCCCCC solid; background: #E6E6E6;\">
                <td>NOM</td>
                <td>PRENOM</td>
                <td>EMAIL</td>
                <td>TELEPHONE</td>
                <td>ROLE</td>
                <td>ACTIVE</td>
            </tr>
            {% for item in users %}
                <tr style=\"border-bottom: 1px #CCC solid; vertical-align: top; {% if loop.index is divisible by(2)  %} background-color: #FFFFFF; {% else %} background-color: #F5F5F5;  {% endif %}\">
                    <td>{{ item.nom }}</td>
                    <td>{{ item.prenom }}</td>
                    <td>{{ item.email }}</td>
                    <td align=\"center\">{{ item.telephonne }}</td>
                    <td align=\"center\">{{ item.roleUser }}</td>
                    <td align=\"center\">
                        {% if item.active == 1 %}
                            OUI
                        {% else %}
                            NON
                        {% endif %}
                    </td>
                </tr>
            {% endfor %}
        </table>
    </body>
</html>", "@UtilisateursUser/Users/users_pdf.html.twig", "/Applications/MAMP/htdocs/facnotev3_banque/facnote_v3_banque/src/Utilisateurs/UserBundle/Resources/views/Users/users_pdf.html.twig");
    }
}
