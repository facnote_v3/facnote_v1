<?php

/* @UtilisateursUser/Users/add.html.twig */
class __TwigTemplate_dd477dfb1abb55e0f6e76a92fad49b936248788dc0f96cb5a13087d1e54d2115 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@UtilisateursUser/Users/add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Users/add.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Users/add.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Gestion des collaborateurs";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        echo "<div class=\"row\">
<div class=\"col-lg-12\">
<div class=\"card\">
<div class=\"card-body\">

 ";
        // line 12
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", array(), "any", false, true), "get", array(0 => "flashMessageUserError"), "method", true, true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->source); })()), "session", array()), "get", array(0 => "flashMessageUserError"), "method") != ""))) {
            // line 13
            echo "  <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
   <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
    <div class=\"alert-icon\"><i class=\"icon-close\"></i></div>
    <div class=\"alert-message\">
      <span><strong>Erreur!</strong> ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 17, $this->source); })()), "session", array()), "get", array(0 => "flashMessageUserError"), "method"), "html", null, true);
            echo "</span>
      ";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 18, $this->source); })()), "session", array()), "set", array(0 => "flashMessageUserError", 1 => ""), "method"), "html", null, true);
            echo "
    </div>
  </div>
  ";
        }
        // line 22
        echo "

    <form id=\"collaborateur-form\" method=\"post\" action=\"\" onsubmit=\"return validFormUsers();\">
      <h4 class=\"form-header text-uppercase\">
        <i class=\"fa fa-user-circle-o\"></i>
         Collaborateur Info
      </h4>
      <div class=\"form-group row\">
        <label for=\"input-1\" class=\"col-sm-2 col-form-label\">";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->source); })()), "nom", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 32, $this->source); })()), "nom", array()), 'widget');
        echo "
        </div>
      </div>
      <div class=\"form-group row\">
        <label for=\"input-2\" class=\"col-sm-2 col-form-label\">";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 36, $this->source); })()), "prenom", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 38, $this->source); })()), "prenom", array()), 'widget');
        echo "
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-4\" class=\"col-sm-2 col-form-label\">";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 43, $this->source); })()), "telephonne", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 45, $this->source); })()), "telephonne", array()), 'widget');
        echo "
        </div>
      </div>
      <h4 class=\"form-header\">
      <i class=\"fa fa-lock\"></i>
        CONNEXION
      </h4>
      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 53, $this->source); })()), "roleid", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 55, $this->source); })()), "roleid", array()), 'widget');
        echo "                  
        </div>
      </div>
      
      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 60, $this->source); })()), "active", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 62, $this->source); })()), "active", array()), 'widget');
        echo "                  
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">";
        // line 67
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 67, $this->source); })()), "login", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 69, $this->source); })()), "login", array()), 'widget');
        echo "                  
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 74, $this->source); })()), "plainPassword", array()), "first", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 76, $this->source); })()), "plainPassword", array()), "first", array()), 'widget');
        echo "  
          <label id=\"error_pw\" class=\"error\" for=\"input-10\"></label>                
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">";
        // line 82
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 82, $this->source); })()), "plainPassword", array()), "second", array()), 'label');
        echo "</label>
        <div class=\"col-sm-10\">
          ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 84, $this->source); })()), "plainPassword", array()), "second", array()), 'widget');
        echo "    
           <label id=\"error_pw2\" class=\"error\" for=\"input-10\"></label>                
        </div>
      </div>


      <div class=\"form-footer\" align=\"center\">
          <button type=\"button\" class=\"btn btn-danger\" onClick=\"javascript:goToUrl('";
        // line 91
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_homepage");
        echo "');\"><i class=\"fa fa-times\"></i> ANNULER</button>
          <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-check-square-o\"></i> VALIDER</button>
      </div>

      <input type=\"hidden\" name=\"idPersone\" value=\"";
        // line 95
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["infosUser"]) || array_key_exists("infosUser", $context) ? $context["infosUser"] : (function () { throw new Twig_Error_Runtime('Variable "infosUser" does not exist.', 95, $this->source); })()), "idPersone", array()), "html", null, true);
        echo "\">
      <input type=\"hidden\" name=\"idTelephone\" value=\"";
        // line 96
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["infosUser"]) || array_key_exists("infosUser", $context) ? $context["infosUser"] : (function () { throw new Twig_Error_Runtime('Variable "infosUser" does not exist.', 96, $this->source); })()), "idTelephone", array()), "html", null, true);
        echo "\">
      <input type=\"hidden\" name=\"idEmail\" value=\"";
        // line 97
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["infosUser"]) || array_key_exists("infosUser", $context) ? $context["infosUser"] : (function () { throw new Twig_Error_Runtime('Variable "infosUser" does not exist.', 97, $this->source); })()), "idEmail", array()), "html", null, true);
        echo "\">


      ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 100, $this->source); })()), 'rest');
        echo "
      
    </form>

</div>
</div>
</div>
</div>

<script language=\"javascript\">
    \$(document).ready(function() {
     
     var roleUserId = '";
        // line 112
        echo twig_escape_filter($this->env, (isset($context["roleUserId"]) || array_key_exists("roleUserId", $context) ? $context["roleUserId"] : (function () { throw new Twig_Error_Runtime('Variable "roleUserId" does not exist.', 112, $this->source); })()), "html", null, true);
        echo "';

     if(roleUserId == 2 ) {

      \$('#utilisateurs_userbundle_users_roleid')
        .find('option')
        .remove()
        .end()
        .append('<option value=\"2\">Expert</option>')
        .val('2')
      ;

     } else if(roleUserId == 3 )  {

      \$('#utilisateurs_userbundle_users_roleid')
        .find('option')
        .remove()
        .end()
        .append('<option value=\"3\">Collaborateur</option>')
        .val('3')
      ;

     }

    



    });

    function validFormUsers() {
        var _password = \$.trim(\$('#utilisateurs_userbundle_users_plainPassword_first').val());
        var _password2 = \$.trim(\$('#utilisateurs_userbundle_users_plainPassword_second').val());
        if(_password.length < 6 && _password!='') {
            \$(\"#error_pw\").html('Le mot de passe doit contenir au moins six caractères.');
            return false;
        }

        if( _password != _password2) {
            \$(\"#error_pw2\").html('Confirmation de mot de passe invalide.');
            return false;
        }

        return true;
    }
</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@UtilisateursUser/Users/add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 112,  237 => 100,  231 => 97,  227 => 96,  223 => 95,  216 => 91,  206 => 84,  201 => 82,  192 => 76,  187 => 74,  179 => 69,  174 => 67,  166 => 62,  161 => 60,  153 => 55,  148 => 53,  137 => 45,  132 => 43,  124 => 38,  119 => 36,  112 => 32,  107 => 30,  97 => 22,  90 => 18,  86 => 17,  80 => 13,  78 => 12,  63 => 7,  45 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}Gestion des collaborateurs{% endblock %}



{% block body %}<div class=\"row\">
<div class=\"col-lg-12\">
<div class=\"card\">
<div class=\"card-body\">

 {% if app.session.get('flashMessageUserError') is defined and app.session.get('flashMessageUserError') != '' %}
  <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
   <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
    <div class=\"alert-icon\"><i class=\"icon-close\"></i></div>
    <div class=\"alert-message\">
      <span><strong>Erreur!</strong> {{ app.session.get('flashMessageUserError') }}</span>
      {{ app.session.set('flashMessageUserError', '') }}
    </div>
  </div>
  {% endif %}


    <form id=\"collaborateur-form\" method=\"post\" action=\"\" onsubmit=\"return validFormUsers();\">
      <h4 class=\"form-header text-uppercase\">
        <i class=\"fa fa-user-circle-o\"></i>
         Collaborateur Info
      </h4>
      <div class=\"form-group row\">
        <label for=\"input-1\" class=\"col-sm-2 col-form-label\">{{ form_label(form.nom) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.nom) }}
        </div>
      </div>
      <div class=\"form-group row\">
        <label for=\"input-2\" class=\"col-sm-2 col-form-label\">{{ form_label(form.prenom) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.prenom) }}
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-4\" class=\"col-sm-2 col-form-label\">{{ form_label(form.telephonne) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.telephonne) }}
        </div>
      </div>
      <h4 class=\"form-header\">
      <i class=\"fa fa-lock\"></i>
        CONNEXION
      </h4>
      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">{{ form_label(form.roleid) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.roleid) }}                  
        </div>
      </div>
      
      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">{{ form_label(form.active) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.active) }}                  
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">{{ form_label(form.login) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.login) }}                  
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">{{ form_label(form.plainPassword.first ) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.plainPassword.first ) }}  
          <label id=\"error_pw\" class=\"error\" for=\"input-10\"></label>                
        </div>
      </div>

      <div class=\"form-group row\">
        <label for=\"input-3\" class=\"col-sm-2 col-form-label\">{{ form_label(form.plainPassword.second ) }}</label>
        <div class=\"col-sm-10\">
          {{ form_widget(form.plainPassword.second ) }}    
           <label id=\"error_pw2\" class=\"error\" for=\"input-10\"></label>                
        </div>
      </div>


      <div class=\"form-footer\" align=\"center\">
          <button type=\"button\" class=\"btn btn-danger\" onClick=\"javascript:goToUrl('{{ path('utilisateurs_user_homepage') }}');\"><i class=\"fa fa-times\"></i> ANNULER</button>
          <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa fa-check-square-o\"></i> VALIDER</button>
      </div>

      <input type=\"hidden\" name=\"idPersone\" value=\"{{ infosUser.idPersone }}\">
      <input type=\"hidden\" name=\"idTelephone\" value=\"{{ infosUser.idTelephone }}\">
      <input type=\"hidden\" name=\"idEmail\" value=\"{{ infosUser.idEmail }}\">


      {{ form_rest(form) }}
      
    </form>

</div>
</div>
</div>
</div>

<script language=\"javascript\">
    \$(document).ready(function() {
     
     var roleUserId = '{{ roleUserId }}';

     if(roleUserId == 2 ) {

      \$('#utilisateurs_userbundle_users_roleid')
        .find('option')
        .remove()
        .end()
        .append('<option value=\"2\">Expert</option>')
        .val('2')
      ;

     } else if(roleUserId == 3 )  {

      \$('#utilisateurs_userbundle_users_roleid')
        .find('option')
        .remove()
        .end()
        .append('<option value=\"3\">Collaborateur</option>')
        .val('3')
      ;

     }

    



    });

    function validFormUsers() {
        var _password = \$.trim(\$('#utilisateurs_userbundle_users_plainPassword_first').val());
        var _password2 = \$.trim(\$('#utilisateurs_userbundle_users_plainPassword_second').val());
        if(_password.length < 6 && _password!='') {
            \$(\"#error_pw\").html('Le mot de passe doit contenir au moins six caractères.');
            return false;
        }

        if( _password != _password2) {
            \$(\"#error_pw2\").html('Confirmation de mot de passe invalide.');
            return false;
        }

        return true;
    }
</script>

{% endblock %}", "@UtilisateursUser/Users/add.html.twig", "/Applications/MAMP/htdocs/facnotev3_banque/facnote_v3_banque/src/Utilisateurs/UserBundle/Resources/views/Users/add.html.twig");
    }
}
