<?php

/* base.html.twig */
class __TwigTemplate_7870cf8523a327db6eac44a69196da52282c936e303a3c0a0a5c7506a4c5a92f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'js_footer' => array($this, 'block_js_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
  <meta charset=\"utf-8\"/>
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/>
  <meta name=\"description\" content=\"\"/>
  <meta name=\"author\" content=\"\"/>
  <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo " </title>
  <!--favicon-->
  <link rel=\"icon\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/logo-icon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 29
        echo "
  ";
        // line 30
        $this->displayBlock('javascripts', $context, $blocks);
        // line 43
        echo "


</head>

<body>
   
";
        // line 50
        $context["userConnect"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 50, $this->source); })()), "session", array()), "get", array(0 => "userConnect"), "method");
        // line 51
        echo "  ";
        $context["appBase"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 51, $this->source); })()), "session", array()), "get", array(0 => "app_base"), "method");
        // line 52
        echo "<!-- Start wrapper-->
 <div id=\"wrapper\">
 
  <!--Start NAVIGATION-->
   <div id=\"sidebar-wrapper\" data-simplebar=\"\" data-simplebar-auto-hide=\"true\">
     <div class=\"brand-logo\">
      <a href=\"#\">
       <img src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/logo_facnote_v3.png"), "html", null, true);
        echo "\" class=\"\" style=\"width: 200px;height: 55px;margin: 4px;\" alt=\"logo icon\">
       <h5 class=\"logo-text\"></h5>
     </a>
     </div>
     <ul class=\"sidebar-menu do-nicescrol\">
      <li class=\"sidebar-header\"> NAVIGATION</li>



      <li ";
        // line 68
        if (((isset($context["classActive"]) || array_key_exists("classActive", $context)) && ((isset($context["classActive"]) || array_key_exists("classActive", $context) ? $context["classActive"] : (function () { throw new Twig_Error_Runtime('Variable "classActive" does not exist.', 68, $this->source); })()) == "users"))) {
            echo " class=\"active\" ";
        }
        echo " >
        <a href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_homepage");
        echo "\" class=\"waves-effect gui_collaborateur_step-1\">
          <i class=\"fa fa-user\"></i>
          <span>Collaborateurs</span>
        </a>
      </li>


      




    </ul>
     
   </div>
   <!--End NAVIGATION-->

<!--Start NAVIGATION TOP -->
<header class=\"topbar-nav\">
 <nav class=\"navbar navbar-expand fixed-top bg-white\">
  <ul class=\"navbar-nav mr-auto align-items-center\">
    <li class=\"nav-item\">
      <a class=\"nav-link toggle-menu\" href=\"javascript:void();\">
       <i class=\"icon-menu menu-icon\"></i>
     </a>
    </li>
    <li class=\"nav-item\">
      <form class=\"search-bar\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Entrer mot clé\">
         <a href=\"javascript:void();\"><i class=\"icon-magnifier\"></i></a>
      </form>
    </li>
  </ul>
     
  <ul class=\"navbar-nav align-items-center right-nav-link\">
    <a href=\"#\" style=\"color:black;\" class=\"gui_ecriture_step-0 gui_ecriture_step-9\"><li id=\"startTheGuide\"><i class=\"fa fa-question-circle-o fa-lg\" aria-hidden=\"true\"></i></li></a>
    <li class=\"nav-item dropdown-lg\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect\" data-toggle=\"dropdown\" href=\"javascript:void();\">
        <i class=\"icon-envelope-open\"></i>
        <!--<span class=\"badge badge-danger badge-up\">0</span>-->
      </a>
      <div class=\"dropdown-menu dropdown-menu-right\">
        <ul class=\"list-group list-group-flush\">
         <li class=\"list-group-item d-flex justify-content-between align-items-center\">
          Vous aves 0 nouveau messages
          <span class=\"badge badge-danger\">0</span>
          </li>
          <li class=\"list-group-item\">
          <a href=\"javaScript:void();\">
           <!--<div class=\"media\">
             <div class=\"avatar\"><img class=\"align-self-start mr-3\" src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/avatars/avatar.png"), "html", null, true);
        echo "\" alt=\"user avatar\"></div>
            <div class=\"media-body\">
            <h6 class=\"mt-0 msg-title\">Jhon Deo</h6>
            <p class=\"msg-info\">Bla bla bla ...</p>
            <small>Mardi, 4:10 PM</small>
            </div>
          </div>-->
          </a>
          </li>
          <li class=\"list-group-item\"><a href=\"javaScript:void();\">Tous Messages</a></li>
        </ul>
        </div>
    </li>

    <li class=\"nav-item dropdown-lg\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect\" data-toggle=\"dropdown\" href=\"javascript:void();\">
      <i class=\"icon-bell\"></i>
     <!-- <span class=\"badge badge-primary badge-up\">0</span> -->
      </a>
      <div class=\"dropdown-menu dropdown-menu-right\">
        <ul class=\"list-group list-group-flush\">
          <li class=\"list-group-item d-flex justify-content-between align-items-center\">
          Vous avez 0 Notifications
         <span class=\"badge badge-primary\">0</span>
          </li>
          <li class=\"list-group-item\">
          <a href=\"javaScript:void();\">
           <!--<div class=\"media\">
             <i class=\"icon-people fa-2x mr-3 text-info\"></i>
            <div class=\"media-body\">
            <h6 class=\"mt-0 msg-title\">New Registered Users</h6>
            <p class=\"msg-info\">Lorem ipsum dolor sit amet...</p>
            </div>
          </div>-->
          </a>
          </li>
          <li class=\"list-group-item\"><a href=\"javaScript:void();\">Toutes Notifications</a></li>
        </ul>
      </div>
    </li>

    <li class=\"nav-item language\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect\" data-toggle=\"dropdown\" href=\"#\"><i class=\"flag-icon flag-icon-fr\"></i></a>
      <ul class=\"dropdown-menu dropdown-menu-right\">
          <li class=\"dropdown-item\"> <i class=\"flag-icon flag-icon-fr mr-2\"></i> Français</li>
          <li class=\"dropdown-item\"> <i class=\"flag-icon flag-icon-gb mr-2\"></i> Anglais</li>
        </ul>
    </li>

    <li class=\"nav-item\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret\" data-toggle=\"dropdown\" href=\"#\">
        <span class=\"user-profile\"><img src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/avatars/avatar.png"), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"user avatar\"></span>
      </a>
      <ul class=\"dropdown-menu dropdown-menu-right\">
       <li class=\"dropdown-item user-details\">
        <a href=\"javaScript:void();\">
           <div class=\"media\">
             <div class=\"avatar\"><img class=\"align-self-start mr-3\" src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/avatars/avatar.png"), "html", null, true);
        echo "\" alt=\"user avatar\"></div>
            <div class=\"media-body\">              
            <h6 class=\"mt-2 user-title\">";
        // line 178
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["userConnect"]) || array_key_exists("userConnect", $context) ? $context["userConnect"] : (function () { throw new Twig_Error_Runtime('Variable "userConnect" does not exist.', 178, $this->source); })()), "nom", array()), "html", null, true);
        echo "   ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["userConnect"]) || array_key_exists("userConnect", $context) ? $context["userConnect"] : (function () { throw new Twig_Error_Runtime('Variable "userConnect" does not exist.', 178, $this->source); })()), "prenom", array()), "html", null, true);
        echo "</h6>
            <p class=\"user-subtitle\">";
        // line 179
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["userConnect"]) || array_key_exists("userConnect", $context) ? $context["userConnect"] : (function () { throw new Twig_Error_Runtime('Variable "userConnect" does not exist.', 179, $this->source); })()), "email", array()), "html", null, true);
        echo "</p>
            </div>
           </div>
          </a>
        </li>
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\"><i class=\"icon-envelope mr-2\"></i> Messages </li> 
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\" onClick=\"javascript:goToUrl('";
        // line 187
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_moncompte_homepage");
        echo "')\"><i class=\"icon-wallet mr-2\"></i> Mon compte</li>
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\"><i class=\"icon-settings mr-2\"></i> Paramètres</li>
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\" onClick=\"javascript:goToUrl('";
        // line 191
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login_logout");
        echo "')\"><i class=\"icon-power mr-2\"></i> Se déconnecter </li>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End NAVIGATION TOP -->
<body>
<div id=\"wrapper\">
    <div class=\"clearfix\"></div>
    
    <div class=\"content-wrapper\">

  <div id=\"errorMsgGuide\" class=\"displayNo alert\" role=\"alert\"></div>
      ";
        // line 205
        $this->displayBlock('body', $context, $blocks);
        // line 206
        echo "
    </div><!--End content-wrapper-->

   <!--Start Back To Top Button-->
    <a href=\"javaScript:void();\" class=\"back-to-top\"><i class=\"fa fa-angle-double-up\"></i> </a>
    <!--End Back To Top Button-->
    <!--Start footer-->
    <footer class=\"footer\">
      <div class=\"container\">
        <div class=\"text-center\">
          Copyright © 2018 <a href=\"http://facnote.com\" target=\"_blank\"> www.facnote.com</a>
        </div>
      </div>
    </footer>
    <!--End footer-->

</div><!--End wrapper-->
";
        // line 223
        $this->displayBlock('js_footer', $context, $blocks);
        // line 224
        echo "</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Facnote";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "  <!-- Bootstrap core CSS-->
  <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
  <!-- animate CSS-->
  <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
  <!-- Icons CSS-->
  <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/icons.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
  <!-- Sidebar CSS-->
  <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/sidebar-menu.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
  <!-- Custom Style-->
  <link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/app-style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
 <!-- Enjoyhint for the Guide-->
<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/css/enjoyhint.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        // line 27
        echo " <link href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/chatsocket/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"/>
  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 30
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 31
        echo "  <!-- Bootstrap core JavaScript-->
  <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/jquery.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
  <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/popper.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
  <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
  <script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/sidebar-menu.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
  <script src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/app-script.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

  <!-- Custom scripts -->
  <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/js/libs.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
  <!-- Index js -->

  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 205
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 223
    public function block_js_footer($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js_footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "js_footer"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  436 => 223,  419 => 205,  405 => 39,  399 => 36,  395 => 35,  391 => 34,  387 => 33,  383 => 32,  380 => 31,  371 => 30,  358 => 27,  354 => 25,  349 => 23,  344 => 21,  339 => 19,  334 => 17,  329 => 15,  326 => 14,  317 => 13,  299 => 10,  287 => 224,  285 => 223,  266 => 206,  264 => 205,  247 => 191,  240 => 187,  229 => 179,  223 => 178,  218 => 176,  209 => 170,  155 => 119,  102 => 69,  96 => 68,  84 => 59,  75 => 52,  72 => 51,  70 => 50,  61 => 43,  59 => 30,  56 => 29,  54 => 13,  50 => 12,  45 => 10,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">

<head>
  <meta charset=\"utf-8\"/>
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"/>
  <meta name=\"description\" content=\"\"/>
  <meta name=\"author\" content=\"\"/>
  <title>{% block title %}Facnote{% endblock %} </title>
  <!--favicon-->
  <link rel=\"icon\" href=\"{{ asset('assets/images/logo-icon.ico') }}\" type=\"image/x-icon\">
  {% block stylesheets %}
  <!-- Bootstrap core CSS-->
  <link href=\"{{ asset('assets/css/bootstrap.min.css') }}\" rel=\"stylesheet\"/>
  <!-- animate CSS-->
  <link href=\"{{ asset('assets/css/animate.css') }}\" rel=\"stylesheet\" type=\"text/css\"/>
  <!-- Icons CSS-->
  <link href=\"{{ asset('assets/css/icons.css') }}\" rel=\"stylesheet\" type=\"text/css\"/>
  <!-- Sidebar CSS-->
  <link href=\"{{ asset('assets/css/sidebar-menu.css') }}\" rel=\"stylesheet\"/>
  <!-- Custom Style-->
  <link href=\"{{ asset('assets/css/app-style.css') }}\" rel=\"stylesheet\"/>
 <!-- Enjoyhint for the Guide-->
<link href=\"{{ asset('assets/css/enjoyhint.css') }}\" rel=\"stylesheet\">
{# CHAT CSS #}
 <link href=\"{{ asset('bundles/chatsocket/css/style.css') }}\" rel=\"stylesheet\"/>
  {% endblock %}

  {% block javascripts %}
  <!-- Bootstrap core JavaScript-->
  <script src=\"{{ asset('assets/js/jquery.min.js') }}\" type=\"text/javascript\"></script>
  <script src=\"{{ asset('assets/js/popper.min.js') }}\" type=\"text/javascript\"></script>
  <script src=\"{{ asset('assets/js/bootstrap.min.js') }}\" type=\"text/javascript\"></script>
  <script src=\"{{ asset('assets/js/sidebar-menu.js') }}\" type=\"text/javascript\"></script>
  <script src=\"{{ asset('assets/js/app-script.js') }}\" type=\"text/javascript\"></script>

  <!-- Custom scripts -->
  <script src=\"{{ asset('assets/js/libs.js') }}\" type=\"text/javascript\"></script>
  <!-- Index js -->

  {% endblock %}



</head>

<body>
   
{% set userConnect = app.session.get('userConnect') %}
  {% set appBase = app.session.get('app_base') %}
<!-- Start wrapper-->
 <div id=\"wrapper\">
 
  <!--Start NAVIGATION-->
   <div id=\"sidebar-wrapper\" data-simplebar=\"\" data-simplebar-auto-hide=\"true\">
     <div class=\"brand-logo\">
      <a href=\"#\">
       <img src=\"{{ asset('assets/images/logo_facnote_v3.png') }}\" class=\"\" style=\"width: 200px;height: 55px;margin: 4px;\" alt=\"logo icon\">
       <h5 class=\"logo-text\"></h5>
     </a>
     </div>
     <ul class=\"sidebar-menu do-nicescrol\">
      <li class=\"sidebar-header\"> NAVIGATION</li>



      <li {% if classActive is defined and classActive==\"users\" %} class=\"active\" {% endif %} >
        <a href=\"{{ path('utilisateurs_user_homepage') }}\" class=\"waves-effect gui_collaborateur_step-1\">
          <i class=\"fa fa-user\"></i>
          <span>Collaborateurs</span>
        </a>
      </li>


      




    </ul>
     
   </div>
   <!--End NAVIGATION-->

<!--Start NAVIGATION TOP -->
<header class=\"topbar-nav\">
 <nav class=\"navbar navbar-expand fixed-top bg-white\">
  <ul class=\"navbar-nav mr-auto align-items-center\">
    <li class=\"nav-item\">
      <a class=\"nav-link toggle-menu\" href=\"javascript:void();\">
       <i class=\"icon-menu menu-icon\"></i>
     </a>
    </li>
    <li class=\"nav-item\">
      <form class=\"search-bar\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Entrer mot clé\">
         <a href=\"javascript:void();\"><i class=\"icon-magnifier\"></i></a>
      </form>
    </li>
  </ul>
     
  <ul class=\"navbar-nav align-items-center right-nav-link\">
    <a href=\"#\" style=\"color:black;\" class=\"gui_ecriture_step-0 gui_ecriture_step-9\"><li id=\"startTheGuide\"><i class=\"fa fa-question-circle-o fa-lg\" aria-hidden=\"true\"></i></li></a>
    <li class=\"nav-item dropdown-lg\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect\" data-toggle=\"dropdown\" href=\"javascript:void();\">
        <i class=\"icon-envelope-open\"></i>
        <!--<span class=\"badge badge-danger badge-up\">0</span>-->
      </a>
      <div class=\"dropdown-menu dropdown-menu-right\">
        <ul class=\"list-group list-group-flush\">
         <li class=\"list-group-item d-flex justify-content-between align-items-center\">
          Vous aves 0 nouveau messages
          <span class=\"badge badge-danger\">0</span>
          </li>
          <li class=\"list-group-item\">
          <a href=\"javaScript:void();\">
           <!--<div class=\"media\">
             <div class=\"avatar\"><img class=\"align-self-start mr-3\" src=\"{{ asset('assets/images/avatars/avatar.png') }}\" alt=\"user avatar\"></div>
            <div class=\"media-body\">
            <h6 class=\"mt-0 msg-title\">Jhon Deo</h6>
            <p class=\"msg-info\">Bla bla bla ...</p>
            <small>Mardi, 4:10 PM</small>
            </div>
          </div>-->
          </a>
          </li>
          <li class=\"list-group-item\"><a href=\"javaScript:void();\">Tous Messages</a></li>
        </ul>
        </div>
    </li>

    <li class=\"nav-item dropdown-lg\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect\" data-toggle=\"dropdown\" href=\"javascript:void();\">
      <i class=\"icon-bell\"></i>
     <!-- <span class=\"badge badge-primary badge-up\">0</span> -->
      </a>
      <div class=\"dropdown-menu dropdown-menu-right\">
        <ul class=\"list-group list-group-flush\">
          <li class=\"list-group-item d-flex justify-content-between align-items-center\">
          Vous avez 0 Notifications
         <span class=\"badge badge-primary\">0</span>
          </li>
          <li class=\"list-group-item\">
          <a href=\"javaScript:void();\">
           <!--<div class=\"media\">
             <i class=\"icon-people fa-2x mr-3 text-info\"></i>
            <div class=\"media-body\">
            <h6 class=\"mt-0 msg-title\">New Registered Users</h6>
            <p class=\"msg-info\">Lorem ipsum dolor sit amet...</p>
            </div>
          </div>-->
          </a>
          </li>
          <li class=\"list-group-item\"><a href=\"javaScript:void();\">Toutes Notifications</a></li>
        </ul>
      </div>
    </li>

    <li class=\"nav-item language\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect\" data-toggle=\"dropdown\" href=\"#\"><i class=\"flag-icon flag-icon-fr\"></i></a>
      <ul class=\"dropdown-menu dropdown-menu-right\">
          <li class=\"dropdown-item\"> <i class=\"flag-icon flag-icon-fr mr-2\"></i> Français</li>
          <li class=\"dropdown-item\"> <i class=\"flag-icon flag-icon-gb mr-2\"></i> Anglais</li>
        </ul>
    </li>

    <li class=\"nav-item\">
      <a class=\"nav-link dropdown-toggle dropdown-toggle-nocaret\" data-toggle=\"dropdown\" href=\"#\">
        <span class=\"user-profile\"><img src=\"{{ asset('assets/images/avatars/avatar.png') }}\" class=\"img-circle\" alt=\"user avatar\"></span>
      </a>
      <ul class=\"dropdown-menu dropdown-menu-right\">
       <li class=\"dropdown-item user-details\">
        <a href=\"javaScript:void();\">
           <div class=\"media\">
             <div class=\"avatar\"><img class=\"align-self-start mr-3\" src=\"{{ asset('assets/images/avatars/avatar.png') }}\" alt=\"user avatar\"></div>
            <div class=\"media-body\">              
            <h6 class=\"mt-2 user-title\">{{ userConnect.nom }}   {{ userConnect.prenom }}</h6>
            <p class=\"user-subtitle\">{{ userConnect.email }}</p>
            </div>
           </div>
          </a>
        </li>
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\"><i class=\"icon-envelope mr-2\"></i> Messages </li> 
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\" onClick=\"javascript:goToUrl('{{ path('utilisateurs_moncompte_homepage') }}')\"><i class=\"icon-wallet mr-2\"></i> Mon compte</li>
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\"><i class=\"icon-settings mr-2\"></i> Paramètres</li>
        <li class=\"dropdown-divider\"></li>
        <li class=\"dropdown-item\" onClick=\"javascript:goToUrl('{{ path('login_logout') }}')\"><i class=\"icon-power mr-2\"></i> Se déconnecter </li>
      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End NAVIGATION TOP -->
<body>
<div id=\"wrapper\">
    <div class=\"clearfix\"></div>
    
    <div class=\"content-wrapper\">

  <div id=\"errorMsgGuide\" class=\"displayNo alert\" role=\"alert\"></div>
      {% block body %}{% endblock %}

    </div><!--End content-wrapper-->

   <!--Start Back To Top Button-->
    <a href=\"javaScript:void();\" class=\"back-to-top\"><i class=\"fa fa-angle-double-up\"></i> </a>
    <!--End Back To Top Button-->
    <!--Start footer-->
    <footer class=\"footer\">
      <div class=\"container\">
        <div class=\"text-center\">
          Copyright © 2018 <a href=\"http://facnote.com\" target=\"_blank\"> www.facnote.com</a>
        </div>
      </div>
    </footer>
    <!--End footer-->

</div><!--End wrapper-->
{% block js_footer %}{% endblock %}
</body>
</html>
", "base.html.twig", "/Applications/MAMP/htdocs/facnotev3_banque/facnote_v3_banque/app/Resources/views/base.html.twig");
    }
}
