<?php

/* @UtilisateursUser/Users/index.html.twig */
class __TwigTemplate_8e6df71fb5122f6c2ac63a56fc203413c06979433d490bc853773c15360840b3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@UtilisateursUser/Users/index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Users/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Users/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo " Facnote - Collaborateurs ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <script src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/clickSave.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <!--Data Tables -->
  <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
  <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 13
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/clickSave.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>  
   <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/popper.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/buttons.html5.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/buttons.print.min.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js"), "html", null, true);
        echo "\"></script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 27
        echo "<div class=\"card\">
<div class=\"card-header\">
\t<div class=\"row\">
\t<div class=\"col-12 col-lg-6\">
      <button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light gui_collaborateur_step-2\" onClick=\"javascript:goToUrl('";
        // line 31
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_add");
        echo "');\" > <i class=\"fa fa-plus\"></i> <span>AJOUTER</span> </button>

       <!--<button type=\"button\" class=\"btn btn-outline-danger waves-effect waves-light gui_collaborateur_step-5\" onClick=\"\" > <i class=\"fa fa-trash-o\"></i> <span>SUPPRIMER</span> </button>-->

      <button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light gui_collaborateur_step-3\" onClick=\"javascript:goToUrl('";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_export_pdf");
        echo "');\"> <i class=\"fa fa fa-file-pdf-o\"></i> <span>PDF</span> </button>



      <button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light gui_collaborateur_step-4\" onClick=\"javascript:goToUrl('";
        // line 39
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_export_csv");
        echo "');\"> <i class=\"fa fa-file-excel-o\"></i> <span>EXCEL</span> </button>

  </div>
      
      <div class=\"col-12 col-lg-6\">

      ";
        // line 45
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", array(), "any", false, true), "get", array(0 => "flashMessageUser"), "method", true, true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 45, $this->source); })()), "session", array()), "get", array(0 => "flashMessageUser"), "method") != ""))) {
            // line 46
            echo "      <div class=\"alert alert-success alert-dismissible\" role=\"alert\" >
\t\t\t\t    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
\t\t\t\t    <div class=\"alert-icon\">
\t\t\t\t\t <i class=\"icon-check\"></i>
\t\t\t\t    </div>
\t\t\t\t    <div class=\"alert-message\">
\t\t\t\t      <span><strong>Success!</strong> ";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 52, $this->source); })()), "session", array()), "get", array(0 => "flashMessageUser"), "method"), "html", null, true);
            echo " </span>
\t\t\t\t    </div>
\t   ";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 54, $this->source); })()), "session", array()), "set", array(0 => "flashMessageUser", 1 => ""), "method"), "html", null, true);
            echo "
      </div>
  ";
        }
        // line 57
        echo "

    </div>

</div>
</div>
\t<div class=\"card-body\">
\t\t<div class=\"table-responsive\">

\t\t\t";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 66, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "flashMessage"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 67
            echo "\t\t\t    <div class=\"alert-message\">
\t\t\t              <span><strong>Succès!</strong> ";
            // line 68
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo " </span>
\t\t\t    </div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "

\t\t\t<div id=\"example_wrapper\">
\t\t\t<table id=\"usersList\" width=\"100%\" class=\"table table-striped table-bordered table-hover gui_collaborateur_step-6\" role=\"grid\" aria-describedby=\"users_info\">
\t\t\t<thead>
\t\t\t    <tr role=\"row\">
\t\t\t    \t<th style=\"width: 166px;\" class=\"sorting_asc\">Nom</th>
\t\t\t    \t<th style=\"width: 166px;\">Prénom</th>
\t\t\t    \t<th style=\"width: 166px;\">Email</th>
\t\t\t    \t<th style=\"width: 166px;\">Téléphone</th>
\t\t\t    \t<th style=\"width: 166px;\">Role</th>
\t\t\t    \t<th style=\"width: 166px;\">Connexion</th>
\t\t\t    \t<th style=\"width: 100px;\">Active</th>
\t\t\t    \t<th style=\"width: 100px;\">Actions</th>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 87
        if ((twig_length_filter($this->env, (isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 87, $this->source); })())) > 0)) {
            // line 88
            echo "\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 88, $this->source); })()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["u"]) {
                // line 89
                echo "\t\t\t    <tr role=\"row\" class=\"";
                if ((0 == twig_get_attribute($this->env, $this->source, $context["loop"], "index", array()) % 2)) {
                    echo " odd ";
                } else {
                    echo " even ";
                }
                echo "\"  >
\t\t\t        <td class=\"editable\" rel=\"";
                // line 90
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "idPersone", array()), "html", null, true);
                echo "-ent_persone-id-nom-required-text\"> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "nom", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td class=\"editable\" rel=\"";
                // line 91
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "idPersone", array()), "html", null, true);
                echo "-ent_persone-id-prenom-norequired-text\"> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "prenom", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td class=\"editable\" rel=\"";
                // line 92
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "idEmail", array()), "html", null, true);
                echo "-ent_email-id-mail-required-email\"> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "email", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td class=\"editable\" rel=\"";
                // line 93
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "idTelephone", array()), "html", null, true);
                echo "-ent_telephone-id-numero-norequired-text\"> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "telephonne", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td ";
                // line 94
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 94, $this->source); })()), "session", array()), "get", array(0 => "user_role"), "method") == 1)) {
                    echo " class=\"editable\" rel=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "id", array()), "html", null, true);
                    echo "-users-id-roleID-required-select-role-nom\" ";
                }
                // line 95
                echo "\t\t\t\t\t> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "roleUser", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t<td> ";
                // line 96
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["u"], "dernierConnexion", array()), "d/m/Y H:i:s"), "html", null, true);
                echo "</td>
\t\t\t\t\t<td align=\"center\"> 
\t\t\t\t\t\t";
                // line 98
                if ((twig_get_attribute($this->env, $this->source, $context["u"], "active", array()) == "1")) {
                    // line 99
                    echo "\t\t\t\t\t\t\t <span class=\"badge gradient-quepal text-white shadow\">Oui</span>
\t\t\t            ";
                } else {
                    // line 101
                    echo "\t\t\t            \t<span class=\"badge gradient-bloody text-white shadow\">Non</span>
\t\t\t            ";
                }
                // line 103
                echo "\t\t\t\t\t</td>
\t\t\t\t\t<td align=\"center\">

\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light m-1 btn_action gui_collaborateur_step-7\" onClick=\"javascript:goToUrl('";
                // line 106
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_edit", array("id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", array()))), "html", null, true);
                echo "');\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Modifier\"> <i class=\"fa fa-edit\"></i> </button>

\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-danger waves-effect waves-light m-1 btn_action gui_collaborateur_step-8\" onClick=\"javascript:confirmDelete('";
                // line 108
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("utilisateurs_user_delete", array("id" => twig_get_attribute($this->env, $this->source, $context["u"], "id", array()))), "html", null, true);
                echo "');\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Supprimer\" > <i class=\"fa fa-trash-o\"  title=\"Delete\" ></i> </button>
\t\t\t\t\t</td>
\t\t\t    </tr>
\t\t\t     ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['u'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo " 
\t\t\t     ";
        } else {
            // line 113
            echo "\t\t        <tr>
\t\t            <td colspan=\"6\">
\t\t                No data found !
\t\t            </td>

\t\t        </tr>
\t\t    ";
        }
        // line 120
        echo "\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t\t</div>
\t</div>
</div>
    <script>
     \$(document).ready(function() {
     \t\$('.containerChat').addClass('containerChat chatbox--tray');
     \t\$('[data-toggle=\"tooltip\"]').tooltip();
      //Default data table
           var langMap = {
        en: {
            path: 'English',
            mods: {
                sLengthMenu:     \"Show _MENU_ entries\",
            }
        },
        fr: {
            path: 'French',
            mods: {
                sLengthMenu: \"Afficher _MENU_ &eacute;l&eacute;ments\"
            }
        },
        gr: {
            path: 'German',
            mods: {
                sLengthMenu: \"_MENU_ Einträge anzeigen\"
            }
        }
    };

\tfunction getLanguage() {
\t  var lang = 'fr';
\t  var result = null;
\t  var path = \"";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("./assets/plugins/bootstrap-datatable/js/"), "html", null, true);
        echo "\";
\t  \$.ajax({
\t    async: false,  
\t    url: path + langMap[lang].path + '.json',
\t    success: function(obj) {
\t      result = \$.extend({}, obj, langMap[lang].mods)
\t    }
\t  })
\t  return result
\t}
var table = \$('#usersList').DataTable({
    buttons: [ 'colvis' ],
    order: [0],
    stateSave: true,
    columnDefs: [ { orderable: false, targets: [7] } ],
    language: getLanguage(),
  
});


table.buttons().container().appendTo( '#usersList_filter' );

\$(\"#usersList_filter div\").css({\"margin-left\":\"30px\", \"margin-right\":\"30px\"});
\$(\".buttons-colvis\").html('<i class=\"fa fa-navicon\"></i> COLONNES ');

// add classes guide 
\$(\"#usersList_length\").addClass('gui_collaborateur_step-91');
\$(\"#usersList_filter input\").addClass('gui_collaborateur_step-92');
\$(\"#usersList_filter div\").addClass('gui_collaborateur_step-93');
\$(\"#usersList_paginate ul\").addClass('gui_collaborateur_step-94');




} );

</script>

<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$(\".editable\").bind('click',function() {
            getEditable(this, '";
        // line 196
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("facnote_click_save");
        echo "', '";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("facnote_click_select");
        echo "' );
        });
    });
</script>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@UtilisateursUser/Users/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  464 => 196,  420 => 155,  383 => 120,  374 => 113,  370 => 111,  352 => 108,  347 => 106,  342 => 103,  338 => 101,  334 => 99,  332 => 98,  327 => 96,  322 => 95,  316 => 94,  310 => 93,  304 => 92,  298 => 91,  292 => 90,  283 => 89,  265 => 88,  263 => 87,  245 => 71,  236 => 68,  233 => 67,  229 => 66,  218 => 57,  212 => 54,  207 => 52,  199 => 46,  197 => 45,  188 => 39,  181 => 35,  174 => 31,  168 => 27,  159 => 26,  146 => 22,  142 => 21,  138 => 20,  134 => 19,  130 => 18,  126 => 17,  122 => 16,  118 => 15,  114 => 14,  109 => 13,  100 => 12,  88 => 9,  84 => 8,  79 => 6,  74 => 5,  65 => 4,  47 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %} Facnote - Collaborateurs {% endblock %}

{% block stylesheets %}
    {{ parent() }}
    <script src=\"{{ asset('js/clickSave.js') }}\" type=\"text/javascript\"></script>
    <!--Data Tables -->
  <link href=\"{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}\" rel=\"stylesheet\" type=\"text/css\">
  <link href=\"{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}\" rel=\"stylesheet\" type=\"text/css\">
{% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script src=\"{{ asset('js/clickSave.js') }}\" type=\"text/javascript\"></script>  
   <script src=\"{{ asset('assets/popper.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}\"></script>
  <script src=\"{{ asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}\"></script>

{% endblock %}

{% block body %}
<div class=\"card\">
<div class=\"card-header\">
\t<div class=\"row\">
\t<div class=\"col-12 col-lg-6\">
      <button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light gui_collaborateur_step-2\" onClick=\"javascript:goToUrl('{{ path('utilisateurs_user_add') }}');\" > <i class=\"fa fa-plus\"></i> <span>AJOUTER</span> </button>

       <!--<button type=\"button\" class=\"btn btn-outline-danger waves-effect waves-light gui_collaborateur_step-5\" onClick=\"\" > <i class=\"fa fa-trash-o\"></i> <span>SUPPRIMER</span> </button>-->

      <button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light gui_collaborateur_step-3\" onClick=\"javascript:goToUrl('{{ path('utilisateurs_user_export_pdf') }}');\"> <i class=\"fa fa fa-file-pdf-o\"></i> <span>PDF</span> </button>



      <button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light gui_collaborateur_step-4\" onClick=\"javascript:goToUrl('{{ path('utilisateurs_user_export_csv') }}');\"> <i class=\"fa fa-file-excel-o\"></i> <span>EXCEL</span> </button>

  </div>
      
      <div class=\"col-12 col-lg-6\">

      {% if app.session.get('flashMessageUser') is defined and app.session.get('flashMessageUser') != '' %}
      <div class=\"alert alert-success alert-dismissible\" role=\"alert\" >
\t\t\t\t    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
\t\t\t\t    <div class=\"alert-icon\">
\t\t\t\t\t <i class=\"icon-check\"></i>
\t\t\t\t    </div>
\t\t\t\t    <div class=\"alert-message\">
\t\t\t\t      <span><strong>Success!</strong> {{ app.session.get('flashMessageUser') }} </span>
\t\t\t\t    </div>
\t   {{ app.session.set('flashMessageUser', '') }}
      </div>
  {% endif %}


    </div>

</div>
</div>
\t<div class=\"card-body\">
\t\t<div class=\"table-responsive\">

\t\t\t{% for flashMessage in app.session.flashbag.get('flashMessage') %}
\t\t\t    <div class=\"alert-message\">
\t\t\t              <span><strong>Succès!</strong> {{ flashMessage }} </span>
\t\t\t    </div>
\t\t\t{% endfor %}


\t\t\t<div id=\"example_wrapper\">
\t\t\t<table id=\"usersList\" width=\"100%\" class=\"table table-striped table-bordered table-hover gui_collaborateur_step-6\" role=\"grid\" aria-describedby=\"users_info\">
\t\t\t<thead>
\t\t\t    <tr role=\"row\">
\t\t\t    \t<th style=\"width: 166px;\" class=\"sorting_asc\">Nom</th>
\t\t\t    \t<th style=\"width: 166px;\">Prénom</th>
\t\t\t    \t<th style=\"width: 166px;\">Email</th>
\t\t\t    \t<th style=\"width: 166px;\">Téléphone</th>
\t\t\t    \t<th style=\"width: 166px;\">Role</th>
\t\t\t    \t<th style=\"width: 166px;\">Connexion</th>
\t\t\t    \t<th style=\"width: 100px;\">Active</th>
\t\t\t    \t<th style=\"width: 100px;\">Actions</th>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t{% if users|length > 0 %}
\t\t\t\t{% for u in users %}
\t\t\t    <tr role=\"row\" class=\"{% if loop.index is divisible by(2)  %} odd {% else %} even {% endif %}\"  >
\t\t\t        <td class=\"editable\" rel=\"{{ u.idPersone }}-ent_persone-id-nom-required-text\"> {{ u.nom  }}</td>
\t\t\t\t\t<td class=\"editable\" rel=\"{{ u.idPersone }}-ent_persone-id-prenom-norequired-text\"> {{ u.prenom  }}</td>
\t\t\t\t\t<td class=\"editable\" rel=\"{{ u.idEmail }}-ent_email-id-mail-required-email\"> {{ u.email  }}</td>
\t\t\t\t\t<td class=\"editable\" rel=\"{{ u.idTelephone }}-ent_telephone-id-numero-norequired-text\"> {{ u.telephonne  }}</td>
\t\t\t\t\t<td {% if app.session.get('user_role') == 1 %} class=\"editable\" rel=\"{{ u.id }}-users-id-roleID-required-select-role-nom\" {% endif %}
\t\t\t\t\t> {{ u.roleUser  }}</td>
\t\t\t\t\t<td> {{ u.dernierConnexion|date('d/m/Y H:i:s')  }}</td>
\t\t\t\t\t<td align=\"center\"> 
\t\t\t\t\t\t{% if(u.active=='1') %}
\t\t\t\t\t\t\t <span class=\"badge gradient-quepal text-white shadow\">Oui</span>
\t\t\t            {% else %}
\t\t\t            \t<span class=\"badge gradient-bloody text-white shadow\">Non</span>
\t\t\t            {% endif %}
\t\t\t\t\t</td>
\t\t\t\t\t<td align=\"center\">

\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-primary waves-effect waves-light m-1 btn_action gui_collaborateur_step-7\" onClick=\"javascript:goToUrl('{{ path('utilisateurs_user_edit', { 'id': u.id } ) }}');\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Modifier\"> <i class=\"fa fa-edit\"></i> </button>

\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-outline-danger waves-effect waves-light m-1 btn_action gui_collaborateur_step-8\" onClick=\"javascript:confirmDelete('{{ path('utilisateurs_user_delete', { 'id': u.id } ) }}');\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"Supprimer\" > <i class=\"fa fa-trash-o\"  title=\"Delete\" ></i> </button>
\t\t\t\t\t</td>
\t\t\t    </tr>
\t\t\t     {% endfor %} 
\t\t\t     {% else %}
\t\t        <tr>
\t\t            <td colspan=\"6\">
\t\t                No data found !
\t\t            </td>

\t\t        </tr>
\t\t    {% endif %}
\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t\t</div>
\t</div>
</div>
    <script>
     \$(document).ready(function() {
     \t\$('.containerChat').addClass('containerChat chatbox--tray');
     \t\$('[data-toggle=\"tooltip\"]').tooltip();
      //Default data table
           var langMap = {
        en: {
            path: 'English',
            mods: {
                sLengthMenu:     \"Show _MENU_ entries\",
            }
        },
        fr: {
            path: 'French',
            mods: {
                sLengthMenu: \"Afficher _MENU_ &eacute;l&eacute;ments\"
            }
        },
        gr: {
            path: 'German',
            mods: {
                sLengthMenu: \"_MENU_ Einträge anzeigen\"
            }
        }
    };

\tfunction getLanguage() {
\t  var lang = 'fr';
\t  var result = null;
\t  var path = \"{{ asset('./assets/plugins/bootstrap-datatable/js/') }}\";
\t  \$.ajax({
\t    async: false,  
\t    url: path + langMap[lang].path + '.json',
\t    success: function(obj) {
\t      result = \$.extend({}, obj, langMap[lang].mods)
\t    }
\t  })
\t  return result
\t}
var table = \$('#usersList').DataTable({
    buttons: [ 'colvis' ],
    order: [0],
    stateSave: true,
    columnDefs: [ { orderable: false, targets: [7] } ],
    language: getLanguage(),
  
});


table.buttons().container().appendTo( '#usersList_filter' );

\$(\"#usersList_filter div\").css({\"margin-left\":\"30px\", \"margin-right\":\"30px\"});
\$(\".buttons-colvis\").html('<i class=\"fa fa-navicon\"></i> COLONNES ');

// add classes guide 
\$(\"#usersList_length\").addClass('gui_collaborateur_step-91');
\$(\"#usersList_filter input\").addClass('gui_collaborateur_step-92');
\$(\"#usersList_filter div\").addClass('gui_collaborateur_step-93');
\$(\"#usersList_paginate ul\").addClass('gui_collaborateur_step-94');




} );

</script>

<script type=\"text/javascript\">
    \$(document).ready(function () {
        \$(\".editable\").bind('click',function() {
            getEditable(this, '{{ path('facnote_click_save') }}', '{{ path('facnote_click_select') }}' );
        });
    });
</script>


{% endblock %}


", "@UtilisateursUser/Users/index.html.twig", "/Applications/MAMP/htdocs/facnotev3_banque/facnote_v3_banque/src/Utilisateurs/UserBundle/Resources/views/Users/index.html.twig");
    }
}
