<?php

/* @UtilisateursUser/Login/login.html.twig */
class __TwigTemplate_791ba08757f339d3e241abdc2677c8ef0a0a21fa4bba7b44f5a2a0a5e1c464c1 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base_login.html.twig", "@UtilisateursUser/Login/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base_login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Login/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@UtilisateursUser/Login/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t<div class=\"card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown\">
\t\t<div class=\"card-body\">
\t\t\t ";
        // line 6
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", array(), "any", false, true), "get", array(0 => "flashMessageLoginError"), "method", true, true) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 6, $this->source); })()), "session", array()), "get", array(0 => "flashMessageLoginError"), "method") != ""))) {
            // line 7
            echo "\t\t\t  <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
\t\t\t   <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
\t\t\t    <div class=\"alert-icon\"><i class=\"icon-close\"></i></div>
\t\t\t    <div class=\"alert-message\">
\t\t\t      <span><strong>Erreur!</strong> ";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 11, $this->source); })()), "session", array()), "get", array(0 => "flashMessageLoginError"), "method"), "html", null, true);
            echo "</span>
\t\t\t      ";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 12, $this->source); })()), "session", array()), "set", array(0 => "flashMessageLoginError", 1 => ""), "method"), "html", null, true);
            echo "
\t\t\t    </div>
\t\t\t  </div>
\t\t\t  ";
        }
        // line 16
        echo "

\t\t <div class=\"card-content p-2\">
\t\t \t<div class=\"text-center\">
\t\t \t\t<img src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/images/logo_facnote_login.png"), "html", null, true);
        echo "\">
\t\t \t</div>
\t\t  <div class=\"card-title text-uppercase text-center py-3\"> Connectez-vous  </div>
\t\t    <form action=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login_check");
        echo "\" method=\"post\">
\t\t\t  <div class=\"form-group\">
\t\t\t   <div class=\"position-relative has-icon-right\">
\t\t\t\t  <label for=\"username\" class=\"sr-only\">Login</label>
\t\t\t\t  <input type=\"text\" name=\"username\" id=\"username\" class=\"form-control form-control-rounded\" placeholder=\"Votre adresse email\" required=\"required\">
\t\t\t\t  <div class=\"form-control-position\">
\t\t\t\t\t  <i class=\"icon-user\"></i>
\t\t\t\t  </div>
\t\t\t   </div>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t   <div class=\"position-relative has-icon-right\">
\t\t\t\t  <label for=\"password\" class=\"sr-only\">Mot de passe</label>
\t\t\t\t  <input type=\"password\" name=\"password\" id=\"password\" class=\"form-control form-control-rounded\" placeholder=\"Votre mot de passe\" required=\"required\">
\t\t\t\t  <div class=\"form-control-position\">
\t\t\t\t\t  <i class=\"icon-lock\"></i>
\t\t\t\t  </div>
\t\t\t   </div>
\t\t\t  </div>
\t\t\t<div class=\"form-row mr-0 ml-0\">
\t\t\t <div class=\"form-group col-6\">
\t\t\t   <div class=\"demo-checkbox\">
                <input type=\"checkbox\" id=\"user-checkbox\" class=\"filled-in chk-col-primary\" checked=\"\" />
                <label for=\"user-checkbox\">Se souvenir de moi</label>
\t\t\t  </div>
\t\t\t </div>
\t\t\t <div class=\"form-group col-6 text-right\">
\t\t\t  <a href=\"";
        // line 50
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login_reset");
        echo "\">Mot de passe oublié ?</a>
\t\t\t </div>
\t\t\t</div>
\t\t\t <button type=\"submit\" class=\"btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light\">Se connecter</button>
\t\t\t  
\t\t\t </form>
\t\t   </div>
\t\t  </div>
\t     </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@UtilisateursUser/Login/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 50,  88 => 23,  82 => 20,  76 => 16,  69 => 12,  65 => 11,  59 => 7,  57 => 6,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base_login.html.twig' %}
 
{% block body %}
\t<div class=\"card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown\">
\t\t<div class=\"card-body\">
\t\t\t {% if app.session.get('flashMessageLoginError') is defined and app.session.get('flashMessageLoginError') != '' %}
\t\t\t  <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">
\t\t\t   <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
\t\t\t    <div class=\"alert-icon\"><i class=\"icon-close\"></i></div>
\t\t\t    <div class=\"alert-message\">
\t\t\t      <span><strong>Erreur!</strong> {{ app.session.get('flashMessageLoginError') }}</span>
\t\t\t      {{ app.session.set('flashMessageLoginError', '') }}
\t\t\t    </div>
\t\t\t  </div>
\t\t\t  {% endif %}


\t\t <div class=\"card-content p-2\">
\t\t \t<div class=\"text-center\">
\t\t \t\t<img src=\"{{ asset('assets/images/logo_facnote_login.png') }}\">
\t\t \t</div>
\t\t  <div class=\"card-title text-uppercase text-center py-3\"> Connectez-vous  </div>
\t\t    <form action=\"{{ path('login_check') }}\" method=\"post\">
\t\t\t  <div class=\"form-group\">
\t\t\t   <div class=\"position-relative has-icon-right\">
\t\t\t\t  <label for=\"username\" class=\"sr-only\">Login</label>
\t\t\t\t  <input type=\"text\" name=\"username\" id=\"username\" class=\"form-control form-control-rounded\" placeholder=\"Votre adresse email\" required=\"required\">
\t\t\t\t  <div class=\"form-control-position\">
\t\t\t\t\t  <i class=\"icon-user\"></i>
\t\t\t\t  </div>
\t\t\t   </div>
\t\t\t  </div>
\t\t\t  <div class=\"form-group\">
\t\t\t   <div class=\"position-relative has-icon-right\">
\t\t\t\t  <label for=\"password\" class=\"sr-only\">Mot de passe</label>
\t\t\t\t  <input type=\"password\" name=\"password\" id=\"password\" class=\"form-control form-control-rounded\" placeholder=\"Votre mot de passe\" required=\"required\">
\t\t\t\t  <div class=\"form-control-position\">
\t\t\t\t\t  <i class=\"icon-lock\"></i>
\t\t\t\t  </div>
\t\t\t   </div>
\t\t\t  </div>
\t\t\t<div class=\"form-row mr-0 ml-0\">
\t\t\t <div class=\"form-group col-6\">
\t\t\t   <div class=\"demo-checkbox\">
                <input type=\"checkbox\" id=\"user-checkbox\" class=\"filled-in chk-col-primary\" checked=\"\" />
                <label for=\"user-checkbox\">Se souvenir de moi</label>
\t\t\t  </div>
\t\t\t </div>
\t\t\t <div class=\"form-group col-6 text-right\">
\t\t\t  <a href=\"{{ path('login_reset') }}\">Mot de passe oublié ?</a>
\t\t\t </div>
\t\t\t</div>
\t\t\t <button type=\"submit\" class=\"btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light\">Se connecter</button>
\t\t\t  
\t\t\t </form>
\t\t   </div>
\t\t  </div>
\t     </div>
{% endblock %}", "@UtilisateursUser/Login/login.html.twig", "/Applications/MAMP/htdocs/facnote_mirah/sf_facnote/src/Utilisateurs/UserBundle/Resources/views/Login/login.html.twig");
    }
}
