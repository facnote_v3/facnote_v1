<?php

namespace Entities\EntityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('EntitiesEntityBundle:Default:index.html.twig');
    }
}
