<?php

namespace Entities\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntSociete
 *
 * @ORM\Table(name="ent_societe")
 * @ORM\Entity(repositoryClass="Entities\EntityBundle\Repository\EntSocieteRepository")
 */
class EntSociete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="raison_sociale", type="string", length=255)
     */
    private $raisonSociale;

    /**
     * @var string
     *
     * @ORM\Column(name="siren", type="string", length=255, nullable=true)
     */
    private $siren;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_tva", type="string", length=255, nullable=true)
     */
    private $numeroTva;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="ape", type="string", length=255, nullable=true)
     */
    private $ape;

    /**
     * @var string
     *
     * @ORM\Column(name="rcs", type="string", length=255, nullable=true)
     */
    private $rcs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_immatriculation", type="datetime", nullable=true)
     */
    private $dateImmatriculation;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="string", length=255, nullable=true)
     */
    private $capital;

    /**
     * @var string
     *
     * @ORM\Column(name="site_web", type="string", length=255, nullable=true)
     */
    private $siteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_agrement_formateur", type="string", length=255, nullable=true)
     */
    private $numeroAgrementFormateur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set raisonSociale
     *
     * @param string $raisonSociale
     *
     * @return EntSociete
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    /**
     * Get raisonSociale
     *
     * @return string
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * Set siren
     *
     * @param string $siren
     *
     * @return EntSociete
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren
     *
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * Set numeroTva
     *
     * @param string $numeroTva
     *
     * @return EntSociete
     */
    public function setNumeroTva($numeroTva)
    {
        $this->numeroTva = $numeroTva;

        return $this;
    }

    /**
     * Get numeroTva
     *
     * @return string
     */
    public function getNumeroTva()
    {
        return $this->numeroTva;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return EntSociete
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set ape
     *
     * @param string $ape
     *
     * @return EntSociete
     */
    public function setApe($ape)
    {
        $this->ape = $ape;

        return $this;
    }

    /**
     * Get ape
     *
     * @return string
     */
    public function getApe()
    {
        return $this->ape;
    }

    /**
     * Set rcs
     *
     * @param string $rcs
     *
     * @return EntSociete
     */
    public function setRcs($rcs)
    {
        $this->rcs = $rcs;

        return $this;
    }

    /**
     * Get rcs
     *
     * @return string
     */
    public function getRcs()
    {
        return $this->rcs;
    }

    /**
     * Set dateImmatriculation
     *
     * @param \DateTime $dateImmatriculation
     *
     * @return EntSociete
     */
    public function setDateImmatriculation($dateImmatriculation)
    {
        $this->dateImmatriculation = $dateImmatriculation;

        return $this;
    }

    /**
     * Get dateImmatriculation
     *
     * @return \DateTime
     */
    public function getDateImmatriculation()
    {
        return $this->dateImmatriculation;
    }

    /**
     * Set capital
     *
     * @param string $capital
     *
     * @return EntSociete
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return EntSociete
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * Set numeroAgrementFormateur
     *
     * @param string $numeroAgrementFormateur
     *
     * @return EntSociete
     */
    public function setNumeroAgrementFormateur($numeroAgrementFormateur)
    {
        $this->numeroAgrementFormateur = $numeroAgrementFormateur;

        return $this;
    }

    /**
     * Get numeroAgrementFormateur
     *
     * @return string
     */
    public function getNumeroAgrementFormateur()
    {
        return $this->numeroAgrementFormateur;
    }

}

