<?php

namespace Entities\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntAdresse
 *
 * @ORM\Table(name="ent_adresse")
 * @ORM\Entity(repositoryClass="Entities\EntityBundle\Repository\EntAdresseRepository")
 */
class EntAdresse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=355, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="code_postale", type="string", length=10, nullable=true)
     */
    private $codePostale;

    /**
     * @var \Crm\ClientBundle\Entity\Pays
     * @ORM\ManyToOne(targetEntity="\Crm\ClientBundle\Entity\Pays")
     * @ORM\JoinColumn(name="id_pays", referencedColumnName="id")
     * 
     */
    private $idPays;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return EntAdresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return EntAdresse
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set codePostale
     *
     * @param string $codePostale
     *
     * @return EntAdresse
     */
    public function setCodePostale($codePostale)
    {
        $this->codePostale = $codePostale;

        return $this;
    }

    /**
     * Get codePostale
     *
     * @return string
     */
    public function getCodePostale()
    {
        return $this->codePostale;
    }

    /**
     * Set idPays
     *
     * @param \Crm\ClientBundle\Entity\Pays $idPays
     *
     * @return EntAdresse
     */
    public function setIdPays(\Crm\ClientBundle\Entity\Pays $idPays = null)
    {
        $this->idPays = $idPays;

        return $this;
    }

    /**
     * Get idPays
     *
     * @return \Crm\ClientBundle\Entity\Pays
     */
    public function getIdPays()
    {
        return $this->idPays;
    }

}

