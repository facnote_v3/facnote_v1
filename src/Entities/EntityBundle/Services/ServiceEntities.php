<?php
namespace Entities\EntityBundle\Services;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\Session\Session;
use Entities\EntityBundle\Entity\EntSociete;
use Entities\EntityBundle\Entity\EntAdresse;
use Entities\EntityBundle\Entity\EntPersone;
use Entities\EntityBundle\Entity\EntEmail;
use Entities\EntityBundle\Entity\EntTelephone;
use Entities\EntityBundle\Entity\EntFiles;
use Crm\ContactBundle\Entity\Contact;
use Tracabilite\TraceBundle\Services\Trace;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ServiceEntities
{   

    private $doctrine;
    private $sessionManager;

    public function __construct(Registry $doctrine, Session $session) {      
        $this->doctrine = $doctrine;
        $this->sessionManager = $session;
    }

    public function addSociete($id,$raisonSociale,$numeroTva,$siret,$codeApe,$capital,$rcs,$siteWeb,$numeroAgrementFormateur,$siren,$dateImmatriculation){

        $em = $this->doctrine->getEntityManager();

        if($id>0){
            $societeEnt = $em->getRepository('EntitiesEntityBundle:EntSociete')->find($id);
        } else {
            $societeEnt = new EntSociete();
        }

        $societeEnt->setRaisonSociale($raisonSociale);
        $societeEnt->setSiren($siren);
        $societeEnt->setSiret($siret);
        $societeEnt->setNumeroTva($numeroTva); 
        $societeEnt->setApe($codeApe);
        $societeEnt->setRcs($rcs);
        $societeEnt->setCapital($capital);
        $societeEnt->setSiteWeb($siteWeb);
        $societeEnt->setNumeroAgrementFormateur($numeroAgrementFormateur);

        $em->persist($societeEnt);
        $em->flush();

        return $societeEnt;

    }

    public function addPersone($id,$nom,$prenom,$civilite, $dateNaissaince, $fonction = '')
    {

        $em = $this->doctrine->getEntityManager();

        if($id>0){
            $persone = $em->getRepository('EntitiesEntityBundle:EntPersone')->find($id);
        } else {
            $persone = new EntPersone();
        }

        $persone->setNom($nom);
        $persone->setPrenom($prenom);
        $persone->setCivilite($civilite);
        if(isset($fonction) AND $fonction != '')
        $persone->setFonction($fonction);
        //$persone->setDateNaissance($dateNaissaince); 

        $em->persist($persone);
        $em->flush();

        return $persone;

    }

    public function addAdresse($id,$adr,$ville,$cp, $idPays)
    {
        if($idPays == '') $idPays = 1;
        $em = $this->doctrine->getEntityManager();

        if($id>0){
            $adresse = $em->getRepository('EntitiesEntityBundle:EntAdresse')->find($id);
        } else {
            $adresse = new EntAdresse();
        }


        $adresse->setAdresse($adr);
        $adresse->setVille($ville);
        $adresse->setCodePostale($cp);
        $pays = $em->getRepository('CrmClientBundle:Pays')->find($idPays);
        $adresse->setIdPays($pays); 

        $em->persist($adresse);
        $em->flush();

        return $adresse;

    }


    public function addEmail($id,$mail,$type){

        $em = $this->doctrine->getEntityManager();
        if($id>0){
            $email = $em->getRepository('EntitiesEntityBundle:EntEmail')->find($id);
        } else {
            $email = new EntEmail();
        }

        $email->setMail($mail);
        $email->setType($type);

        $em->persist($email);
        $em->flush();

        return $email;

    }


    public function addPhone($id,$tel,$type){

        $em = $this->doctrine->getEntityManager();
        if($id>0){
            $phone = $em->getRepository('EntitiesEntityBundle:EntTelephone')->find($id);
        } else {
            $phone = new EntTelephone();
        }

        $phone->setNumero($tel);
        $phone->setType($type);

        $em->persist($phone);
        $em->flush();

        return $phone;
    }

    public function addContact($id,$idSociete,$persone,$email,$phone,$mobile,$type){

        $em = $this->doctrine->getEntityManager();
        if($id>0){
            $contact = $em->getRepository('CrmContactBundle:Contact')->find($id);
        } else {
            $contact = new Contact();
        }

        $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($idSociete);
        $contact->setIdSociete($societe);
        $contact->setIdPersone($persone);
        $contact->setIdEmail($email);
        $contact->setIdTelFixe($phone);
        $contact->setIdTelMobile($mobile);
        $contact->setSource($type);

        $em->persist($contact);
        $em->flush();

        return $contact;
    }

    /**
     * Ajouter/Editer les contacts d'une societe table contact
     *
     */
    public function addContacts($idSociete,$contacts){

        $em = $this->doctrine->getEntityManager();

        $em->getRepository('CrmContactBundle:Contact')->deleteContactsSociete($idSociete);

        foreach ($contacts as $key => $c) {
            //print_r($c);exit;
        $idPersone = $this->addPersone('',$c['firstname'],$c['lastname'],$c['title'],'');
        $idEmail = $this->addEmail('',$c['email'],'societe');
        $idFixe = $this->addPhone('',$c['phone'],'fixe');
        $idPhone = $this->addPhone('',$c['mobile'],'mobile');
        $idContact = $this->addContact('',$idSociete,$idPersone,$idEmail,$idPhone,$idFixe,'societe');
        }
    }

    /**
    * Service upload photo
    *
    */

    public function addFile($dir,$image,$extensions,$idElement,$type, $base = '') {
        $erreur = 0;
        $flashMsg = "";
        $filename = "";
        $idFile = 0; 

        $extensions = array('.jpg','.jpeg','.png','.pdf','.csv','.doc','.xls','.docx','.xlsx','.numbers','.pages');

        if(($image instanceof UploadedFile) and ($image->getError()=='0'))
        {
            $originalName = $image->getClientOriginalName();
            $extension = strrchr($originalName, '.');
            $extension = strtolower($extension);
            $session = new Session();
            $app_base = $session->get('app_base');

            if (strlen($base) > 2 ) {
                $app_base = $base;
            }

            if(in_array($extension, $extensions)) {
                // taile de fichier 
                $taille_fichier = $image->getClientSize();
                if($taille_fichier > 5120000 ) { // taille max 5M
                    $erreur = 1;
                    $flashMsg="Taille de fichier ne doit pas dépasée 5M ";
                } else {

                    $em = $this->doctrine->getEntityManager();
                    $file = new EntFiles();
                    $file->setNom($originalName);
                    //$file->setUrl($dir.'/'.$filename);
                    $file->setBase($app_base);
                    $file->setType($type);
                    $file->setDateCreation(new \DateTime());
                    $em->persist($file);
                    $em->flush();

                    $idFile = $file;
                    $idFileValue = $file->getId();
                    $filename = date('Ym')."_".$idFileValue.$extension;
                    $image->move($dir, $filename);
                    $file->setUrl($filename);
                    $em->persist($file);
                    $em->flush();

                }
            } else {
                $erreur = 1;
                $flashMsg="Formats acceptés: ".implode(",",$extensions);
            }
        } else {
            $erreur = 1;
            $flashMsg="Problème au niveau de téléchargement de fichier ";   
        }
        return array('erreur'=>$erreur,'msgErreur' =>$flashMsg, 'idFile' => $idFile);
    }

}