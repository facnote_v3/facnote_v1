<?php

namespace Support\TicketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Tracabilite\TraceBundle\Services\Trace;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Support\TicketBundle\Entity\Ticket;
use Support\TicketBundle\Entity\Ticket_message;
use Support\TicketBundle\Entity\TicketStats;
use Support\TicketBundle\Form\Ticket_messageType;
use Support\TicketBundle\Form\TicketType;
//use Ecritures\EcrituresBundle\Entity\SaveCollones;
class TicketController extends Controller
{
    public function indexAction($status='sansRegle', $id=null, $idTicket=null, $regle=null, $regleOpen=null, Request $request)
    {
      if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));
        
        $session = new Session();        
        $app_base = $session->get('app_base');
        date_default_timezone_set('Europe/Paris');
        $idUserConnect = $session->get('user_id');
        $filterByUser=null;
        $dateLivraison=null;
        $em = $this->getDoctrine()->getManager();
        $savedlengthPages='null';
        $savedcurrentPage='null';
        $searchTable='';
        $tri='';
        $laColumn='';
        $leTrie='';
        $hiddenColumns='';
       /*
       $savedlengthPages='null';
       $lengthPages = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/lengthPages'));
          if (!empty($lengthPages) AND !empty($lengthPages[0]->getCollones())) {
              $savedlengthPages = $lengthPages[0]->getCollones();
          }
          else{
              $savedlengthPages='null';
          }
       $currentPage = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/pages'));
          if (!empty($currentPage) AND !empty($currentPage[0]->getCollones())) {
              $savedcurrentPage = $currentPage[0]->getCollones();
          }
          else{
              $savedcurrentPage='null';
          }
       $searchTableResult = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/search'));
          if (!empty($searchTableResult) AND !empty($searchTableResult[0]->getCollones())) {
              $searchTable = $searchTableResult[0]->getCollones();
          }
          else{
              $searchTable='';
          }
       $tri = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/tri'));
          if (!empty($tri) AND !empty($tri[0]->getCollones())) {
              $tri = $tri[0]->getCollones();
              $order = explode(",", $tri);
              $laColumn=$order[0];
              $leTrie=$order[1];
          }
          else{
              $tri='';
              $laColumn='';
              $leTrie='';
          }
        $hiddenColumn = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/hiddenColumn'));
          if (!empty($hiddenColumn) OR $hiddenColumn==0 AND !empty($hiddenColumn[0]->getCollones())) {
              $hiddenColumns = $hiddenColumn[0]->getCollones();
          }
          else{
              $hiddenColumns='';
          }
         */


    if ($session->get('user_role')!==1) {
            $filterByUser=$idUserConnect;

          }
      $listTickets = $em->getRepository('SupportTicketBundle:Ticket')->getListTickets($status,$filterByUser);
      //dump($listTickets);
    foreach ($listTickets as $key => $value) {
      if($value->getBase()===null && $filterByUser!==null)
      {
        $value->setBase($app_base);
        $em->persist($value);
        $em->flush();
        $listTickets[$key]=$value;
      }
      $ticketId=$value->getId();
      $lastMessage = $em->getRepository('SupportTicketBundle:Ticket')->getLastMessage($ticketId);
      $listTickets[$key]->LastMessage=$lastMessage;
      
    }
    //dump($listTickets);
      // $UserByRole = $em->getRepository('SupportTicketBundle:Ticket')->getByRole($app_base);
    
      $UserByRole = $em->getRepository('UtilisateursUserBundle:Users')->findBy(
      array('roleid' => 1,'supprimer' => 0));

      $lastAdminCharged = $em->getRepository('SupportTicketBundle:Ticket')->lastAdminCharged($status,$filterByUser);

      $random=rand (0,count($UserByRole)-1);
      if (!empty($lastAdminCharged) AND !empty($UserByRole)) {
        while ($lastAdminCharged[0]->getAdmincharged() == $UserByRole[$random]) {
        $random=rand (0,count($UserByRole)-1);
        
        }
      }

      $User = $em->getRepository('UtilisateursUserBundle:Users')->findBy(
      array('id' => $idUserConnect,'supprimer' => 0));
      // $User = $em->getRepository('UtilisateursUserBundle:Users')->find($idUserConnect);
      if (isset($idTicket)) {
        $TicketInstance = $em->getRepository('SupportTicketBundle:Ticket')->find($idTicket);
        if($TicketInstance)
        {
          if($app_base=="FA0001" && $TicketInstance->getTicketLu()===false)
          {
            $TicketInstance->setTicketLu(true);
            $em->persist($TicketInstance);
            $em->flush();
          }
        }
      }
    if (isset($regleOpen) AND $regleOpen=='regleOpen' AND ($id==$idUserConnect  OR $session->get('user_role')==1 )) {
            if ($session->get('user_role')==1) {
              
            $TicketInstance->setStatus('attenteClient');
            }
            else
            {
            $TicketInstance->setStatus('attenteAdmin');
            $TicketInstance->setTicketLu(false);
            }
            $em->persist($TicketInstance);
            $em->flush();
    }
    if (isset($regle) AND $regle=='regle' AND ($id==$idUserConnect  OR $session->get('user_role')==1 )) {
            
            $TicketInstance->setStatus('regle');
            $TicketInstance->setTicketLu(true);
            $TicketInstance->setDateresolu(new \DateTime());
            $em->persist($TicketInstance);
            $em->flush();

       //      if ($session->get('user_role')==1) {
       // $emailClient=$TicketInstance->getUserId()->getIdEmail()->getMail();
       //      $message = \Swift_Message::newInstance()
       //          ->setContentType('text/html')
       //          ->setCharset('utf-8')
       //          ->setReplyTo('support@facnote')
       //          ->setSubject('FacNote : Ticket Résolu')
       //          ->setFrom(array('support@facnote' => 'FacNote'))
       //          ->setTo($emailClient)
       //          ->setBody(
       //              $this->renderView(
       //                  '@SupportTicket/Ticket/emailResolu.html.twig', array(
       //                      //'username' => $_username,
       //                      'responsePath' => $request->getScheme() . '://' . $request->getHttpHost(). $this->generateUrl('support_ticket_arguments', array('id' => $id,'idTicket' => $idTicket)),
       //                  )
       //              )
       //          )
       //        ;
       //        $this->get('mailer')->send($message);

       //   }
      }
    if (isset($regle) AND $regle=='urgent' AND ($id==$idUserConnect  OR $session->get('user_role')==1 ) AND $TicketInstance->getStatus()!=='regle') {
            
            $TicketInstance->setStatus('urgent');
            $em->persist($TicketInstance);
            $em->flush();
      }

    $miseEnDev = $request->request->get('miseEnDev');
    
    if (!empty($miseEnDev) AND $session->get('user_role')==1 ) {
      
      $time= date('H:i:s');

      // $miseEnDev=$miseEnDev.' '.$time;
      // $miseEnDev= new \DateTime($miseEnDev);

        $datetime= new \DateTime();
      $miseEnDev = $datetime->createFromFormat('d/m/Y H:i:s', $miseEnDev.' '.$time);

      $TicketStat = $em->getRepository('SupportTicketBundle:TicketStats')->findByTicketId($idTicket);

      if (!empty($TicketStat[0])) {
       $TicketStat[0]->setMiseEnDev($miseEnDev);

      $em->persist($TicketStat[0]);
      $em->flush();

      $this->get('session')->set('flashMessageUser', 'Date de mise sur serveur de développement mis à jour');
      }
    }
    $miseEnProd = $request->request->get('miseEnProd');
    
    if (!empty($miseEnProd) AND $session->get('user_role')==1 ) {
      
      $time= date('H:i:s');
      // $miseEnProd=$miseEnProd.' '.$time;
      // $miseEnProd= new \DateTime($miseEnProd);


      $datetime= new \DateTime();
      $miseEnProd = $datetime->createFromFormat('d/m/Y H:i:s', $miseEnProd.' '.$time);
      
      $TicketStat = $em->getRepository('SupportTicketBundle:TicketStats')->findByTicketId($idTicket);

      if (!empty($TicketStat[0])) {
       $TicketStat[0]->setMiseEnProd($miseEnProd);

      $em->persist($TicketStat[0]);
      $em->flush();

      $this->get('session')->set('flashMessageUser', 'Date de mise sur serveur de production mis à jour');
      }
    }

    $dateLivraison = $request->request->get('dateLivraison');

    if (!empty($dateLivraison) AND $session->get('user_role')==1 ) {
        // $dateLivraison= new \DateTime($dateLivraison);

        $datetime= new \DateTime();
      $dateLivraison = $datetime->createFromFormat('d/m/Y', $dateLivraison);

        // dump($dateLivraison);die();

          $TicketInstance->setDatelivraison($dateLivraison);

          $Ticket_message = new Ticket_message();

            $Ticket_message->setTicketId($TicketInstance);

            $UserResponse = $em->getRepository('UtilisateursUserBundle:Users')->find($idUserConnect);
            
            $Ticket_message->setUserResponse($UserResponse);

            $dateLi=$dateLivraison->format('d/m/Y');

            $LivraisonMessage='Votre demande est prise en compte. La date prévisionnelle de livraison est le '.$dateLi;
            // dump($LivraisonMessage);die();
            $Ticket_message->setMessage($LivraisonMessage);

            $TicketStat = $em->getRepository('SupportTicketBundle:TicketStats')->findByTicketId($idTicket);

            if (!empty($TicketStat[0])) {
             $TicketStat[0]->setDernierMsg(new \DateTime());

            $em->persist($TicketStat[0]);
            $em->flush();
            }

          if ($session->get('user_role')==1) {
            $Ticket_message->setAdmin(1);
            }
            else
            {
              $Ticket_message->setAdmin(3);
            }
               $Ticket_message->setDate(new \DateTime());
          $em->persist($Ticket_message);


          $em->persist($TicketInstance);
          $em->flush();

      $this->get('session')->set('flashMessageUser', 'Date de livraison mis à jour');
      }
      elseif(isset($TicketInstance))
      {
        $dateLivraison = $TicketInstance->getDatelivraison();
      }

    $changeAdminCharge = $request->request->get('changeAdminCharge');
    if (isset($changeAdminCharge) AND $session->get('user_role')==1 ) {

      // $UserAdmin = $em->getRepository('UtilisateursUserBundle:Users')->findBy(array('id' => $changeAdminCharge,'supprimer' => 0));
      $UserAdmin = $em->getRepository('UtilisateursUserBundle:Users')->find($changeAdminCharge);


      $TicketInstance->setAdmincharged($UserAdmin);
      $em->persist($TicketInstance);
      $em->flush();
    }

    if (isset($id) AND ($id==$idUserConnect OR $session->get('user_role')==1 ) AND isset($idTicket)) {
      
    $Ticket_message = new Ticket_message();
    $formMessage  = $this->get('form.factory')->create(Ticket_messageType::
    class, $Ticket_message);

   if($request->isMethod('POST')) {
         $formMessage->handleRequest($request);
         if($formMessage->isSubmitted() && $formMessage->isValid()) {


    $TicketStat = $em->getRepository('SupportTicketBundle:TicketStats')->findByTicketId($idTicket);

    if (!empty($TicketStat[0])) {
     $TicketStat[0]->setDernierMsg(new \DateTime());

    $em->persist($TicketStat[0]);
    $em->flush();
    }

    $TicketInstance = $em->getRepository('SupportTicketBundle:Ticket')->find($idTicket);

      $Ticket_message->setTicketId($TicketInstance);

      $UserResponse = $em->getRepository('UtilisateursUserBundle:Users')->find($idUserConnect);
      
      $Ticket_message->setUserResponse($UserResponse);

      $emptyMessage=$Ticket_message->getMessage();
      if (empty($emptyMessage) OR $emptyMessage=="<br>") {
        $this->get('session')->set('flashMessageUserError', 'Votre message ne peut pas être vide');
        return $this->redirect($this->generateUrl('support_ticket_arguments',array('id'=>$id,'idTicket'=>$idTicket)));
      }
          // if it's an Admin (==1)
     if ($session->get('user_role')==1) {
      $Ticket_message->setAdmin(1);
      }
      else
      {
        $Ticket_message->setAdmin(3);
      }
         $Ticket_message->setDate(new \DateTime());


      $dir = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/tickets/';

      $produitId=$idTicket;
       // $image = $formMessage['fichier']->getData();
       $filename="";
       $fileIds=$request->request->get('token_envoi_ticket');

       if( $fileIds !== null  ) {
       $fileIds=implode(",", $fileIds);
// dump($IdPhoto);die();
              $Ticket_message->setFichier($fileIds);
          // $extensions = array('.jpg','.jpeg','.png','.zip','.rar','.pdf','.doc','.csv','.xls','.xlsx');
          // $return_logo = $this->container->get('service.entities')->addFile($dir,$image,$extensions,$produitId,'Ticket_message');
          // if($return_logo['erreur'] == '0'){
          //     $IdPhoto = $return_logo['idFile'];
          // } else {
          //    $this->get('session')->set('flashMessageUserError', $return_logo['msgErreur']);
          //    return $this->redirect($this->generateUrl('support_ticket_arguments',array('id'=>$id,'idTicket'=>$idTicket)));

          // }
        }


         $em->persist($Ticket_message);
         $em->flush();
      // if ($session->get('user_role')!==1) {
      //  $emailClient=$session->get('userConnect')['email'];
      //  // dump($emailClient);
   //          $message = \Swift_Message::newInstance()
   //              ->setContentType('text/html')
   //              ->setCharset('utf-8')
   //              ->setReplyTo('support@facnote')
   //              ->setSubject('FacNote : Réponse au Ticket ')
   //              ->setFrom(array('support@facnote' => 'FacNote'))
   //              ->setTo($emailClient)
   //              ->setBody(
   //                  $this->renderView(
   //                      '@SupportTicket/Ticket/email_notification.html.twig', array(
   //                          //'username' => $_username,
   //                          'responsePath' => $request->getScheme() . '://' . $request->getHttpHost(). $this->generateUrl('support_ticket_arguments', array('id' => $id,'idTicket' => $idTicket)),
   //                      )
   //                  )
   //              )
    //           ;
    //           $this->get('mailer')->send($message);

      //    }

        
         // to change status
      $listMessages = $em->getRepository('SupportTicketBundle:Ticket_message')->getListMessages($idTicket);
      
      $TicketInstance = $em->getRepository('SupportTicketBundle:Ticket')->find($idTicket);
      
      $prenom=$TicketInstance->getUserId()->getidPersone()->getPrenom();
      if (($session->get('user_role')==1 AND $session->get('userConnect')['prenom']==$prenom) AND isset($listMessages)) {
          
              $TicketInstance->setStatus('attenteAdmin');
              $TicketInstance->setTicketLu(false);
               $em->persist($TicketInstance);
               $em->flush();

        }
      else if (($session->get('user_role')==1 AND $session->get('userConnect')['prenom']!==$prenom) AND isset($listMessages)) {
          
              $TicketInstance->setStatus('attenteClient');
               $em->persist($TicketInstance);
               $em->flush();

        }
        else if ($session->get('user_role')!==1 AND isset($listMessages)) {
          
              $TicketInstance->setStatus('attenteAdmin');
              $TicketInstance->setTicketLu(false);
               $em->persist($TicketInstance);
               $em->flush();
        } // End to change status
        
    }
  }


    $TicketInstance = $em->getRepository('SupportTicketBundle:Ticket')->find($idTicket);
    $signal_new_ticket=0; //variable utilisé pour savoir s'il y a un ticket non lu ou pas / = 1 s'il existe
    foreach ($listTickets as $key => $value) {
          $ticketId=$value->getId();
          if($signal_new_ticket==0 && $value->getTicketLu()===false && $app_base=="FA0001")
            $signal_new_ticket=1;
          $lastMessage = $em->getRepository('SupportTicketBundle:Ticket')->getLastMessage($ticketId);
          $listTickets[$key]->LastMessage=$lastMessage;
          
        }
    if($signal_new_ticket==1)
    {
      $this->get('session')->set('flashMessageUserNew', 'Nouveaux Tickets Créé');
    }
  $listMessages = $em->getRepository('SupportTicketBundle:Ticket_message')->getListMessages($idTicket);

  $responsables=array();
  if($app_base=="FA0001")
  {
    $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, 0, $session->get('user_role'), 0, 1);
    // foreach ($societes as $s) {
    //   $responsables[$s['numeroDossier']]=$s['nomResponsable'];
    // }
    foreach ($societes as $s) {
      $b=$s["comptable"];
      foreach ($societes as $key2 => $value2) {
        if($value2["numeroDossier"]==$b)
        {
          $responsables[$s["numeroDossier"]]=$value2['raisonSociale'];
          break;
        }
      }
    }
  }
  
     return $this->render('@SupportTicket/Ticket/index.html.twig', array(
      'listTickets' => $listTickets,
      'classActive' => 'Tickets',
      'formMessage' => $formMessage->createView(),
      'listMessages' => $listMessages,
      'User'=> $User,
      'dateLivraison'=> $dateLivraison,
      'TicketInstance'=> $TicketInstance,
        'UserByRole'=> $UserByRole,
      'savedlengthPages'=> $savedlengthPages,
        'savedcurrentPage'=> $savedcurrentPage,
        'leTrie'=> $leTrie,
        'laColumn'=> $laColumn,
        'hiddenColumns'=> $hiddenColumns,
        'searchTable'=> $searchTable,
        'app_base'=> $app_base,
        'comptables'=>$responsables
    ));
  }
  else
  {
    $Ticket = new Ticket();

    $formTicket  = $this->get('form.factory')->create(TicketType::
    class, $Ticket);

     if($request->isMethod('POST')) {
         $formTicket->handleRequest($request);
         if($formTicket->isSubmitted() && $formTicket->isValid()) {


    $dir = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/tickets/';

      $produitId=$idTicket;
       // $image = $formMessage['fichier']->getData();
       $filename="";
       $fileIdsNew=$request->request->get('token_envoi_nouveau_ticket');
       if( $fileIdsNew !== null  ) {
       $fileIdsNew=implode(",", $fileIdsNew);
// dump($IdPhoto);die();
              $Ticket->getMessage()->setFichier($fileIdsNew);
          // $extensions = array('.jpg','.jpeg','.png','.zip','.rar','.pdf','.doc','.csv','.xls','.xlsx');
          // $return_logo = $this->container->get('service.entities')->addFile($dir,$image,$extensions,$produitId,'Ticket_message');
          // if($return_logo['erreur'] == '0'){
          //     $IdPhoto = $return_logo['idFile'];
          // } else {
          //    $this->get('session')->set('flashMessageUserError', $return_logo['msgErreur']);
          //    return $this->redirect($this->generateUrl('support_ticket_arguments',array('id'=>$id,'idTicket'=>$idTicket)));

          // }
        }

      $emptyTicketMsg=$Ticket->getMessage()->getMessage();
      $emptyTicketTitre=$Ticket->getTitre();
      if (empty($emptyTicketTitre) OR empty($emptyTicketMsg) OR $emptyTicketMsg=="<br>") {
        $this->get('session')->set('flashMessageUserError', 'Votre message et/ou Titre ne peut pas être vide');
        return $this->redirect($this->generateUrl('support_ticket_homepage'));
      }
        $Ticket->getMessage()->setTicketId($Ticket);
        $Ticket->getMessage()->setUserResponse($User[0]);
         $Ticket->getMessage()->setAdmin($session->get('user_role'));
         $Ticket->getMessage()->setDate(new \DateTime());
         $Ticket->setUserId($User[0]);
         // $Ticket->setAdmincharged($UserByRole[$random]);
         $Ticket->setStatus('attenteAdmin');
         $Ticket->setDate(new \DateTime());
          $Ticket->setBase($app_base);
          $Ticket->setTicketLu(false);
         $em->persist($Ticket);
         $em->flush();
        
        $ticketStats = new TicketStats();
    

        $ticketStats->setTicketId($Ticket);
        $ticketStats->setDateCreation(new \DateTime());
        // dump($ticketStats);die();
         $em->persist($ticketStats);
         $em->flush();

      }
    }
    

    $listTickets = $em->getRepository('SupportTicketBundle:Ticket')->getListTickets($status,$filterByUser);
    $signal_new_ticket=0; //variable utilisé pour savoir s'il y a un ticket non lu ou pas / = 1 s'il existe
    $responsables=array();
    if($app_base=="FA0001")
    {
      $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, 0, $session->get('user_role'), 0, 1);
      foreach ($societes as $s) {
        $b=$s["comptable"];
        foreach ($societes as $key2 => $value2) {
          if($value2["numeroDossier"]==$b)
          {
            $responsables[$s["numeroDossier"]]=$value2['raisonSociale'];
            break;
          }
        }
      }
    }
foreach ($listTickets as $key => $value) {
      $ticketId=$value->getId();
      if($signal_new_ticket==0 && $value->getTicketLu()===false && $app_base=="FA0001")
        $signal_new_ticket=1;
      $lastMessage = $em->getRepository('SupportTicketBundle:Ticket')->getLastMessage($ticketId);
      $listTickets[$key]->LastMessage=$lastMessage;
      
    }
    if($signal_new_ticket==1)
    {
      $this->get('session')->set('flashMessageUserNew', 'Nouveaux Tickets Créé');
    }
        return $this->render('@SupportTicket/Ticket/index.html.twig', array(
         'listTickets' => $listTickets,
         'classActive' => 'Tickets',
         'formTicket' => $formTicket->createView(),
        'User'=> $User,
      'savedlengthPages'=> $savedlengthPages,
        'savedcurrentPage'=> $savedcurrentPage,
        'searchTable'=> $searchTable,
        'leTrie'=> $leTrie,
        'laColumn'=> $laColumn,
        'hiddenColumns'=> $hiddenColumns,
        'app_base'=> $app_base,
          'comptables'=>$responsables,
     ));
    }

        $responsables=array();
        if($app_base=="FA0001")
        {
          $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, 0, $session->get('user_role'), 0, 1);
          // foreach ($societes as $s) {
          //   $responsables[$s['numeroDossier']]=$s['nomResponsable'];
          // }
          foreach ($societes as $s) {
            $b=$s["comptable"];
            foreach ($societes as $key2 => $value2) {
              if($value2["numeroDossier"]==$b)
              {
                $responsables[$s["numeroDossier"]]=$value2['raisonSociale'];
                break;
              }
            }
          }
        }

        return $this->render('@SupportTicket/Ticket/index.html.twig', array(
         'listTickets' => $listTickets,
         'classActive' => 'Tickets',
        'User'=> $User,
        'UserByRole'=> $UserByRole,
      'savedlengthPages'=> $savedlengthPages,
        'savedcurrentPage'=> $savedcurrentPage,
        'searchTable'=> $searchTable,
        'leTrie'=> $leTrie,
        'laColumn'=> $laColumn,
        'hiddenColumns'=> $hiddenColumns,
        'app_base'=> $app_base,
         'comptables'=>$responsables,
     ));
    }


    public function getFichierAction($idFichier=null)
    {
      if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));
        
        $session = new Session();        
        $app_base = $session->get('app_base');

        $idUserConnect = $session->get('user_id');
        $filterByUser=null;
        $em = $this->getDoctrine()->getManager();
        if ($idFichier!=null) {
          
      $fichier = $em->getRepository('EntitiesEntityBundle:EntFiles')->find($idFichier);
        }
        if($fichier!== null ) $photoPath = basename($fichier->getUrl());

      $extension=preg_replace('#^.*?.([a-zA-z]+)$#', '$1', $fichier->getUrl());

      $theBase=$fichier->getBase();
      
      return $this->render('@SupportTicket/Ticket/fichier.html.twig', array(
         'fichier' => $fichier,
         'classActive' => 'Tickets',
         'photoPath' => $photoPath,
         'theBase' => $theBase,
         'extension'=> $extension
    ));
    }


    public function addFichierAction(Request $request){

      if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));
        
        $session = new Session();        
        $app_base = $session->get('app_base');
        $em = $this->getDoctrine()->getManager();

        // $Ticket_message = new Ticket_message();
    // $formMessage  = $this->get('form.factory')->create(Ticket_messageType::
    // class, $Ticket_message);
    
    $dir = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/tickets/';

      // $produitId=$idTicket;
      $produitId=111;
       // $image = $formMessage['fichier']->getData();
      $image = $request->files->get('file');
       $filename="";

       if( $image !== null  ) {
          $extensions = array('.jpg','.jpeg','.png','.zip','.rar','.pdf','.doc','.csv','.xls','.xlsx');
          $return_logo = $this->container->get('service.entities')->addFile($dir,$image,$extensions,$produitId,'Ticket_message');
          if($return_logo['erreur'] == '0'){
              $IdPhoto = $return_logo['idFile'];
              // $Ticket_message->setFichier($IdPhoto);
          } else {
             $this->get('session')->set('flashMessageUserError', $return_logo['msgErreur']);
             return $this->redirect($this->generateUrl('support_ticket_arguments',array('id'=>$id,'idTicket'=>$idTicket)));

          }
        }

      return new Response($IdPhoto->getId());
    }
    /*
    public function saveCollonesAction(Request $req){

        $session = new Session();
        $app_base = $session->get('app_base');

    $pageLength = $req->request->get('pageLength');
    $currentPage = $req->request->get('currentPage');
    $searchTable = $req->request->get('searchTable');
    $searchTable = $req->request->get('searchTable');
    $columnIndex = $req->request->get('columnIndex');
    $order = $req->request->get('order');
    $column = $req->request->get('column');
    $state = $req->request->get('state');
    
    $currentPage=intval($currentPage)+1;
    $em = $this->getDoctrine()->getManager();

    if (!empty($currentPage)) {
    $pagesTickets = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/pages'));
    if (empty($pagesTickets)) {
        $pagesTickets = new SaveCollones;
    $pagesTickets->setModule('tickets/pages');
    $pagesTickets->setUtilisateur($session->get('user_id'));
    $pagesTickets->setCollones($currentPage);
    $em->persist($pagesTickets);
    }
    else
    {
    $pagesTickets[0]->setModule('tickets/pages');
    $pagesTickets[0]->setUtilisateur($session->get('user_id'));
    $pagesTickets[0]->setCollones($currentPage);
    $em->persist($pagesTickets[0]);
    }

    }

    if (!empty($pageLength)) {
    $lengthTickets = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/lengthPages'));
    if (empty($lengthTickets)) {
        $lengthTickets = new SaveCollones;
    $lengthTickets->setModule('tickets/lengthPages');
    $lengthTickets->setUtilisateur($session->get('user_id'));
    $lengthTickets->setCollones($pageLength);
    $em->persist($lengthTickets);
    }
    else
    {
    $lengthTickets[0]->setModule('tickets/lengthPages');
    $lengthTickets[0]->setUtilisateur($session->get('user_id'));
    $lengthTickets[0]->setCollones($pageLength);
    $em->persist($lengthTickets[0]);
    }
    }

    if ($searchTable!==null) {
    $searchTickets = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/search'));
    if (empty($searchTickets)) {
        $searchTickets = new SaveCollones;
    $searchTickets->setModule('tickets/search');
    $searchTickets->setUtilisateur($session->get('user_id'));
    $searchTickets->setCollones($searchTable);
    $em->persist($searchTickets);
    }
    else
    {
    $searchTickets[0]->setModule('tickets/search');
    $searchTickets[0]->setUtilisateur($session->get('user_id'));
    $searchTickets[0]->setCollones($searchTable);
    $em->persist($searchTickets[0]);
    }

    }

    if ((!empty($columnIndex) OR $columnIndex==0) AND !empty($order)) {
      $pagesTickets = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/tri'));
      if (empty($pagesTickets)) {
          $pagesTickets = new SaveCollones;
      $pagesTickets->setModule('tickets/tri');
      $pagesTickets->setUtilisateur($session->get('user_id'));
      $pagesTickets->setCollones($columnIndex.','.$order);
      $em->persist($pagesTickets);
      }
      else
      {
      $pagesTickets[0]->setModule('tickets/tri');
      $pagesTickets[0]->setUtilisateur($session->get('user_id'));
      $pagesTickets[0]->setCollones($columnIndex.','.$order);
      $em->persist($pagesTickets[0]);
      }
    }

    
    if (!empty($column) OR $column==0) {
    $lengthTickets = $em->getRepository('EcrituresEcrituresBundle:SaveCollones')->findBy(array('utilisateur' => $session->get('user_id'),'module' => 'tickets/hiddenColumn'));
    if (empty($lengthTickets)) {
        $lengthTickets = new SaveCollones;
    $lengthTickets->setModule('tickets/hiddenColumn');
    $lengthTickets->setUtilisateur($session->get('user_id'));
    $lengthTickets->setCollones($column);
    $em->persist($lengthTickets);
    }
    else
    {
      if (empty($lengthTickets[0]->getCollones()) AND $lengthTickets[0]->getCollones() !==0) {
      $allHiddenColumn=$column;
      }
      else
      {

      $allColumns=$lengthTickets[0]->getCollones();
      
      $pieces = explode(",", $allColumns);
      $key = array_search($column, $pieces);
      
      if ($state=='false' AND $key === false) {
        array_push($pieces, $column);
      
      }
      elseif ($state=='true' AND $key !== false) {
          unset($pieces[$key]);
      
      }
      $allHiddenColumn=implode(",", $pieces);
      }
      
    $lengthTickets[0]->setModule('tickets/hiddenColumn');
    $lengthTickets[0]->setUtilisateur($session->get('user_id'));
    $lengthTickets[0]->setCollones($allHiddenColumn);
    $em->persist($lengthTickets[0]);
    }
    }
    $em->flush();
    return new JsonResponse(array("result" => "ok","colll" => '$colll'));
    }  */      
}
