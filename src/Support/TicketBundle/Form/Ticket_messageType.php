<?php

namespace Support\TicketBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
class Ticket_messageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->add('message')->add('ticketId');
        $builder->add('message', TextareaType::class, array(
                                            'required' => false,
                                            'label' => 'Message ',
                                            'attr' => array('class' => 'form-control')
                                        )
        )

        ->add('ticketId', IntegerType::class, array(
                                            'required' => false,
                                            'label' => 'ticketId ',
                                            'attr' => array('class' => 'form-control')
                                        )
                )
        ->add('fichier', FileType::class, array(
                                            'required' => false,
                                            'label' => 'Fichier ',
                                            'data_class' => null,
                                            'mapped' => false,
                                            'attr' => array('class' => '')
                                        )
                )
      ->add('save', SubmitType::class);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Support\TicketBundle\Entity\Ticket_message'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'support_ticketbundle_ticket_message';
    }


}
