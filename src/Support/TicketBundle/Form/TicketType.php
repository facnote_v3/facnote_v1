<?php

namespace Support\TicketBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Support\TicketBundle\Form\Ticket_messageType;
class TicketType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $builder->add('titre')->add('date')->add('description')->add('status')->add('userId')->add('fichier');

        $builder->add('titre', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Sujet ',
                                            'attr' => array('class' => 'form-control',
                                                'maxlength' => 25)
                                        )
        )

        ->add('date', DateType::class, array(
                                            'required' => true,
                                            'label' => 'Date de Facture'
                                        )
        )


        ->add('status', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Status ',
                                            'attr' => array('class' => 'form-control')
                                        )
        )

        ->add('message', Ticket_messageType::class, array(
                                            'required' => true,
                                            'label' => 'Message',
                                            'attr' => array('class' => 'form-control')
                                        )
        )


        // ->add('userId', EntityType::class, array(
        //                                     "class" => 'Utilisateurs\UserBundle\Entity\Users',
        //                                     "choice_label"=>"userId",
        //                                     "mapped" => "false",
        //                                     'label' => "Utilisateur",
        //                                     'attr' => array('class' => 'form-control')
        //                                     )
        //         )
         ->add('fichier', FileType::class, array(
                                            'required' => false,
                                            'label' => 'Fichier ',
                                            'data_class' => null,
                                            'mapped' => false,
                                            'attr' => array('class' => '')
                                        )
                )

         ->add('save', SubmitType::class);


    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Support\TicketBundle\Entity\Ticket'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'support_ticketbundle_ticket';
    }


}
