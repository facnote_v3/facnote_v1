<?php

namespace Support\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket_message
 *
 * @ORM\Table(name="ticket_message")
 * @ORM\Entity(repositoryClass="Support\TicketBundle\Repository\Ticket_messageRepository")
 */
class Ticket_message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=2500)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="admin", type="integer", length=11)
     */
    private $admin;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     * @var \Support\TicketBundle\Entity\Ticket
     * @ORM\ManyToOne(targetEntity="Support\TicketBundle\Entity\Ticket")
     * @ORM\JoinColumn(name="ticketID", referencedColumnName="id")
     *
     */
    private $ticketId;

    /**
     * @var string
     *
     * @ORM\Column(name="fichier", type="string", length=200)
     */
    private $fichier;
    
    /**
     * @var \Utilisateurs\UserBundle\Entity\Users
     * @ORM\ManyToOne(targetEntity="\Utilisateurs\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="UserResponse", referencedColumnName="id")
     *
     */
    private $UserResponse;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Ticket_message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set ticketId
     *
     * @param \Support\TicketBundle\Entity\Ticket $ticketId
     *
     * @return Ticket_message
     */
    public function setTicketId(\Support\TicketBundle\Entity\Ticket $ticketId = null)
    {
        $this->ticketId = $ticketId;

        return $this;
    }

    /**
     * Get ticketId
     *
     * @return \Support\TicketBundle\Entity\Ticket
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * Set admin
     *
     * @param integer $admin
     *
     * @return Ticket_message
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return integer
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Ticket_message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fichier
     *
     * @param string $fichier
     *
     * @return Ticket_message
     */
    public function setFichier($fichier = null)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier
     *
     * @return string
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Set userResponse.
     *
     * @param \Utilisateurs\UserBundle\Entity\Users|null $userResponse
     *
     * @return Ticket_message
     */
    public function setUserResponse(\Utilisateurs\UserBundle\Entity\Users $userResponse = null)
    {
        $this->UserResponse = $userResponse;

        return $this;
    }

    /**
     * Get userResponse.
     *
     * @return \Utilisateurs\UserBundle\Entity\Users|null
     */
    public function getUserResponse()
    {
        return $this->UserResponse;
    }
}
