<?php

namespace Support\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket")
 * @ORM\Entity(repositoryClass="Support\TicketBundle\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=25)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;
    
    /**
     * @var \Utilisateurs\UserBundle\Entity\Users
     * @ORM\ManyToOne(targetEntity="\Utilisateurs\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="userID", referencedColumnName="id")
     *
     */
    private $userId;


    /**
     * @var \Entities\EntityBundle\Entity\EntFiles
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntFiles")
     * @ORM\JoinColumn(name="fichier", referencedColumnName="id")
     * 
     */
    private $fichier;

    /**
     * @var \Support\TicketBundle\Entity\Ticket_message
     * @ORM\ManyToOne(targetEntity="\Support\TicketBundle\Entity\Ticket_message",cascade={"persist"})
     * @ORM\JoinColumn(name="message", referencedColumnName="id")
     *
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateresolu", type="datetime", nullable=true)
     */
    private $dateresolu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datelivraison", type="datetime", nullable=true)
     */
    private $datelivraison;

    /**
     * @var \Utilisateurs\UserBundle\Entity\Users
     * @ORM\ManyToOne(targetEntity="\Utilisateurs\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="admincharged", referencedColumnName="id")
     *
     */
    private $admincharged;

    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=20)
     */
    private $base;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ticket_lu", type="boolean", nullable=true)
     */
    private $ticket_lu;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Ticket
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Ticket
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set userId
     *
     * @param \Utilisateurs\UserBundle\Entity\Users $userId
     *
     * @return Ticket
     */
    public function setUserId(\Utilisateurs\UserBundle\Entity\Users $userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \Utilisateurs\UserBundle\Entity\Users
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set fichier
     *
     * @param \Entities\EntityBundle\Entity\EntFiles $fichier
     *
     * @return \Entities\EntityBundle\Entity\EntFiles
     */
    public function setFichier(\Entities\EntityBundle\Entity\EntFiles $fichier = null)
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * Get fichier
     *
     * @return \Entities\EntityBundle\Entity\EntFiles
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Set message
     *
     * @param \Support\TicketBundle\Entity\Ticket_message $message
     *
     * @return Ticket
     */
    public function setMessage(\Support\TicketBundle\Entity\Ticket_message $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \Support\TicketBundle\Entity\Ticket_message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set dateresolu
     *
     * @param \DateTime $dateresolu
     *
     * @return Ticket
     */
    public function setDateresolu($dateresolu)
    {
        $this->dateresolu = $dateresolu;

        return $this;
    }

    /**
     * Get dateresolu
     *
     * @return \DateTime
     */
    public function getDateresolu()
    {
        return $this->dateresolu;
    }

    /**
     * Set datelivraison
     *
     * @param \DateTime $datelivraison
     *
     * @return Ticket
     */
    public function setDatelivraison($datelivraison)
    {
        $this->datelivraison = $datelivraison;

        return $this;
    }

    /**
     * Get datelivraison
     *
     * @return \DateTime
     */
    public function getDatelivraison()
    {
        return $this->datelivraison;
    }

    /**
     * Set admincharged.
     *
     * @param string $admincharged
     *
     * @return Ticket
     */
    public function setAdmincharged($admincharged)
    {
        $this->admincharged = $admincharged;

        return $this;
    }

    /**
     * Get admincharged.
     *
     * @return string
     */
    public function getAdmincharged()
    {
        return $this->admincharged;
    }

    /**
     * Set base.
     *
     * @param string $base
     *
     * @return Ticket
     */
    public function setBase($base)
    {
        $this->base = $base;
    
        return $this;
    }

    /**
     * Get base.
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set ticketLu.
     *
     * @param bool|null $ticketLu
     *
     * @return Ticket
     */
    public function setTicketLu($ticketLu = null)
    {
        $this->ticket_lu = $ticketLu;
    
        return $this;
    }

    /**
     * Get ticketLu.
     *
     * @return bool|null
     */
    public function getTicketLu()
    {
        return $this->ticket_lu;
    }
}
