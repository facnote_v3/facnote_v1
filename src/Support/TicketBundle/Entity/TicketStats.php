<?php

namespace Support\TicketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketStats
 *
 * @ORM\Table(name="TicketStats")
 * @ORM\Entity(repositoryClass="Support\TicketBundle\Repository\TicketStatsRepository")
 */
class TicketStats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateCreation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dernierMsg", type="datetime", nullable=true)
     */
    private $dernierMsg;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="miseEnDev", type="datetime", nullable=true)
     */
    private $miseEnDev;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="miseEnProd", type="datetime", nullable=true)
     */
    private $miseEnProd;

    /**
     * @var \Support\TicketBundle\Entity\Ticket
     * @ORM\ManyToOne(targetEntity="Support\TicketBundle\Entity\Ticket")
     * @ORM\JoinColumn(name="ticketID", referencedColumnName="id")
     *
     */
    private $ticketId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation.
     *
     * @param \DateTime|null $dateCreation
     *
     * @return TicketStats
     */
    public function setDateCreation($dateCreation = null)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return \DateTime|null
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dernierMsg.
     *
     * @param \DateTime|null $dernierMsg
     *
     * @return TicketStats
     */
    public function setDernierMsg($dernierMsg = null)
    {
        $this->dernierMsg = $dernierMsg;

        return $this;
    }

    /**
     * Get dernierMsg.
     *
     * @return \DateTime|null
     */
    public function getDernierMsg()
    {
        return $this->dernierMsg;
    }

    /**
     * Set miseEnDev.
     *
     * @param \DateTime|null $miseEnDev
     *
     * @return TicketStats
     */
    public function setMiseEnDev($miseEnDev = null)
    {
        $this->miseEnDev = $miseEnDev;

        return $this;
    }

    /**
     * Get miseEnDev.
     *
     * @return \DateTime|null
     */
    public function getMiseEnDev()
    {
        return $this->miseEnDev;
    }

    /**
     * Set miseEnProd.
     *
     * @param \DateTime|null $miseEnProd
     *
     * @return TicketStats
     */
    public function setMiseEnProd($miseEnProd = null)
    {
        $this->miseEnProd = $miseEnProd;

        return $this;
    }

    /**
     * Get miseEnProd.
     *
     * @return \DateTime|null
     */
    public function getMiseEnProd()
    {
        return $this->miseEnProd;
    }


    /**
     * Set ticketId
     *
     * @param \Support\TicketBundle\Entity\Ticket $ticketId
     *
     * @return Ticket_message
     */
    public function setTicketId(\Support\TicketBundle\Entity\Ticket $ticketId = null)
    {
        $this->ticketId = $ticketId;

        return $this;
    }

    /**
     * Get ticketId
     *
     * @return \Support\TicketBundle\Entity\Ticket
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }
}
