<?php

namespace Crm\SocieteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Crm\SocieteBundle\Entity\infosSociete;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SocieteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $infosSociete = $options['infos_societe'][0];

        $builder->add('raisonSocial', TextType::class, array(
                                            'required' => true,
                                            'label' => 'Raison sociale *',
                                            'mapped' => false,
                                            'data' => $infosSociete['raisonSociale'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('emailContact', EmailType::class, array(
                                            'required' => false,
                                            'label' => 'Email société',
                                            'mapped' => false,
                                            'data' => $infosSociete['emailSociete'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('responsable', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Nom de gérant',
                                            'mapped' => false,
                                            'data' => $infosSociete['nomResponsable'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('fixe', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Téléphone',
                                            'mapped' => false,
                                            'data' => $infosSociete['phoneSociete'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('siret', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Siret',
                                            'mapped' => false,
                                            'data' => $infosSociete['siret'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('fax', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Fax',
                                            'mapped' => false,
                                            'data' => $infosSociete['faxSociete'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('siren', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Siren',
                                            'mapped' => false,
                                            'data' => $infosSociete['siren'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('numeroTva', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Numéro TVA',
                                            'mapped' => false,
                                            'data' => $infosSociete['numeroTva'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('codeApe', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Code APE',
                                            'mapped' => false,
                                            'data' => $infosSociete['codeApe'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('type', EntityType::class, array(
                                            "class" => 'Crm\SocieteBundle\Entity\TypeSociete',
                                            'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('t')
                                                        ->where('t.active = 1 ')
                                                        ->orderBy('t.id', 'ASC');
                                                },
                                            "choice_label"=>"type",
                                            "mapped" => "false",
                                            'label' => "Type *",
                                            'mapped' => false,
                                            'attr' => array('class' => 'form-control')
                                             )
                )
               ->add('idStatut', EntityType::class, array(
                                            "class" => 'Crm\SocieteBundle\Entity\Statut',
                                            'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('st')
                                                        ->where('st.active = 1 ')
                                                        ->orderBy('st.id', 'ASC');
                                                },
                                            "choice_label"=>"statut",
                                            "mapped" => "false",
                                            'label' => "Statut *",
                                            'mapped' => false,
                                            'attr' => array('class' => 'form-control')
                                            )
                   )
               ->add('idSecteur', EntityType::class, array(
                                            "class" => 'Crm\SocieteBundle\Entity\SecteurActivite',
                                            'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('sect')
                                                        ->where('sect.active = 1 ')
                                                        ->orderBy('sect.secteur', 'ASC');
                                                },
                                            "choice_label"=>"secteur",
                                            "mapped" => "false",
                                            'label' => "Secteur *",
                                            'mapped' => false,
                                            'attr' => array('class' => 'form-control')
                                            )
                   )
               ->add('capital', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Capital ',
                                            'mapped' => false,
                                            'data' => $infosSociete['capital'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('rcs', TextType::class, array(
                                            'required' => false,
                                            'label' => 'RCS ',
                                            'mapped' => false,
                                            'data' => $infosSociete['rcs'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('siteWeb', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Site Web ',
                                            'mapped' => false,
                                            'data' => $infosSociete['siteWeb'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('numeroAgrementFormateur', TextType::class, array(
                                            'required' => false,
                                            'label' => 'N° d\'agrement formateur ',
                                            'mapped' => false,
                                            'data' => $infosSociete['numeroAgrementFormateur'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('logo1', FileType::class, array(
                                            'required' => false,
                                            'label' => 'Logo 1 ',
                                            'data_class' => null,
                                            'mapped' => false,
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('logo2', FileType::class, array(
                                            'required' => false,
                                            'label' => 'Logo 2 ',
                                            'data_class' => null,
                                            'mapped' => false,
                                            'attr' => array('class' => 'form-control')
                                        )
                );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crm\SocieteBundle\Entity\Societe',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'intention' => 'societe_item',
            'app_base' => null,
            'infos_societe' => array(),
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'crm_societebundle_societe';
    }


}
