<?php

namespace Crm\SocieteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Crm\SocieteBundle\Entity\Societe;
use Crm\SocieteBundle\Entity\infos_societe;
use Crm\SocieteBundle\Form\SocieteType;
use Utilisateurs\UserBundle\Services\AuthUser;
use Tracabilite\TraceBundle\Services\Trace;
use Entities\EntityBundle\Services\ServiceEntities;


class SocieteController extends Controller
{
    /**
     * Affiche la page principale du module de gestion des Societes.
     *
     */
    public function indexAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');


        $em = $this->getDoctrine()->getManager();
        $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, 0, $session->get('user_role'));
        //dump($societes);exit;

        $response = $this->render('@CrmSociete/Societe/index.html.twig', array(
            'societes' => $societes,
            'classActive' => 'societe'
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }

    /**
     * Gère l'ajout et l'édition des societe.
     * 
     * <p>
     * <b>addAction()</b> peut être appelé en GET pour afficher le formulaire (création ou modification) ou en POST pour insérer un nouveau user<br />
     * 
     * </p>
     * 
     */

    public function addAction(Request $request, $id = null) {
        
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');
        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');

        $idUserConnect = $session->get('user_id');
        $infosSocieteArr = array();
        $contactsSociete = array();
        $banquesSociete = array();
        $logo1_url = '';
        $logo2_url = '';

        $_editMod = 0;
        $societeId = '';
        $em = $this->getDoctrine()->getManager();
        if(isset($id)) { // Edition
            $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($id);
            //dump($societe);exit;
            if(!$societe) {
                $this->get('session')->set('flashMessageUser', "Societe non trouvé !");
            }
            $_editMod = 1;
            $societeId = $id;
            $infosSocieteArr = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, $id, $session->get('user_role'));

            $contactsSociete = $em->getRepository('CrmContactBundle:Contact')->findAllContactBySociete($id);
            $banquesSociete = $em->getRepository('CrmSocieteBundle:BanqueInfos')->findInfosBanqueSociete($id);
            //dump($banquesSociete);exit;

            if($societe->getIdLogo1() !== null ) $logo1_url = basename($societe->getIdLogo1()->getUrl());
            if($societe->getIdLogo2() !== null ) $logo2_url = basename($societe->getIdLogo2()->getUrl());

        } else {
            $societe = new Societe();
            $infosSocieteArr[0] = array('raisonSociale'=>'','emailSociete'=>'','nomResponsable'=>'','phoneSociete'=>'','faxSociete' => '','siren' => '','siret'=>'', 'numeroTva' => '', 'codeApe' => '', 'capital'=>'','rcs'=>'','siteWeb'=>'','numeroAgrementFormateur'=>'','adresseSiege'=>'','villeSiege'=>'','codePostaleSiege'=>'', 'paysSiege'=>'','adresseFact'=>'','villeFact'=>'','codePostaleFact'=>'', 'paysFact'=>'','idSocieteEnt'=>'','idPersone'=>'','idEmail'=>'','idPhone'=>'','idFax'=>'','idAdresseSiege'=>'','idAdresseFacturation'=>'', 'idStatut'=>'','idSecteur'=>''  );
        }

        $form = $this->createForm(SocieteType::class, $societe, array(
            'app_base' => $app_base,
            'infos_societe' => $infosSocieteArr
        ));

    
        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {

                $contacts = $request->get('contacts');


                $data = array(
                    'idSociete' => $societeId,
                    'idSocieteEnt' => $request->get("idSocieteEnt"),
                    'idPersone' => $request->get("idPersone"),
                    'idEmail' => $request->get("idEmail"),
                    'idPhone' => $request->get("idPhone"),
                    'idFax' => $request->get("idFax"),
                    'idAdresseSiege' => $request->get("idAdresseSiege"),
                    'idAdresseFacturation' => $request->get("idAdresseFacturation"),
                    'idStatut' => $form->get("idStatut")->getData()->getId(),
                    'idSecteur' => $form->get("idSecteur")->getData()->getId(),

                    'raisonSociale' => $form->get("raisonSocial")->getData(),
                    'typeSociete' => $form->get("type")->getData()->getId(),
                    'emailSociete' => $form->get("emailContact")->getData(),
                    'nomResponsable' => $form->get("responsable")->getData(),
                    'phoneSociete' => $form->get("fixe")->getData(),
                    'faxSociete' => $form->get("fax")->getData(),
                    'siren' => $form->get("siren")->getData(),
                    'siret' => $form->get("siret")->getData(),
                    'numeroTva' => $form->get("numeroTva")->getData(),
                    'codeApe' => $form->get("codeApe")->getData(),
                    'capital' => $form->get("capital")->getData(),
                    'rcs' => $form->get("rcs")->getData(),
                    'siteWeb' => $form->get("siteWeb")->getData(),
                    'numeroAgrementFormateur' => $form->get("numeroAgrementFormateur")->getData(),
                    'adresseSiege' => $request->get("adresseSiege"),
                    'villeSiege' => $request->get("villeSiege"),
                    'codePostaleSiege' => $request->get("codePostaleSiege"),
                    'paysSiege' => $request->get("paysSiege"),
                    'adresseFact' => $request->get("adresseFact"),
                    'villeFact' => $request->get("villeFact"),
                    'codePostaleFact' => $request->get("codePostaleFact"),
                    'paysFact' => $request->get("paysFact"),
                    'contacts' => $request->get('contacts'),
                    'banques' => $request->get('banques')
                );

                //dump($data);exit;
                    
                // add societe ent
                $idSocieteEnt = $this->container->get('service.entities')->addSociete($data['idSocieteEnt'],$data['raisonSociale'],$data['numeroTva'],$data['siret'],$data['codeApe'],$data['capital'],$data['rcs'],$data['siteWeb'],$data['numeroAgrementFormateur'],$data['siren'],'');
                // add adresse siege
                $idAdresseSiege = $this->container->get('service.entities')->addAdresse($data['idAdresseSiege'],$data['adresseSiege'],$data['villeSiege'],$data['codePostaleSiege'],$data['paysSiege']);
                // add adresse facturation 
                 $idAdresseFacturation = $this->container->get('service.entities')->addAdresse($data['idAdresseFacturation'],$data['adresseFact'],$data['villeFact'],$data['codePostaleFact'],$data['paysFact']);
                // add persone 
                 $idPersone = $this->container->get('service.entities')->addPersone($data['idPersone'],$data['nomResponsable'],'','','');
                // add email
                 $idEmail = $this->container->get('service.entities')->addEmail($data['idEmail'],$data['emailSociete'],'societe');
                // add phone
                 $idPhone = $this->container->get('service.entities')->addPhone($data['idPhone'],$data['phoneSociete'],'fixe');
                // add fax
                 $idFax = $this->container->get('service.entities')->addPhone($data['idFax'],$data['faxSociete'],'fax');

                // add societe 
                 $societe->setBase($app_base);
                 $societe->setIdEntSociete($idSocieteEnt);
                 $societe->setIdGerant($idPersone);
                 $societe->setIdAdresseSiege($idAdresseSiege);
                 $societe->setIdAdresseFacturation($idAdresseFacturation);
                 $societe->setIdEmail($idEmail);
                 $societe->setIdTelSiege($idPhone);
                 $societe->setIdFaxSiege($idFax);

                 $secteur = $em->getRepository('CrmSocieteBundle:SecteurActivite')->find($data['idSecteur']);
                 $statut = $em->getRepository('CrmSocieteBundle:Statut')->find($data['idStatut']);
                 $typeSociete =  $em->getRepository('CrmSocieteBundle:TypeSociete')->find($data['typeSociete']);

                 $societe->setIdSecteur($secteur);
                 $societe->setIdStatut($statut);
                 $societe->setIdType($typeSociete);


                $em->persist($societe);
                $em->flush();
                $idSociete = $societe->getId();

                // add logo file 
                $dir = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/logos/';
                $image1 = $form['logo1']->getData();
                $extensions = array('.jpg','.jpeg','.png'); 
                if( $image1 !== null  ) {
                    $return_logo1 = $this->container->get('service.entities')->addFile($dir,$image1,$extensions,$idSociete,'Societe');
                    if($return_logo1['erreur'] == '0'){
                        $IdLogo1 = $return_logo1['idFile'];
                        $societe->setIdLogo1($IdLogo1);
                    } else {
                       $this->get('session')->set('flashMessageUserError', $return_logo1['msgErreur']);
                    }
                }

                $image2 = $form['logo2']->getData();
                //dump($image2);exit;
                if( $image2 !== null  ) {
                    $return_logo2 = $this->container->get('service.entities')->addFile($dir,$image2,$extensions,$idSociete,'Societe');
                    if($return_logo2['erreur'] == '0'){
                        $IdLogo2 = $return_logo2['idFile'];
                        $societe->setIdLogo2($IdLogo2);
                    } else {
                       $this->get('session')->set('flashMessageUserError', $return_logo2['msgErreur']);
                    }
                }

                // add contacts 
                if(isset($data['contacts'])) 
                    $this->container->get('service.entities')->addContacts($idSociete, $data['contacts']);

                // add banques
                if(isset($data['banques']))
                    $em->getRepository('CrmSocieteBundle:BanqueInfos')->addBanques($idSociete, $data['banques']);

                if( ( isset($return_logo1['erreur']) AND $return_logo1['erreur'] == 1 )  OR  ( isset($return_logo2['erreur']) AND $return_logo2['erreur'] == 1 ) ) {
                    return $this->redirect($this->generateUrl('crm_societe_edit',array('id' => $idSociete )));
                } else {
                    $em->persist($societe);
                    $em->flush();
                }

                $em->getRepository('CrmSocieteBundle:Societe')->addParameters($idSociete,array(),1);

                $flashMsg = "Societe ajouté avec succès "; // Ajout (par défaut)
                if(isset($id)) { // Edition
                    $flashMsg = "Societe modifié avec succès !";
                }
                $this->get('session')->set('flashMessageUser', $flashMsg);

                 // Enregistrer trace ajout/modification societe
                if(isset($id)) {
                    $this->container->get('tracabilite.trace')->setTrace('Societe','Modification societe '.$societe->getBase().' id = '.$societeId);
                } else {
                    $this->container->get('tracabilite.trace')->setTrace('Societe','Ajout nouveau societe '.$societe->getBase().' id = '.$societeId);
                }

                return $this->redirect($this->generateUrl('crm_societe_homepage'));
            }
        }

        $pays = $em->getRepository('CrmClientBundle:Pays')->findAll();
    
        return $this->render('@CrmSociete/Societe/add.html.twig', array(
                'editMod' => $_editMod,
                'societeId' => $societeId,
                'infosSociete' => $infosSocieteArr[0],
                'contactsSociete' => $contactsSociete,
                'banquesSociete' => $banquesSociete,
                'classActive' => 'societe',
                'form' => $form->createView(),
                'pays' => $pays,
                'logo1_url' => $logo1_url,
                'logo2_url' => $logo2_url
        ));
    }
    /**
     * Supprime Un societe/societe.
     *
     */
    public function deleteAction($id) {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));


        $em = $this->getDoctrine()->getManager();
        $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($id);
        if(!$societe) {
             $this->get('session')->set('flashMessageUser', "Societe  non trouvé !");
        }

        try {
           /*
            // Delete societe_ent
            $idSocieteEnt = $societe->getIdEntSociete()->getId();
            $societeEnt = $em->getRepository('EntitiesEntityBundle:EntSociete')->find($idSocieteEnt);
            
            // Delete persone_ent
            $idPersone = $societe->getIdGerant()->getId();
            $EntPersone = $em->getRepository('EntitiesEntityBundle:EntPersone')->find($idPersone);

            // Delete persone_ent
            $idEmail = $societe->getIdEmail()->getId();
            $EntEmail = $em->getRepository('EntitiesEntityBundle:EntEmail')->find($idEmail);

             // Delete adresse_ent siege
            $idAdrSiege = $societe->getIdAdresseSiege()->getId();
            $AdrSiege = $em->getRepository('EntitiesEntityBundle:EntAdresse')->find($idAdrSiege);

            // Delete adresse_ent fact
            $idAdrFact = $societe->getIdAdresseFacturation()->getId();
            $AdrFact = $em->getRepository('EntitiesEntityBundle:EntAdresse')->find($idAdrFact);
            
            // Delete contacts societe
            $em->getRepository('CrmContactBundle:Contact')->deleteContactsSociete($id);

            // Delete banques societe
            $em->getRepository('CrmSocieteBundle:BanqueInfos')->deleteBanquesSociete($id);


            $em->remove($societe);$em->flush();
            $em->remove($societeEnt);$em->flush();
            $em->remove($EntPersone);$em->flush();
            $em->remove($EntEmail);$em->flush();
            $em->remove($AdrSiege);$em->flush();
            $em->remove($AdrFact);$em->flush();
            //$em->flush();
            
            // Delete infos societe
           // $em->getRepository('CrmSocieteBundle:Societe')->deleteInfosSociete($id);


           $this->container->get('tracabilite.trace')->setTrace('Societe','Suppression de societe '.$societe->getBase().' id = '.$id);

           */
           $societe->setSupprimer(true);
           $em->flush();
            
           
        } catch (\Doctrine\DBAL\DBALException $e) {
             $this->get('session')->set('flashMessageUserError', "Un problème est rencontré avant la suppression de la société  ");
            return $this->redirect($this->generateUrl('crm_societe_homepage'));
        } 
        
        $flashMsg = "Societe supprimé avec succès "; 
        $this->get('session')->set('flashMessageUser', $flashMsg);

        return $this->redirect($this->generateUrl('crm_societe_homepage'));
    }

    /**
    * Consulter les infos d'un societe
    *
    */
    public function infosAction($id) {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));


        $em = $this->getDoctrine()->getManager();
        $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($id);
        $infosSocieteArr = $em->getRepository('CrmSocieteBundle:infos_societe')->findByidSociete($societe);

        return $this->render('@CrmSociete/Societe/infos.html.twig', array(
                'societe' => $societe,
                'infosSociete' => $infosSocieteArr[0],
                'classActive' => 'societe',
        ));
    }

    /**
    * Export CSV des Societes
    *
    */
    public function CSVExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');

        $em = $this->getDoctrine()->getManager();

        $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, 0, $session->get('user_role'));
        //dump($societes);exit;

        $output = "RAISON SOCIALE;RESPONSABLE;TYPE;EMAIL;TELEPHONE;COMPTABLE;SIREN;SIRET;TVA;STATUT;SECTEUR;ADRESSE";
        $output .="\r\n";

        $this->container->get('tracabilite.trace')->setTrace('Societe','Exporter liste des societes en csv ');


        foreach ($societes as $societe) {

            $output .= $societe['raisonSociale'].";".$societe['nomResponsable'].";".$societe['typeSociete'].";".$societe['emailSociete'].";".$societe['phoneSociete'].";".$societe['comptable'].";".$societe['siren'].";".$societe['siret'].";".$societe['numeroTva'].";".$societe['statutSociete'].";".$societe['secteur'].";".$societe['codePostaleSiege']." ".$societe['villeSiege']." ".$societe['adresseSiege'];
            $output .="\r\n";
        }


        $output = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $output);
        $path = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/csv/';
        $_docName = 'societe-export.csv';
        $_fileName = $path . $_docName;
        $f = fopen ($_fileName,'w');
        fwrite($f, $output);
        fclose($f);
        
        // Telechargement
        if(file_exists($_fileName)) {
            @ob_end_clean();
            @ini_set('zlib.output_compression', '0');
        
            // date courante
            $maintenant=gmdate('D, d M Y H:i:s').' GMT';
        
            // envoi des en-têtes nécessaires au navigateur
            header('Content-Type: application/csv'); // charset=utf-8
            header('Content-Disposition: attachment; filename="'.$_docName.'"');
        
            // Internet Explorer nécessite des en-têtes spécifiques
            if(preg_match('/msie|(microsoft internet explorer)/i', $_SERVER['HTTP_USER_AGENT']))
            {
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
            }
            else header('Pragma: no-cache');
            header('Last-Modified: '.$maintenant);
            header('Expires: '.$maintenant);
            header('Content-Length: '.strlen(file_get_contents($_fileName)));
            readfile($_fileName);
            
            @unlink($_fileName);
        }
        
        $response->setEtag(md5($response->getContent()));
        return $response;
    }


    /**
     * Exporter la liste des Societes dans un fichier pdf .
     *
     */
    public function PDFExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');

        $em = $this->getDoctrine()->getManager();

        $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, 0, $session->get('user_role'));

                $html = $this->renderView('@CrmSociete/Societe/societes_pdf.html.twig', array(
                    'societes' => $societes
                ));
        
        $this->container->get('tracabilite.trace')->setTrace('Societe','Exporter liste des societes en pdf ');

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="societes-export.pdf"',
                'orientation'           => 'Landscape'
            )
        );

    }

    /**
     * Supprimer le logo ( logo1 ou logo2) de la société button supremer fiche société .
     *
     */
    public function deleteLogoAction($id,$idLogo){

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');

        $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($id);
        if(!$societe) {
             $this->get('session')->set('flashMessageUserError', "Societe  non trouvé !");
        }

        if($idLogo == 1 ){
            $societe->setIdLogo1(null);
        }

        if($idLogo == 2 ){
            $societe->setIdLogo2(null);
        }

        $em->persist($societe);
        $em->flush();

        $flashMsg = "Logo supprimé avec succès "; 
        $this->get('session')->set('flashMessageUser', $flashMsg);
        //return $this->redirect($this->generateUrl('crm_societe_edit',array('id' => $id )));
        return $this->redirect($this->generateUrl('crm_config_societe_homepage'));

        
    }


    /**
     * Supprime des societes en masse on se base sur l'array session des societes sélectionnés.
     *
     */
    public function deleteAllAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $em = $this->getDoctrine()->getManager();
        $session = new Session();
        $_societesIDs = array();

        if($session->has('affectSocietesIDs'))
            $_societesIDs = $session->get('affectSocietesIDs');
        //$session->remove('affectSocietesIDs');

        //print_r($_societesIDs);exit;

        if(count($_societesIDs) > 0 ) {

            foreach ($_societesIDs as $key => $societe) {
                $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($key);
                //dump($societe);exit;

                $this->container->get('tracabilite.trace')->setTrace('Societe','Suppression societe référence = '.$societe->getIdGerant()->getNom().' id = '.$key);
                
                $societe->setSupprimer(true);
                //$em->remove($societe);
                $em->flush();
            }

            $flashMsg = "Societes supprimés avec succès "; 
            $this->get('session')->set('flashMessageUser', $flashMsg);
            return $this->redirect($this->generateUrl('crm_societe_homepage'));


        } else {
            $flashMsg = "Merci de sélectionner les sociétés à supprimer "; 
            $this->get('session')->set('flashMessageUserError', $flashMsg);
            return $this->redirect($this->generateUrl('crm_societe_homepage'));
        }
        

        return new Response();
    }


    /**
     * Ajouter a la session( tableau 1 dimenssion) les societes selectionner
     */
    public function checkedAction(Request $request) {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = $request->getSession();
        $session->set('selectAll', '0');

        //$session->remove('affectSocietesIDs');

        $_societeIDs = array();

        if($request->isMethod('POST')) {
            $_societe_id = $request->request->get('id_societe');
            $_checked = $request->request->get('checked');


            if($session->has('affectSocietesIDs')) { // la session existe, et il faut ajouter
                $_societeIDs = $session->get('affectSocietesIDs');
                    if($_checked == 'true') {
                        $_societeIDs[$_societe_id]=$_societe_id;
                    } else {
                        if(isset($_societeIDs[$_societe_id]))
                            unset($_societeIDs[$_societe_id]);
                    }
                $session->set('affectSocietesIDs',  $_societeIDs);
            } else {
               //la session n'existe pas et donc aucun ID sauvegardé encore
                $_societeIDs[$_societe_id] = $_societe_id;
                $session->set('affectSocietesIDs',  $_societeIDs);
            }
        }


        dump($_societeIDs);
        exit;

        return new Response();
    }
    
}
