<?php

namespace Crm\SocieteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Crm\SocieteBundle\Entity\Societe;
use Crm\SocieteBundle\Entity\infos_societe;
use Crm\SocieteBundle\Form\ConfigurationType;
use Utilisateurs\UserBundle\Services\AuthUser;
use Tracabilite\TraceBundle\Services\Trace;
use Entities\EntityBundle\Services\ServiceEntities;

class ConfigurationController extends Controller
{
    /**
     * Affiche la page principale du module de configuration société.
     *
     */
    public function indexAction(Request $request) {
        
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');
        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');
        
        $idUserConnect = $session->get('user_id');
        $infosSocieteArr = array();
        //$contactsSociete = array();
        $banquesSociete = array();
        $logo1_url = '';
        $logo2_url = '';

        $_editMod = 1;
        $em = $this->getDoctrine()->getManager();
        $societes = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base);
        $id = $societes[0]['id'];
        $societeId = $id;

        $societe = $em->getRepository('CrmSocieteBundle:Societe')->find($id);
        //dump($societe);exit;
        if(!$societe) {
            $this->get('session')->set('flashMessageUserError', "Societe non trouvé !");
        }
        $infosSocieteArr = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($app_base, $id);
       // $contactsSociete = $em->getRepository('CrmContactBundle:Contact')->findAllContactBySociete($id);
        $banquesSociete = $em->getRepository('CrmSocieteBundle:BanqueInfos')->findInfosBanqueSociete($id);
        if($societe->getIdLogo1() !== null ) $logo1_url = basename($societe->getIdLogo1()->getUrl());
        if($societe->getIdLogo2() !== null ) $logo2_url = basename($societe->getIdLogo2()->getUrl());

        $parametres_array = array( "prefix_reference_facture" => null,
                                   "prefix_depart_reference_facture" => null,
                                   "debut_exercice" => null,
                                   "fin_exercice" => null,
                                   "journal_achats" => "AC",
                                   "journal_ventes" => "VT",
                                   "compta_treso" => "0",
                                   "regime_tva" => "ST");

        $parametres = $em->getRepository('CrmSocieteBundle:Societe')->findParametres($id);
        if(isset($parametres[0]) AND count($parametres[0]) > 0 ) $parametres_array = $parametres[0];
        

        $form = $this->createForm(ConfigurationType::class, $societe, array(
            'app_base' => $app_base,
            'infos_societe' => $infosSocieteArr
        ));

    
        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {

                $contacts = $request->get('contacts');

                $data = array(
                    'idSociete' => $societeId,
                    'idSocieteEnt' => $request->get("idSocieteEnt"),
                    'idPersone' => $request->get("idPersone"),
                    'idEmail' => $request->get("idEmail"),
                    'idPhone' => $request->get("idPhone"),
                    'idFax' => $request->get("idFax"),
                    'idAdresseSiege' => $request->get("idAdresseSiege"),
                    'idAdresseFacturation' => $request->get("idAdresseFacturation"),
                    'idStatut' => $form->get("idStatut")->getData()->getId(),
                    'idSecteur' => $form->get("idSecteur")->getData()->getId(),
                    'raisonSociale' => $form->get("raisonSocial")->getData(),
                    'typeSociete' => $form->get("type")->getData()->getId(),
                    'emailSociete' => $form->get("emailContact")->getData(),
                    'nomResponsable' => $form->get("responsable")->getData(),
                    'phoneSociete' => $form->get("fixe")->getData(),
                    'faxSociete' => $form->get("fax")->getData(),
                    'siren' => $form->get("siren")->getData(),
                    'siret' => $form->get("siret")->getData(),
                    'numeroTva' => $form->get("numeroTva")->getData(),
                    'codeApe' => $form->get("codeApe")->getData(),
                    'capital' => $form->get("capital")->getData(),
                    'rcs' => $form->get("rcs")->getData(),
                    'siteWeb' => $form->get("siteWeb")->getData(),
                    'numeroAgrementFormateur' => $form->get("numeroAgrementFormateur")->getData(),
                    'adresseSiege' => $request->get("adresseSiege"),
                    'villeSiege' => $request->get("villeSiege"),
                    'codePostaleSiege' => $request->get("codePostaleSiege"),
                    'paysSiege' => $request->get("paysSiege"),
                    'adresseFact' => $request->get("adresseFact"),
                    'villeFact' => $request->get("villeFact"),
                    'codePostaleFact' => $request->get("codePostaleFact"),
                    'paysFact' => $request->get("paysFact"),
                    //'contacts' => $request->get('contacts'),
                    'banques' => $request->get('banques')
                );

                //dump($data);exit;
                    
                // add societe ent
                $idSocieteEnt = $this->container->get('service.entities')->addSociete($data['idSocieteEnt'],$data['raisonSociale'],$data['numeroTva'],$data['siret'],$data['codeApe'],$data['capital'],$data['rcs'],$data['siteWeb'],$data['numeroAgrementFormateur'],$data['siren'],'');
                // add adresse siege
                $idAdresseSiege = $this->container->get('service.entities')->addAdresse($data['idAdresseSiege'],$data['adresseSiege'],$data['villeSiege'],$data['codePostaleSiege'],$data['paysSiege']);
                // add adresse facturation 
                 $idAdresseFacturation = $this->container->get('service.entities')->addAdresse($data['idAdresseFacturation'],$data['adresseFact'],$data['villeFact'],$data['codePostaleFact'],$data['paysFact']);
                // add persone 
                 $idPersone = $this->container->get('service.entities')->addPersone($data['idPersone'],$data['nomResponsable'],'','','');
                // add email
                 $idEmail = $this->container->get('service.entities')->addEmail($data['idEmail'],$data['emailSociete'],'societe');
                // add phone
                 $idPhone = $this->container->get('service.entities')->addPhone($data['idPhone'],$data['phoneSociete'],'fixe');
                // add fax
                 $idFax = $this->container->get('service.entities')->addPhone($data['idFax'],$data['faxSociete'],'fax');

                // add societe 
                 $societe->setBase($app_base);
                 $societe->setIdEntSociete($idSocieteEnt);
                 $societe->setIdGerant($idPersone);
                 $societe->setIdAdresseSiege($idAdresseSiege);
                 $societe->setIdAdresseFacturation($idAdresseFacturation);
                 $societe->setIdEmail($idEmail);
                 $societe->setIdTelSiege($idPhone);
                 $societe->setIdFaxSiege($idFax);

                 $secteur = $em->getRepository('CrmSocieteBundle:SecteurActivite')->find($data['idSecteur']);
                 $statut = $em->getRepository('CrmSocieteBundle:Statut')->find($data['idStatut']);
                 $typeSociete =  $em->getRepository('CrmSocieteBundle:TypeSociete')->find($data['typeSociete']);

                 $societe->setIdSecteur($secteur);
                 $societe->setIdStatut($statut);
                 $societe->setIdType($typeSociete);


                $em->persist($societe);
                $em->flush();
                $idSociete = $societe->getId();

                // add logo file 
                $dir = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/logos/';
                $image1 = $form['logo1']->getData();
                $extensions = array('.jpg','.jpeg','.png'); 
                if( $image1 !== null  ) {
                    $return_logo1 = $this->container->get('service.entities')->addFile($dir,$image1,$extensions,$idSociete,'Societe');
                    if($return_logo1['erreur'] == '0'){
                        $IdLogo1 = $return_logo1['idFile'];
                        $societe->setIdLogo1($IdLogo1);
                    } else {
                       $this->get('session')->set('flashMessageUserError', $return_logo1['msgErreur']);
                    }
                }

                $image2 = $form['logo2']->getData();
                //dump($image2);exit;
                if( $image2 !== null  ) {
                    $return_logo2 = $this->container->get('service.entities')->addFile($dir,$image2,$extensions,$idSociete,'Societe');
                    if($return_logo2['erreur'] == '0'){
                        $IdLogo2 = $return_logo2['idFile'];
                        $societe->setIdLogo2($IdLogo2);
                    } else {
                       $this->get('session')->set('flashMessageUserError', $return_logo2['msgErreur']);
                    }
                }

                // add contacts 
                //if(isset($data['contacts'])) 
                  //  $this->container->get('service.entities')->addContacts($idSociete, $data['contacts']);

                // add banques
                if(isset($data['banques']))
                    $em->getRepository('CrmSocieteBundle:BanqueInfos')->addBanques($idSociete, $data['banques']);

                if( ( isset($return_logo1['erreur']) AND $return_logo1['erreur'] == 1 )  OR  ( isset($return_logo2['erreur']) AND $return_logo2['erreur'] == 1 ) ) {
                    return $this->redirect($this->generateUrl('crm_config_societe_homepage'));
                } else {
                    $em->persist($societe);
                    $em->flush();
                }


                // Parametres société
                $parametres_data = array( 
                    "prefix_reference_facture" => $request->get("prefix_reference_facture"),
                    "prefix_depart_reference_facture" => $request->get("prefix_depart_reference_facture"),
                    "debut_exercice" => $request->get("debut_exercice"),
                    "fin_exercice" => $request->get("fin_exercice"),
                    "journal_achats" => $request->get("journal_achats"),
                    "journal_ventes" => $request->get("journal_ventes"),
                    "compta_treso" => $request->get("compta_treso"),
                    "regime_tva" => $request->get("regimetva")
                    );
                //dump($parametres_data);exit;
                $em->getRepository('CrmSocieteBundle:Societe')->addParameters($idSociete,$parametres_data,2);


                $this->get('session')->set('flashMessageUser', "Modification enregistrée avec succès !");

                 // Enregistrer trace ajout/modification societe
                $this->container->get('tracabilite.trace')->setTrace('Societe','Modification de configuration de la société '.$societe->getBase().' id = '.$societeId);
                
                return $this->redirect($this->generateUrl('crm_config_societe_homepage'));
            }
        }

        $pays = $em->getRepository('CrmClientBundle:Pays')->findAll();
    
        return $this->render('@CrmSociete/Configuration/index.html.twig', array(
                'editMod' => 1,
                'societeId' => $societeId,
                'infosSociete' => $infosSocieteArr[0],
                //'contactsSociete' => $contactsSociete,
                'banquesSociete' => $banquesSociete,
                'classActive' => 'configuration',
                'form' => $form->createView(),
                'pays' => $pays,
                'logo1_url' => $logo1_url,
                'logo2_url' => $logo2_url,
                'parametres' => $parametres_array
        ));
    }

    
}
