<?php

namespace Crm\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Societe
 *
 * @ORM\Table(name="societe")
 * @ORM\Entity(repositoryClass="Crm\SocieteBundle\Repository\SocieteRepository")
 */
class Societe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Entities\EntityBundle\Entity\EntSociete
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntSociete")
     * @ORM\JoinColumn(name="id_ent_societe", referencedColumnName="id")
     * 
     */
    private $idEntSociete;

    /**
     * @var \Entities\EntityBundle\Entity\EntPersone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntPersone")
     * @ORM\JoinColumn(name="id_gerant", referencedColumnName="id")
     * 
     */
    private $idGerant;

    /**
     * @var \Entities\EntityBundle\Entity\EntAdresse
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntAdresse")
     * @ORM\JoinColumn(name="id_adresse_siege", referencedColumnName="id")
     * 
     */
    private $idAdresseSiege;

    /**
     * @var \Entities\EntityBundle\Entity\EntAdresse
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntAdresse")
     * @ORM\JoinColumn(name="id_adresse_fact", referencedColumnName="id")
     * 
     */
    private $idAdresseFacturation;

    /**
     * @var \Entities\EntityBundle\Entity\EntEmail
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntEmail")
     * @ORM\JoinColumn(name="id_email", referencedColumnName="id")
     * 
     */
    private $idEmail;

    /**
     * @var \Entities\EntityBundle\Entity\EntTelephone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntTelephone")
     * @ORM\JoinColumn(name="id_tel_siege", referencedColumnName="id")
     * 
     */
    private $idTelSiege;


    /**
     * @var \Entities\EntityBundle\Entity\EntTelephone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntTelephone")
     * @ORM\JoinColumn(name="id_fax_siege", referencedColumnName="id")
     * 
     */
    private $idFaxSiege;


    /**
     * @var \Crm\SocieteBundle\Entity\Statut
     * @ORM\ManyToOne(targetEntity="Statut")
     * @ORM\JoinColumn(name="id_statut", referencedColumnName="id")
     * 
     */
    private $idStatut;

    /**
     * @var \Crm\SocieteBundle\Entity\SecteurActivite
     * @ORM\ManyToOne(targetEntity="SecteurActivite")
     * @ORM\JoinColumn(name="id_secteur", referencedColumnName="id")
     * 
     */
    private $idSecteur;

    /**
     * @var \Crm\SocieteBundle\Entity\TypeSociete
     * @ORM\ManyToOne(targetEntity="TypeSociete")
     * @ORM\JoinColumn(name="id_type", referencedColumnName="id")
     * 
     */
    private $idType;

    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=20)
     */
    private $base;


    /**
     * @var string
     *
     * @ORM\Column(name="soctype", type="string", length=255, nullable=true)
     */
    private $soctype;

    /**
     * @var string
     *
     * @ORM\Column(name="comptable", type="string", length=20, nullable=true)
     */
    private $comptable;
     

    /**
     * @var \Entities\EntityBundle\Entity\EntFiles
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntFiles")
     * @ORM\JoinColumn(name="id_logo1", referencedColumnName="id")
     * 
     */
    private $idLogo1;

    /**
     * @var \Entities\EntityBundle\Entity\EntFiles
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntFiles")
     * @ORM\JoinColumn(name="id_logo2", referencedColumnName="id")
     * 
     */
    private $idLogo2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="supprimer", type="boolean", nullable=false)
     */
    private $supprimer;

    public function __construct()
    {
        $this->supprimer = false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEntSociete
     *
     * @param \Entities\EntityBundle\Entity\EntSociete $idEntSociete
     *
     * @return \Entities\EntityBundle\Entity\EntSociete
     */
    public function setIdEntSociete(\Entities\EntityBundle\Entity\EntSociete $idEntSociete=null)
    {
        $this->idEntSociete = $idEntSociete;

        return $this;
    }

    /**
     * Get idEntSociete
     *
     * @return \Entities\EntityBundle\Entity\EntSociete
     */
    public function getIdEntSociete()
    {
        return $this->idEntSociete;
    }

    /**
     * Set idGerant
     *
     * @param \Entities\EntityBundle\Entity\EntPersone $idGerant
     *
     * @return \Entities\EntityBundle\Entity\EntPersone
     */
    public function setIdGerant(\Entities\EntityBundle\Entity\EntPersone $idGerant = null)
    {
        $this->idGerant = $idGerant;

        return $this;
    }

    /**
     * Get idGerant
     *
     * @return \Entities\EntityBundle\Entity\EntPersone
     */
    public function getIdGerant()
    {
        return $this->idGerant;
    }

    /**
     * Set idAdresseSiege
     *
     * @param \Entities\EntityBundle\Entity\EntAdresse $idAdresseSiege
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function setIdAdresseSiege(\Entities\EntityBundle\Entity\EntAdresse $idAdresseSiege = null)
    {
        $this->idAdresseSiege = $idAdresseSiege;

        return $this;
    }

    /**
     * Get idAdresseSiege
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function getIdAdresseSiege()
    {
        return $this->idAdresseSiege;
    }

    /**
     * Set idAdresseFacturation
     *
     * @param \Entities\EntityBundle\Entity\EntAdresse $idAdresseFacturation
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function setIdAdresseFacturation(\Entities\EntityBundle\Entity\EntAdresse $idAdresseFacturation = null)
    {
        $this->idAdresseFacturation = $idAdresseFacturation;

        return $this;
    }

    /**
     * Get idAdresseFacturation
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function getIdAdresseFacturation()
    {
        return $this->idAdresseFacturation;
    }

    /**
     * Set idEmail
     *
     * @param \Entities\EntityBundle\Entity\EntEmail $idEmail
     *
     * @return \Entities\EntityBundle\Entity\EntEmail
     */
    public function setIdEmail(\Entities\EntityBundle\Entity\EntEmail $idEmail=null)
    {
        $this->idEmail = $idEmail;

        return $this;
    }

    /**
     * Get idEmail
     *
     * @return \Entities\EntityBundle\Entity\EntEmail
     */
    public function getIdEmail()
    {
        return $this->idEmail;
    }

    /**
     * Set idTelSiege
     *
     * @param \Entities\EntityBundle\Entity\EntTelephone $idTelSiege
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function setIdTelSiege(\Entities\EntityBundle\Entity\EntTelephone $idTelSiege = null)
    {
        $this->idTelSiege = $idTelSiege;

        return $this;
    }

    /**
     * Get idTelSiege
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function getIdTelSiege()
    {
        return $this->idTelSiege;
    }

    /**
     * Set idFaxSiege
     *
     * @param \Entities\EntityBundle\Entity\EntTelephone $idFaxSiege
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function setIdFaxSiege(\Entities\EntityBundle\Entity\EntTelephone $idFaxSiege = null)
    {
        $this->idFaxSiege = $idFaxSiege;

        return $this;
    }

    /**
     * Get idFaxSiege
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function getIdFaxSiege()
    {
        return $this->idFaxSiege;
    }

    /**
     * Set idLogo1
     *
     * @param \Entities\EntityBundle\Entity\EntFiles $idLogo1
     *
     * @return \Entities\EntityBundle\Entity\EntFiles
     */
    public function setIdLogo1(\Entities\EntityBundle\Entity\EntFiles $idLogo1 = null)
    {
        $this->idLogo1 = $idLogo1;

        return $this;
    }

    /**
     * Get idLogo1
     *
     * @return \Entities\EntityBundle\Entity\EntFiles
     */
    public function getIdLogo1()
    {
        return $this->idLogo1;
    }

    /**
     * Set idLogo2
     *
     * @param \Entities\EntityBundle\Entity\EntFiles $idLogo2
     *
     * @return \Entities\EntityBundle\Entity\EntFiles
     */
    public function setIdLogo2(\Entities\EntityBundle\Entity\EntFiles $idLogo2 = null)
    {
        $this->idLogo2 = $idLogo2;

        return $this;
    }

    /**
     * Get idLogo2
     *
     * @return \Entities\EntityBundle\Entity\EntFiles
     */
    public function getIdLogo2()
    {
        return $this->idLogo2;
    }

    /**
     * Set idStatut
     *
     * @param \Crm\SocieteBundle\Entity\Statut $idStatut
     *
     * @return \Crm\SocieteBundle\Entity\Statut
     */
    public function setIdStatut(\Crm\SocieteBundle\Entity\Statut $idStatut = null)
    {
        $this->idStatut = $idStatut;

        return $this;
    }

    /**
     * Get idStatut
     *
     * @return \Crm\SocieteBundle\Entity\Statut
     */
    public function getIdStatut()
    {
        return $this->idStatut;
    }

    /**
     * Set idSecteur
     *
     * @param \Crm\SocieteBundle\Entity\SecteurActivite $idSecteur
     *
     * @return \Crm\SocieteBundle\Entity\SecteurActivite
     */
    public function setIdSecteur(\Crm\SocieteBundle\Entity\SecteurActivite $idSecteur = null)
    {
        $this->idSecteur = $idSecteur;

        return $this;
    }

    /**
     * Get idSecteur
     *
     * @return \Crm\SocieteBundle\Entity\SecteurActivite
     */
    public function getIdSecteur()
    {
        return $this->idSecteur;
    }


    /**
     * Set idType
     *
     * @param integer $idType
     *
     * @return Societe
     */

    /**
     * Set idType
     *
     * @param \Crm\SocieteBundle\Entity\TypeSociete $idType
     *
     * @return \Crm\SocieteBundle\Entity\TypeSociete
     */
    public function setIdType(\Crm\SocieteBundle\Entity\TypeSociete $idType = null)
    {
        $this->idType = $idType;

        return $this;
    }

    /**
     * Get idType
     *
     * @return \Crm\SocieteBundle\Entity\TypeSociete 
     */
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set base
     *
     * @param string $base
     *
     * @return Societe
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set soctype
     *
     * @param string $soctype
     *
     * @return Societe
     */
    public function setSoctype($soctype)
    {
        $this->soctype = $soctype;

        return $this;
    }

    /**
     * Get soctype
     *
     * @return string
     */
    public function getSoctype()
    {
        return $this->soctype;
    }

    /**
     * Set comptable
     *
     * @param string $comptable
     *
     * @return Societe
     */
    public function setComptable($comptable)
    {
        $this->comptable = $comptable;

        return $this;
    }

    /**
     * Get comptable
     *
     * @return string
     */
    public function getComptable()
    {
        return $this->comptable;
    }


    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     *
     * @return Users
     */
    public function setSupprimer($supprimer)
    {
        $this->supprimer = $supprimer;

        return $this;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getSupprimer()
    {
        return $this->supprimer;
    }

}

