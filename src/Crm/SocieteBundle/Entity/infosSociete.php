<?php

namespace Crm\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * infosSociete
 *
 * @ORM\Table(name="infos_societe")
 * @ORM\Entity(repositoryClass="Crm\SocieteBundle\Repository\infosSocieteRepository")
 */
class infosSociete
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=20)
     */
    private $base;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $capital;

    /**
     * @var string
     *
     * @ORM\Column(name="assurance", type="string", length=255, nullable=true)
     */
    private $assurance;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_agrement_formateur", type="string", length=255, nullable=true)
     */
    private $numeroAgrementFormateur;

    /**
     * @var string
     *
     * @ORM\Column(name="site_web", type="string", length=255, nullable=true)
     */
    private $siteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="rcs", type="string", length=255, nullable=true)
     */
    private $rcs;

    /**
     * @var string
     *
     * @ORM\Column(name="logo1", type="string", length=255, nullable=true)
     */
    private $logo1;

    /**
     * @var string
     *
     * @ORM\Column(name="logo2", type="string", length=255, nullable=true)
     */
    private $logo2;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set base
     *
     * @param string $base
     *
     * @return infosSociete
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set capital
     *
     * @param string $capital
     *
     * @return infosSociete
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set assurance
     *
     * @param string $assurance
     *
     * @return infosSociete
     */
    public function setAssurance($assurance)
    {
        $this->assurance = $assurance;

        return $this;
    }

    /**
     * Get assurance
     *
     * @return string
     */
    public function getAssurance()
    {
        return $this->assurance;
    }

    /**
     * Set numeroAgrementFormateur
     *
     * @param string $numeroAgrementFormateur
     *
     * @return infosSociete
     */
    public function setNumeroAgrementFormateur($numeroAgrementFormateur)
    {
        $this->numeroAgrementFormateur = $numeroAgrementFormateur;

        return $this;
    }

    /**
     * Get numeroAgrementFormateur
     *
     * @return string
     */
    public function getNumeroAgrementFormateur()
    {
        return $this->numeroAgrementFormateur;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return infosSociete
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * Set rcs
     *
     * @param string $rcs
     *
     * @return infosSociete
     */
    public function setRcs($rcs)
    {
        $this->rcs = $rcs;

        return $this;
    }

    /**
     * Get rcs
     *
     * @return string
     */
    public function getRcs()
    {
        return $this->rcs;
    }

    /**
     * Set logo1
     *
     * @param string $logo1
     *
     * @return infosSociete
     */
    public function setLogo1($logo1)
    {
        $this->logo1 = $logo1;

        return $this;
    }

    /**
     * Get logo1
     *
     * @return string
     */
    public function getLogo1()
    {
        return $this->logo1;
    }

    /**
     * Set logo2
     *
     * @param string $logo2
     *
     * @return infosSociete
     */
    public function setLogo2($logo2)
    {
        $this->logo2 = $logo2;

        return $this;
    }

    /**
     * Get logo2
     *
     * @return string
     */
    public function getLogo2()
    {
        return $this->logo2;
    }
}

