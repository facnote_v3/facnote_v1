<?php

namespace Crm\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BanqueInfos
 *
 * @ORM\Table(name="banque_infos")
 * @ORM\Entity(repositoryClass="Crm\SocieteBundle\Repository\BanqueInfosRepository")
 */
class BanqueInfos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Crm\SocieteBundle\Entity\Societe
     * @ORM\ManyToOne(targetEntity="\Crm\SocieteBundle\Entity\Societe")
     * @ORM\JoinColumn(name="id_societe", referencedColumnName="id")
     * 
     */
    private $idSociete;

    /**
     * @var string
     *
     * @ORM\Column(name="banque", type="string", length=255)
     */
    private $banque;

    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=255, nullable=true)
     */
    private $iban;

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=255, nullable=true)
     */
    private $bic;

    /**
     * @var string
     *
     * @ORM\Column(name="rib", type="string", length=255, nullable=true)
     */
    private $rib;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSociete
     *
     * @param \Crm\SocieteBundle\Entity\Societe $idSociete
     *
     * @return \Crm\SocieteBundle\Entity\Societe
     */
    public function setIdSociete(\Crm\SocieteBundle\Entity\Societe $idSociete=null)
    {
        $this->idSociete = $idSociete;

        return $this;
    }

    /**
     * Get idSociete
     *
     * @return \Crm\SocieteBundle\Entity\Societe
     */
    public function getIdSociete()
    {
        return $this->idSociete;
    }

    /**
     * Set banque
     *
     * @param string $banque
     *
     * @return BanqueInfos
     */
    public function setBanque($banque)
    {
        $this->banque = $banque;

        return $this;
    }

    /**
     * Get banque
     *
     * @return string
     */
    public function getBanque()
    {
        return $this->banque;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return BanqueInfos
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bic
     *
     * @param string $bic
     *
     * @return BanqueInfos
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set rib
     *
     * @param string $rib
     *
     * @return BanqueInfos
     */
    public function setRib($rib)
    {
        $this->rib = $rib;

        return $this;
    }

    /**
     * Get rib
     *
     * @return string
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return BanqueInfos
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

