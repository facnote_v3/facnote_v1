<?php

namespace Crm\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecteurActivite
 *
 * @ORM\Table(name="secteur_activite")
 * @ORM\Entity(repositoryClass="Crm\SocieteBundle\Repository\SecteurActiviteRepository")
 */
class SecteurActivite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="secteur", type="string", length=355)
     */
    private $secteur;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set secteur
     *
     * @param string $secteur
     *
     * @return SecteurActivite
     */
    public function setSecteur($secteur)
    {
        $this->secteur = $secteur;

        return $this;
    }

    /**
     * Get secteur
     *
     * @return string
     */
    public function getSecteur()
    {
        return $this->secteur;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return SecteurActivite
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

