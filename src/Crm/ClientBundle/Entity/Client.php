<?php

namespace Crm\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="Crm\ClientBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Utilisateurs\UserBundle\Entity\Users
     * @ORM\ManyToOne(targetEntity="\Utilisateurs\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="id_commercial", referencedColumnName="id")
     * 
     */
    private $idCommercial;

    /**
     * @var \Crm\ClientBundle\Entity\Pays
     * @ORM\ManyToOne(targetEntity="Pays")
     * @ORM\JoinColumn(name="id_pays", referencedColumnName="id")
     * 
     */
    private $idPays;


    /**
     * @var \Crm\ClientBundle\Entity\typeClient
     * @ORM\ManyToOne(targetEntity="typeClient")
     * @ORM\JoinColumn(name="id_type", referencedColumnName="id")
     * 
     */
    private $idType;

    
    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=10, nullable=false)
     */
    private $base;

    /**
     * @var \Entities\EntityBundle\Entity\EntPersone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntPersone")
     * @ORM\JoinColumn(name="id_persone", referencedColumnName="id")
     * 
     */
    private $idPersone;

    /**
     * @var \Entities\EntityBundle\Entity\EntAdresse
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntAdresse")
     * @ORM\JoinColumn(name="id_adresse_livraison", referencedColumnName="id")
     * 
     */
    private $idAdresseLivraison;

    /**
     * @var \Entities\EntityBundle\Entity\EntAdresse
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntAdresse")
     * @ORM\JoinColumn(name="id_adresse_facturation", referencedColumnName="id")
     * 
     */
    private $idAdresseFacturation;

    /**
     * @var \Entities\EntityBundle\Entity\EntEmail
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntEmail")
     * @ORM\JoinColumn(name="id_email", referencedColumnName="id")
     * 
     */
    private $idEmail;

    /**
     * @var \Entities\EntityBundle\Entity\EntTelephone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntTelephone")
     * @ORM\JoinColumn(name="id_tel_fixe", referencedColumnName="id")
     * 
     */
    private $idTelFixe;


    /**
     * @var \Entities\EntityBundle\Entity\EntTelephone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntTelephone")
     * @ORM\JoinColumn(name="id_tel_mobile", referencedColumnName="id")
     * 
     */
    private $idTelMobile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="supprimer", type="boolean", nullable=false)
     */
    private $supprimer;

    public function __construct()
    {
        $this->supprimer = false;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCommercial
     *
     * @param integer $idCommercial
     *
     * @return Client
     */
    public function setIdCommercial($idCommercial)
    {
        $this->idCommercial = $idCommercial;

        return $this;
    }

    /**
     * Get idCommercial
     *
     * @return int
     */
    public function getIdCommercial()
    {
        return $this->idCommercial;
    }

    /**
     * Set base
     *
     * @param string $base
     *
     * @return Users
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set idPersone
     *
     * @param \Entities\EntityBundle\Entity\EntPersone $idPersone
     *
     * @return \Entities\EntityBundle\Entity\EntPersone
     */
    public function setIdPersone(\Entities\EntityBundle\Entity\EntPersone $idPersone = null)
    {
        $this->idPersone = $idPersone;

        return $this;
    }

    /**
     * Get idPersone
     *
     * @return \Entities\EntityBundle\Entity\EntPersone
     */
    public function getIdPersone()
    {
        return $this->idPersone;
    }

    /**
     * Set idAdresseLivraison
     *
     * @param \Entities\EntityBundle\Entity\EntAdresse $idAdresseLivraison
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function setIdAdresseLivraison(\Entities\EntityBundle\Entity\EntAdresse $idAdresseLivraison = null)
    {
        $this->idAdresseLivraison = $idAdresseLivraison;

        return $this;
    }

    /**
     * Get idAdresseLivraison
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function getIdAdresseLivraison()
    {
        return $this->idAdresseLivraison;
    }

    /**
     * Set idAdresseFacturation
     *
     * @param \Entities\EntityBundle\Entity\EntAdresse $idAdresseFacturation
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function setIdAdresseFacturation(\Entities\EntityBundle\Entity\EntAdresse $idAdresseFacturation = null)
    {
        $this->idAdresseFacturation = $idAdresseFacturation;

        return $this;
    }

    /**
     * Get idAdresseFacturation
     *
     * @return \Entities\EntityBundle\Entity\EntAdresse
     */
    public function getIdAdresseFacturation()
    {
        return $this->idAdresseFacturation;
    }

    /**
     * Set idEmail
     *
     * @param \Entities\EntityBundle\Entity\EntEmail $idEmail
     *
     * @return \Entities\EntityBundle\Entity\EntEmail
     */
    public function setIdEmail(\Entities\EntityBundle\Entity\EntEmail $idEmail=null)
    {
        $this->idEmail = $idEmail;

        return $this;
    }

    /**
     * Get idEmail
     *
     * @return \Entities\EntityBundle\Entity\EntEmail
     */
    public function getIdEmail()
    {
        return $this->idEmail;
    }

    /**
     * Set idTelFixe
     *
     * @param \Entities\EntityBundle\Entity\EntTelephone $idTelFixe
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function setIdTelFixe(\Entities\EntityBundle\Entity\EntTelephone $idTelFixe = null)
    {
        $this->idTelFixe = $idTelFixe;

        return $this;
    }

    /**
     * Get idTelFixe
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function getIdTelFixe()
    {
        return $this->idTelFixe;
    }

    /**
     * Set idTelMobile
     *
     * @param \Entities\EntityBundle\Entity\EntTelephone $idTelMobile
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function setIdTelMobile(\Entities\EntityBundle\Entity\EntTelephone $idTelMobile = null)
    {
        $this->idTelMobile = $idTelMobile;

        return $this;
    }

    /**
     * Get idTelMobile
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function getIdTelMobile()
    {
        return $this->idTelMobile;
    }

    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     *
     * @return Users
     */
    public function setSupprimer($supprimer)
    {
        $this->supprimer = $supprimer;

        return $this;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getSupprimer()
    {
        return $this->supprimer;
    }

    /**
     * Set idType
     *
     * @param \Crm\ClientBundle\Entity\typeClient $idType
     *
     * @return \Crm\ClientBundle\Entity\typeClient
     */
    public function setIdType(\Crm\ClientBundle\Entity\typeClient $idType = null)
    {
        $this->idType = $idType;

        return $this;
    }

    /**
     * Get idType
     *
     * @return \Crm\ClientBundle\Entity\typeClient
     */
    public function getIdType()
    {
        return $this->idType;
    }

}

