<?php

namespace Crm\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * infos_client
 *
 * @ORM\Table(name="infos_client")
 * @ORM\Entity(repositoryClass="Crm\ClientBundle\Repository\infos_clientRepository")
 */
class infos_client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var \Crm\ClientBundle\Entity\Client
     * @ORM\OneToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="id_client", referencedColumnName="id")
     * 
     */
    private $idClient;

    /**
     * @var string
     *
     * @ORM\Column(name="rib_bancaire", type="string", length=50, nullable=true)
     */
    private $ribBancaire;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=20, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_tva", type="string", length=30, nullable=true)
     */
    private $numeroTva;

    /**
     * @var string
     *
     * @ORM\Column(name="branche", type="string", length=255, nullable=true)
     */
    private $branche;

    /**
     * @var string
     *
     * @ORM\Column(name="email_facture", type="string", length=100, nullable=true)
     */
    private $emailFacture;

    /**
     * @var int
     *
     * @ORM\Column(name="autoliquidation", type="smallint", nullable=true)
     */
    private $autoliquidation;

    /**
     * @var string
     *
     * @ORM\Column(name="artile_autoliquidation", type="text", nullable=true)
     */
    private $artileAutoliquidation;

    /**
     * @var string
     *
     * @ORM\Column(name="article_penalites_retard", type="text", nullable=true)
     */
    private $articlePenalitesRetard;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_contact", type="string", length=255, nullable=true)
     */
    private $nomContact;


    /**
     * @var string
     *
     * @ORM\Column(name="responsable", type="string", length=255, nullable=true)
     */
    private $responsable;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction", type="string", length=255, nullable=true)
     */
    private $fonction;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return infos_client
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return int
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set ribBancaire
     *
     * @param string $ribBancaire
     *
     * @return infos_client
     */
    public function setRibBancaire($ribBancaire)
    {
        $this->ribBancaire = $ribBancaire;

        return $this;
    }

    /**
     * Get ribBancaire
     *
     * @return string
     */
    public function getRibBancaire()
    {
        return $this->ribBancaire;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return infos_client
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set numeroTva
     *
     * @param string $numeroTva
     *
     * @return infos_client
     */
    public function setNumeroTva($numeroTva)
    {
        $this->numeroTva = $numeroTva;

        return $this;
    }

    /**
     * Get numeroTva
     *
     * @return string
     */
    public function getNumeroTva()
    {
        return $this->numeroTva;
    }

    /**
     * Set branche
     *
     * @param string $branche
     *
     * @return infos_client
     */
    public function setBranche($branche)
    {
        $this->branche = $branche;

        return $this;
    }

    /**
     * Get branche
     *
     * @return string
     */
    public function getBranche()
    {
        return $this->branche;
    }

    /**
     * Set emailFacture
     *
     * @param string $emailFacture
     *
     * @return infos_client
     */
    public function setEmailFacture($emailFacture)
    {
        $this->emailFacture = $emailFacture;

        return $this;
    }

    /**
     * Get emailFacture
     *
     * @return string
     */
    public function getEmailFacture()
    {
        return $this->emailFacture;
    }

    /**
     * Set autoliquidation
     *
     * @param integer $autoliquidation
     *
     * @return infos_client
     */
    public function setAutoliquidation($autoliquidation)
    {
        $this->autoliquidation = $autoliquidation;

        return $this;
    }

    /**
     * Get autoliquidation
     *
     * @return int
     */
    public function getAutoliquidation()
    {
        return $this->autoliquidation;
    }

    /**
     * Set artileAutoliquidation
     *
     * @param string $artileAutoliquidation
     *
     * @return infos_client
     */
    public function setArtileAutoliquidation($artileAutoliquidation)
    {
        $this->artileAutoliquidation = $artileAutoliquidation;

        return $this;
    }

    /**
     * Get artileAutoliquidation
     *
     * @return string
     */
    public function getArtileAutoliquidation()
    {
        return $this->artileAutoliquidation;
    }

    /**
     * Set articlePenalitesRetard
     *
     * @param string $articlePenalitesRetard
     *
     * @return infos_client
     */
    public function setArticlePenalitesRetard($articlePenalitesRetard)
    {
        $this->articlePenalitesRetard = $articlePenalitesRetard;

        return $this;
    }

    /**
     * Get articlePenalitesRetard
     *
     * @return string
     */
    public function getArticlePenalitesRetard()
    {
        return $this->articlePenalitesRetard;
    }

    /**
     * Set nomContact
     *
     * @param string $nomContact
     *
     * @return infos_client
     */
    public function setNomContact($nomContact)
    {
        $this->nomContact = $nomContact;

        return $this;
    }

    /**
     * Get nomContact
     *
     * @return string
     */
    public function getNomContact()
    {
        return $this->nomContact;
    }

    /**
     * Set responsable
     *
     * @param string $responsable
     *
     * @return infos_client
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }

    /**
     * Get responsable
     *
     * @return string
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     *
     * @return infos_client
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }


}

