<?php

namespace Crm\ClientBundle\Repository;

/**
 * ClientRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ClientRepository extends \Doctrine\ORM\EntityRepository
{
	/**
     * Retourne la liste des clients trouvées dans la base de données d'un dossier 
     *
     */
    public function findAllClientByBase($base, $idClient = null ) {
       /*$em = $this->getEntityManager();
        $dq = $em->createQueryBuilder('c');
        $dq->select('c.id,c.nom,c.prenom,c.email,c.adresse,c.mobile,c.fixe,c.fax,c.ville,c.nomContact,t.type as typeClient ')
            ->from('CrmClientBundle:Client', 'c')
            ->join('c.idType', 't')
            ->where('c.base = :baseClient')
            ->orderBy('c.nom', 'ASC')
            ->setParameter('baseClient', $base);
        $query = $dq->getQuery();
        return $query->getResult(); */

        $con = $this->getEntityManager()->getConnection();
        $sql = 'SELECT * FROM v_client';
        $cond = '';
        if(isset($base) AND strlen($base)>4 ){ $cond .= " AND numeroDossier = '".$base."' "; }
        if(isset($idClient) ){ $cond .= " AND  idClient = '".$idClient."' "; }
        if($cond != '') $sql = $sql . " WHERE 1=1 ".$cond;
        //echo $sql;exit;
        $stmt = $con->executeQuery($sql);
        $result = $stmt->fetchAll();
        return $result;
    }
    
    /**
     * Supprimer les infos d'un client table infos_client 
     *
     */
    public function deleteInfosClient($id) {
        $q = $this->getEntityManager()->getConnection()->prepare("DELETE FROM infos_client WHERE idClient = :id;");
            $q->bindValue("id",$id);
            $q->execute();
    }
}
