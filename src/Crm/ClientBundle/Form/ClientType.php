<?php

namespace Crm\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Crm\ClientBundle\Entity\infos_client;

class ClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {    
         $infosClient = $options['infos_client'];

         $builder->add('nom', TextType::class, array(
                                            'required' => true,
                                            'label' => 'Nom *',
                                            'mapped' => false,
                                            'data' => $infosClient['nom'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('prenom', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Prénom ',
                                            'mapped' => false,
                                            'data' => $infosClient['prenom'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('fonction', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Fonction ',
                                            'mapped' => false,
                                            'data' => $infosClient['fonction'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('nomContact', TextType::class, array(
                                            'required' => false,
                                            'mapped' => false,
                                            'label' => 'Nom de contact *',
                                            'data' => $infosClient['nomContact'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('responsable', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Responsable ',
                                            'mapped' => false,
                                            'data' => $infosClient['responsable'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('fax', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Fax',
                                            'mapped' => false,
                                            'data' => $infosClient['fax'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('siret', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Siret',
                                            'mapped' => false,
                                            'data' => $infosClient['siret'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('numTva', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Numéro TVA',
                                            'mapped' => false,
                                            'data' => $infosClient['numeroTva'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('cpFacturation', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Code postal',
                                            'mapped' => false,
                                            'data' => $infosClient['cpFacturation'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('villeFacturation', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Ville ',
                                            'mapped' => false,
                                            'data' => $infosClient['villeFacturation'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('adresseFacturation', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Adresse facturation',
                                            'mapped' => false,
                                            'data' => $infosClient['adresseFacturation'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )

               ->add('cpLivraison', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Code postal',
                                            'mapped' => false,
                                            'data' => $infosClient['cpLivraison'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('villeLivraison', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Ville ',
                                            'mapped' => false,
                                            'data' => $infosClient['villeLivraison'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('adresseLivraison', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Adresse livraison',
                                            'mapped' => false,
                                            'data' => $infosClient['adresseLivraison'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('fixe', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Tél Bureau',
                                            'mapped' => false,
                                            'data' => $infosClient['telephoneFixe'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('mobile', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Tél Mobile',
                                            'mapped' => false,
                                            'data' => $infosClient['telephoneMobile'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('email', EmailType::class, array(
                                                'required' => false,
                                                'label' => ' Email ',
                                                'mapped' => false,
                                                'data' => $infosClient['mail'],
                                                'attr' => array('class' => 'form-control')
                                            )
                )
                /*->add('codePostal', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Code postal',
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('ville', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Ville',
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('adresse', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Adresse',
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               /* ->add('emailFacture', EmailType::class, array(
                                            'required' => false,
                                            'label' => 'E-mail facture',
                                            'mapped' => false,
                                            'data' => $infosClient->getEmailFacture(),
                                            'attr' => array('class' => 'form-control')
                                        )
                )*/
                ->add('autoliquidation', ChoiceType::class, array(
                                            'required' => true,
                                            'choices' => array('OUI' => '1', 'NON' => '0'),
                                            'mapped' => false,
                                            'data' => $infosClient['autoliquidation'],
                                            'expanded' => true,
                                           // 'multiple' => false,
                                            'label' => 'Autoliquidation',
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('autoliquidationArticle', TextareaType::class, array(
                                            'required' => false,
                                            'label' => 'Artile autoliquidation',
                                            'mapped' => false,
                                            'data' => $infosClient['articleAutoliquidation'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('penaliteRetard', TextareaType::class, array(
                                            'required' => false,
                                            'label' => 'Pénalités de retard',
                                            'mapped' => false,
                                            'data' => $infosClient['articlePenaliteRetard'],                                            
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('idCommercial', EntityType::class, array(
                                            "class" => 'Utilisateurs\UserBundle\Entity\Users',
                                            'query_builder' => function(EntityRepository $er) use($options) {
                                                return $er->createQueryBuilder('u')
                                                        ->where("u.base = '".$options['app_base']."'")
                                                        ->orderBy('u.login', 'ASC');
                                                },
                                            "choice_label"=>"login",
                                            'label' => "Commercial *",
                                            'attr' => array('class' => 'form-control')
                                             )
                )
                ->add('idType', EntityType::class, array(
                                            "class" => 'Crm\ClientBundle\Entity\typeClient',
                                            'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('t')
                                                        ->where('t.active = 1 ')
                                                        ->orderBy('t.id', 'ASC');
                                                },
                                            "choice_label"=>"type",
                                            'label' => "Civilité *",
                                            'attr' => array('class' => 'form-control')
                                             )
                )
                ->add('paysFacturation', EntityType::class, array(
                                            "class" => 'Crm\ClientBundle\Entity\Pays',
                                            'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('p')
                                                        ->orderBy('p.id', 'ASC');
                                                },
                                            "choice_label"=>"Pays",
                                            "mapped" => false,
                                            'label' => "Pays *",
                                            'data' => $infosClient['paysFacturation'],
                                            'attr' => array('class' => 'form-control')
                                             )
                )
                ->add('paysLivraison', EntityType::class, array(
                                            "class" => 'Crm\ClientBundle\Entity\Pays',
                                            'query_builder' => function(EntityRepository $er) {
                                                return $er->createQueryBuilder('p')
                                                        ->orderBy('p.id', 'ASC');
                                                },
                                            "choice_label"=>"Pays",
                                            "mapped" => false,
                                            'label' => "Pays *",
                                            'data' => $infosClient['paysLivraison'],
                                            'attr' => array('class' => 'form-control')
                                             )
                );
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crm\ClientBundle\Entity\Client',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'intention' => 'client_item',
            'app_base' => null,
            'infos_client' => array(),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'crm_clientbundle_client';
    }

    
    /**
     * @return string
     */
    public function getName()
    {
        return 'client';
    }

    


}
