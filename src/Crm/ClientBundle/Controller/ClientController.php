<?php

namespace Crm\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Crm\ClientBundle\Entity\Client;
use Crm\ClientBundle\Entity\infos_client;
use Crm\ClientBundle\Form\ClientType;
use Utilisateurs\UserBundle\Services\AuthUser;
use Tracabilite\TraceBundle\Services\Trace;
use Entities\EntityBundle\Services\ServiceEntities;


class ClientController extends Controller
{
    /**
     * Affiche la page principale du module de gestion des clients.
     *
     */
    public function indexAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');


        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository('CrmClientBundle:Client')->findAllClientByBase($app_base);
        //dump($clients);exit;

        $response = $this->render('@CrmClient/Client/index.html.twig', array(
            'clients' => $clients,
            'classActive' => 'client'
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }


     /**
     * Gère l'ajout et l'édition des client.
     * 
     * <p>
     * <b>addAction()</b> peut être appelé en GET pour afficher le formulaire (création ou modification) ou en POST pour insérer un nouveau user<br />
     * 
     * </p>
     * 
     */

    public function addAction(Request $request, $id = null) {
        
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');
        $idUserConnect = $session->get('user_id');
        $infosClientArr[0] = array();
        $clientInfosId = '';

        $_editMod = 0;
        $clientId = '';
        $em = $this->getDoctrine()->getManager();
        if(isset($id)) { // Edition
            $client = $em->getRepository('CrmClientBundle:Client')->find($id);
            if(!$client) {
                $this->get('session')->set('flashMessageUser', "Client non trouvé !");
            }
            $_editMod = 1;
            $clientId = $id;
            $infosClientArr = $em->getRepository('CrmClientBundle:Client')->findAllClientByBase($app_base, $id);
            //dump($infosClientArr);exit;
            $clientInfosId = $infosClientArr[0]['idClientInfos'];
        } else {
            $client = new Client();
            $infosClientArr[0] = array("idClient" => "","idPersone" => "","idEmail" => "","idFixe" => "","idMobile" => "","numeroDossier" => "","idType" => "","idCommercial" => "","typeClient" => "","nom" => "","prenom" => "","civilite" => "","mail" => "","idTelFixe" => "","idTelMobile" => "","telephoneFixe" => "","telephoneMobile" => "","idAdresseFacturation" => "","idAdresseLivraison" => "","adresseFacturation" => "","adresseLivraison" => "","villeFacturation" => "","villeLivraison" => "","cpFacturation" => "","cpLivraison" => "","paysFacturation" => "","paysLivraison" => "","idClientInfos" => "","ribBancaire" => "","siret" => "","numeroTva" => "","fax" => "","branche" => "","emailFacture" => "","autoliquidation" => "","articleAutoliquidation" => "","articlePenaliteRetard" => "","responsable" => "","nomContact" => "","fonction" => "");
        }

        $form = $this->createForm(ClientType::class, $client, array(
            'app_base' => $app_base,
            'infos_client' => $infosClientArr[0]
        ));
    
        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                  
                $client->setBase($app_base);
                $client->setIdType($form->get("idType")->getData());
                $client->setIdCommercial($form->get("idCommercial")->getData());

                $data = array(
                    'idClient' => $clientId,
                    'idPersone' => $request->get("idPersone"),
                    'idEmail' => $request->get("idEmail"),
                    'idFixe' => $request->get("idFixe"),
                    'idMobile' => $request->get("idMobile"),
                    'idAdresseLivraison' => $request->get("idAdresseLivraison"),
                    'idAdresseFacturation' => $request->get("idAdresseFacturation"),
                    'idType' => $form->get("idType")->getData()->getId(),
                    'civilite' => $form->get("idType")->getData()->getType(),
                    'nom' => $form->get("nom")->getData(),
                    'mail' => $form->get("email")->getData(),
                    'prenom' => $form->get("prenom")->getData(),
                    //'fonction' => $form->get("fonction")->getData(),
                    //'nomResponsable' => $form->get("responsable")->getData(),
                    'mobileClient' => $form->get("mobile")->getData(),
                    'fixeClient' => $form->get("fixe")->getData(),
                    //'faxClient' => $form->get("fax")->getData(),
                    //'siret' => $form->get("siret")->getData(),
                    //'numTva' => $form->get("numTva")->getData(),
                    'adresseLivraison' => $form->get("adresseLivraison")->getData(),
                    'villeLivraison' => $form->get("villeLivraison")->getData(),
                    'cpLivraison' => $form->get("cpLivraison")->getData(),
                    'paysLivraison' => $form->get("paysLivraison")->getData(),
                    'adresseFacturation' => $form->get("adresseFacturation")->getData(),
                    'villeFacturation' => $form->get("villeFacturation")->getData(),
                    'cpFacturation' => $form->get("cpFacturation")->getData(),
                    'paysFacturation' => $request->get("paysFacturation"),
                );

                //dump($data);exit;

                // add persone 
                 $idPersone = $this->container->get('service.entities')->addPersone($data['idPersone'],$data['nom'],$data['prenom'],$data['civilite'],'');
                // add email
                 $idEmail = $this->container->get('service.entities')->addEmail($data['idEmail'],$data['mail'],'Client');
                // add phone fixe
                 $idFixe = $this->container->get('service.entities')->addPhone($data['idFixe'],$data['fixeClient'],'Fixe');
                // add phone mobile
                 $idMobile = $this->container->get('service.entities')->addPhone($data['idMobile'],$data['mobileClient'],'Mobile');

                // add adresse facturation 
                 $idAdresseFacturation = $this->container->get('service.entities')->addAdresse($data['idAdresseFacturation'],$data['adresseLivraison'],$data['villeFacturation'],$data['cpFacturation'],$data['paysFacturation']);
                // add adresse livraison
                $idAdresseLivraison = $this->container->get('service.entities')->addAdresse($data['idAdresseLivraison'],$data['adresseLivraison'],$data['villeLivraison'],$data['cpLivraison'],$data['paysLivraison']);


                $client->setIdPersone($idPersone);
                $client->setIdEmail($idEmail);
                $client->setIdTelFixe($idFixe);
                $client->setIdTelMobile($idMobile);
                $client->setIdAdresseFacturation($idAdresseFacturation);
                $client->setIdAdresseLivraison($idAdresseLivraison);


                $em->persist($client);
                $em->flush();

                $clientId = $client->getId();

                // ajout infos client
                if(isset($id)) { 
                   $infosClientObj = $em->getRepository('CrmClientBundle:infos_client')->find($clientInfosId);
                   //dump($infosClientObj);exit;
                } else {
                   $infosClientObj = new infos_client();
                   $infosClientObj->setIdClient($client);
                }

                $infosClientObj->setSiret($form->get("siret")->getData());
                $infosClientObj->setNumeroTva($form->get("numTva")->getData());

                $infosClientObj->setFonction($form->get("fonction")->getData());
                $infosClientObj->setNomContact($form->get("nomContact")->getData());
                $infosClientObj->setResponsable($form->get("responsable")->getData());
                //$infosClientObj->setEmailFacture($form->get("emailFacture")->getData());
                $infosClientObj->setAutoliquidation($form->get("autoliquidation")->getData());
                $infosClientObj->setArtileAutoliquidation($form->get("autoliquidationArticle")->getData());
                $infosClientObj->setArticlePenalitesRetard($form->get("penaliteRetard")->getData());

                $em->persist($infosClientObj);
                $em->flush();


                $flashMsg = "Client ajouté avec succès "; // Ajout (par défaut)
                if(isset($id)) { // Edition
                    $flashMsg = "Client modifié avec succès !";
                }
                $this->get('session')->set('flashMessageUser', $flashMsg);

                 // Enregistrer trace ajout/modification client
                if(isset($id)) {
                    $this->container->get('tracabilite.trace')->setTrace('Client','Modification client '.$client->getIdPersone()->getNom().' id = '.$clientId);
                } else {
                    $this->container->get('tracabilite.trace')->setTrace('Client','Ajout nouveau client '.$client->getIdPersone()->getNom().' id = '.$clientId);
                }

                return $this->redirect($this->generateUrl('crm_client_homepage'));
            }
        }
    
        return $this->render('@CrmClient/Client/add.html.twig', array(
                'editMod' => $_editMod,
                'clientId' => $clientId,
                'infosClient' => $infosClientArr[0],
                'classActive' => 'client',
                'form' => $form->createView(),
        ));
    }
    /**
     * Supprime Un client/client.
     *
     */
    public function deleteAction($id) {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));


        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('CrmClientBundle:Client')->find($id);
        if(!$client) {
             $this->get('session')->set('flashMessageUser', "Client  non trouvé !");
        }

        try {
           $this->container->get('tracabilite.trace')->setTrace('Client','Suppression de client '.$client->getNom().' id = '.$id);
           $em->getRepository('CrmClientBundle:Client')->deleteInfosClient($id);
            //$em->remove($client);
           $client->setSupprimer(true);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            $this->get('session')->set('flashMessageUser', "Problème suppression de client");
        }
        
        $flashMsg = "Client supprimé avec succès "; 
        $this->get('session')->set('flashMessageUser', $flashMsg);

        return $this->redirect($this->generateUrl('crm_client_homepage'));
    }

    /**
    * Consulter les infos d'un client
    *
    */
    public function infosAction($id) {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));


        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('CrmClientBundle:Client')->find($id);
        $infosClientArr = $em->getRepository('CrmClientBundle:infos_client')->findByidClient($client);

        return $this->render('@CrmClient/Client/infos.html.twig', array(
                'client' => $client,
                'infosClient' => $infosClientArr[0],
                'classActive' => 'client',
        ));
    }

    /**
    * Export CSV des clients
    *
    */
    public function CSVExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');

        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('CrmClientBundle:Client')->findAllClientByBase($app_base);

        $output = "NOM ET PRENOM;CONTACT;EMAIL;TELEPHONE;VILLE;ADRESSE";
        $output .="\r\n";

        $this->container->get('tracabilite.trace')->setTrace('Client','Exporter liste des clients en csv ');

        foreach ($clients as $client) {

            $output .= $client['nom']." ".$client['prenom'].";".$client['nomContact'].";".$client['mail'].";".$client['telephoneMobile']."/".$client['telephoneFixe'].";".$client['villeFacturation'].";".$client['adresseFacturation'];
            $output .="\r\n";
        }


        $output = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $output);
        $path = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/csv/';
        $_docName = 'client-export.csv';
        $_fileName = $path . $_docName;
        $f = fopen ($_fileName,'w');
        fwrite($f, $output);
        fclose($f);
        
        // Telechargement
        if(file_exists($_fileName)) {
            @ob_end_clean();
            @ini_set('zlib.output_compression', '0');
        
            // date courante
            $maintenant=gmdate('D, d M Y H:i:s').' GMT';
        
            // envoi des en-têtes nécessaires au navigateur
            header('Content-Type: application/csv'); // charset=utf-8
            header('Content-Disposition: attachment; filename="'.$_docName.'"');
        
            // Internet Explorer nécessite des en-têtes spécifiques
            if(preg_match('/msie|(microsoft internet explorer)/i', $_SERVER['HTTP_USER_AGENT']))
            {
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
            }
            else header('Pragma: no-cache');
            header('Last-Modified: '.$maintenant);
            header('Expires: '.$maintenant);
            header('Content-Length: '.strlen(file_get_contents($_fileName)));
            readfile($_fileName);
            
            @unlink($_fileName);
        }
        
        $response->setEtag(md5($response->getContent()));
        return $response;
    }


    /**
     * Exporter la liste des clients dans un fichier pdf .
     *
     */
    public function PDFExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('CrmClientBundle:Client')->findAllClientByBase($app_base);

                $html = $this->renderView('@CrmClient/Client/clients_pdf.html.twig', array(
                    'clients' => $clients
                ));
        
        $this->container->get('tracabilite.trace')->setTrace('Client','Exporter liste des clients en pdf ');

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="clients-export.pdf"',
                'orientation'           => 'Landscape'
            )
        );

    }

    /**
     * Supprime des clients en masse on se base sur l'array session des clients sélectionnés.
     *
     */
    public function deleteAllAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $em = $this->getDoctrine()->getManager();
        $session = new Session();
        $_clientsIDs = array();

        if($session->has('affectClientsIDs'))
            $_clientsIDs = $session->get('affectClientsIDs');
        //$session->remove('affectClientsIDs');

        //print_r($_clientsIDs);exit;

        if(count($_clientsIDs) > 0 ) {

            foreach ($_clientsIDs as $key => $client) {
                $client = $em->getRepository('CrmClientBundle:Client')->find($key);
                //dump($client);exit;

                $this->container->get('tracabilite.trace')->setTrace('Client','Suppression client référence = '.$client->getIdPersone()->getNom().' id = '.$key);
                
                $client->setSupprimer(true);
                //$em->remove($client);
                $em->flush();
            }

            $flashMsg = "Clients supprimés avec succès "; 
            $this->get('session')->set('flashMessageUser', $flashMsg);
            return $this->redirect($this->generateUrl('crm_client_homepage'));


        } else {
            $flashMsg = "Merci de sélectionner les clients à supprimer "; 
            $this->get('session')->set('flashMessageUserError', $flashMsg);
            return $this->redirect($this->generateUrl('crm_client_homepage'));
        }
        

        return new Response();
    }


    /**
     * Ajouter a la session( tableau 1 dimenssion) les clients selectionner
     */
    public function checkedAction(Request $request) {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = $request->getSession();
        $session->set('selectAll', '0');

        //$session->remove('affectClientsIDs');

        $_clientIDs = array();

        if($request->isMethod('POST')) {
            $_client_id = $request->request->get('id_client');
            $_checked = $request->request->get('checked');


            if($session->has('affectClientsIDs')) { // la session existe, et il faut ajouter
                $_clientIDs = $session->get('affectClientsIDs');
                    if($_checked == 'true') {
                        $_clientIDs[$_client_id]=$_client_id;
                    } else {
                        if(isset($_clientIDs[$_client_id]))
                            unset($_clientIDs[$_client_id]);
                    }
                $session->set('affectClientsIDs',  $_clientIDs);
            } else {
               //la session n'existe pas et donc aucun ID sauvegardé encore
                $_clientIDs[$_client_id] = $_client_id;
                $session->set('affectClientsIDs',  $_clientIDs);
            }
        }


        dump($_clientIDs);
        exit;

        return new Response();
    }
    
}
