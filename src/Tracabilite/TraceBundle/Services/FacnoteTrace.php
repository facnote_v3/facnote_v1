<?php
namespace Tracabilite\TraceBundle\Services;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\Session\Session;
use Tracabilite\TraceBundle\Entity\Trace;
use Utilisateurs\UserBundle\Entity\Users;

class FacnoteTrace
{   

    private $doctrine;
    private $sessionManager;

    public function __construct(Registry $doctrine, Session $session) {      
        $this->doctrine = $doctrine;
        $this->sessionManager = $session;
    }

    public function setTrace($module,$commentaire)
    {

        $session = new Session();        
        $app_base = $session->get('app_base');
        $userId = $session->get('user_id');
        $em = $this->doctrine->getEntityManager();

        if($app_base == '') $app_base = "FA0001";

        $trace = new Trace();
        $trace->setBase($app_base);
        $userObj = $em->getRepository('UtilisateursUserBundle:Users')->find($userId);
        $trace->setUserId($userObj);
        $trace->setModule($module);
        $trace->setIp($_SERVER['REMOTE_ADDR']);
        $trace->setCommentaire($commentaire);
        $trace->setDate(new \DateTime());

        $em->persist($trace);
        $em->flush();

        return true;

    }
}