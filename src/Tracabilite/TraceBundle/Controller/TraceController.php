<?php

namespace Tracabilite\TraceBundle\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TraceController extends Controller
{

    public function indexAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');


        $em = $this->getDoctrine()->getManager();
        $trace = $em->getRepository('TracabiliteTraceBundle:Trace')->findAllTraceByBase($app_base);

        return  $this->render('@TracabiliteTrace/Trace/index.html.twig', array(
            'trace' => $trace
        ));

    }

    public function PDFExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');
        $em = $this->getDoctrine()->getManager();

        $trace= $em->getRepository('TracabiliteTraceBundle:Trace')->findAllTraceByBase($app_base);

        $html = $this->renderView('@TracabiliteTrace/Trace/trace_pdf.html.twig', array(
            'trace' => $trace
        ));

        $this->container->get('tracabilite.trace')->setTrace('Trace','Export la liste des Tracabilite en pdf ');

         return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="tracabilite-export.pdf"',
                'orientation'           => 'Landscape'
            )
        );

    }
    public function CSVExportAction(){
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');
        $em = $this->getDoctrine()->getManager();

        $trace= $em->getRepository('TracabiliteTraceBundle:Trace')->findAllTraceByBase($app_base);

        $output = "Base;Utilisateur;Module;Commentaire;Ip;Date";
        $output .="\r\n";

        foreach ($trace as $trace) {


            $output .= $trace['base'].";".$trace['utilisateur'].";".$trace['module'].";".$trace['commentaire'].";".$trace['ip'].";".date_format($trace['date'],'string');
            $output .="\r\n";
        }

        $output = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $output);
        $path = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/csv/';
        $_docName = 'Tracabilite-export.csv';
        $_fileName = $path . $_docName;
        $f = fopen ($_fileName,'w');
        fwrite($f, $output);
        fclose($f);

        // Telechargement
        if(file_exists($_fileName)) {
            @ob_end_clean();
            @ini_set('zlib.output_compression', '0');

            // date courante
            $maintenant=gmdate('D, d M Y H:i:s').' GMT';

            // envoi des en-têtes nécessaires au navigateur
            header('Content-Type: application/csv'); // charset=utf-8
            header('Content-Disposition: attachment; filename="'.$_docName.'"');

            // Internet Explorer nécessite des en-têtes spécifiques
            if(preg_match('/msie|(microsoft internet explorer)/i', $_SERVER['HTTP_USER_AGENT']))
            {
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
            }
            else header('Pragma: no-cache');
            header('Last-Modified: '.$maintenant);
            header('Expires: '.$maintenant);
            header('Content-Length: '.strlen(file_get_contents($_fileName)));
            readfile($_fileName);

            @unlink($_fileName);
        }

        $response->setEtag(md5($response->getContent()));
        return $response;
    }
}
