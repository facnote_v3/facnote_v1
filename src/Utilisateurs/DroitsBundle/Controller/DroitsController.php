<?php

namespace Utilisateurs\DroitsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Utilisateurs\UserBundle\Entity\Users;
use Utilisateurs\UserBundle\Form\UsersType;
use Utilisateurs\UserBundle\Services\AuthUser;
use Tracabilite\TraceBundle\Services\Trace;
use Entities\EntityBundle\Services\ServiceEntities;


class DroitsController extends Controller
{
    /**
     * Affiche la page principale du module de gestion droits des utilisateurs.
     *
     */
    public function indexAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');


        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) 
        $app_base = $session->get('app_base_expert');



        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('UtilisateursUserBundle:Users')->findAllUserByBase($app_base);
        //dump($users);exit;
        $dossiers = $em->getRepository('UtilisateursDroitsBundle:DroitsDossiers')->getMyDossiers($app_base);
        //dump($dossiers);exit;

        $DroitsUsers = $em->getRepository('UtilisateursDroitsBundle:DroitsDossiers')->getDroitsUsers($app_base);
        //dump($DroitsUsers);exit;

        $droitsByUser = array();
        foreach ($DroitsUsers as $key => $value) {
        	$droitsByUser[$value['dossierID']][$value['userID']] = $value['active'];        
        }
        //dump($droitsByUser);exit;

        $response = $this->render('@UtilisateursDroits/Droits/dossiers.html.twig', array(
            'users' => $users,
            'dossiers' => $dossiers,
            'droits' => $droitsByUser,
            'classActive' => 'droitDossiers'
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }


    public function checkDroitsUserAction(Request $request)
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $em = $this->getDoctrine()->getManager();

        $app_base = $session->get('app_base_expert');

        $droit = trim($request->request->get('droit'));
		$dossierId = $request->request->get('base');
		$userId = trim($request->request->get('user'));

		$em->getRepository('UtilisateursDroitsBundle:DroitsDossiers')->updateDroitsDossier($app_base,$dossierId,$userId,$droit);

        $response = new Response();
        return $response;
    }

     /**
     * Affiche la page du module de gestion droits des utilisateurs par module.
     *
     */
    public function droitModulesAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');

        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) 
        $app_base = $session->get('app_base_expert');

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('UtilisateursUserBundle:Users')->findAllUserByBase($app_base);
        //dump($users);exit;
        $modules = $em->getRepository('UtilisateursDroitsBundle:Modules')->findAll();
        //dump($modules);exit;

        $DroitsUsers = $em->getRepository('UtilisateursDroitsBundle:DroitsModules')->getDroitsModules($app_base);
        //dump($DroitsUsers);exit;

        $droitsByUser = array();
        foreach ($DroitsUsers as $key => $value) {
        	$droitsByUser[$value['moduleID']][$value['userID']] = $value['active'];        
        }
        
        //dump($droitsByUser);exit;
        
        $response = $this->render('@UtilisateursDroits/Droits/modules.html.twig', array(
            'users' => $users,
            'modules' => $modules,
            'droits' => $droitsByUser,
            'classActive' => 'droitModules'
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }

    public function checkDroitsModulesAction(Request $request)
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $em = $this->getDoctrine()->getManager();

        $app_base = $session->get('app_base');

        $droit = trim($request->request->get('droit'));
		$moduleId = $request->request->get('module');
		$userId = trim($request->request->get('user'));

		$em->getRepository('UtilisateursDroitsBundle:DroitsModules')->updateDroitsModules($moduleId,$userId,$droit);

        $response = new Response();
        return $response;
    }

    /**
     * Affiche la page du module de gestion droits des societes d'un expert par module.
     *
     */
    public function droitDossiersModulesAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $app_base = $session->get('app_base');

        if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) 
        $app_base = $session->get('app_base_expert');

        $em = $this->getDoctrine()->getManager();

        $dossiers = $em->getRepository('UtilisateursDroitsBundle:DroitsDossiers')->getMyDossiers($app_base);
        //dump($dossiers);exit;
        $modules = $em->getRepository('UtilisateursDroitsBundle:Modules')->findAll();
        //dump($modules);exit;

        $DroitsDossiersModules = $em->getRepository('UtilisateursDroitsBundle:DroitsDossiersModules')->getDroitsDossiersModules($app_base);
        //dump($DroitsUsers);exit;

        $droitsByDossiers = array();
        foreach ($DroitsDossiersModules as $key => $value) {
        	$droitsByDossiers[$value['moduleID']][$value['dossierID']] = $value['active'];        
        }
        
        //dump($droitsByUser);exit;
        
        $response = $this->render('@UtilisateursDroits/Droits/dossiers_modules.html.twig', array(
            'dossiers' => $dossiers,
            'modules' => $modules,
            'droits' => $droitsByDossiers,
            'classActive' => 'droitDossiersModules'
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }

    public function checkDroitsDossiersModulesAction(Request $request)
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();
        $em = $this->getDoctrine()->getManager();

        $baseExpert = $session->get('app_base_expert');

        $droit = trim($request->request->get('droit'));
		$moduleId = $request->request->get('module');
		$dossierId = trim($request->request->get('dossier'));

		$em->getRepository('UtilisateursDroitsBundle:DroitsDossiersModules')->updateDroitsDossiersModules($baseExpert, $moduleId,$dossierId,$droit);

        $response = new Response();
        return $response;
    }
    



   
}
