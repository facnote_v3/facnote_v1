<?php

namespace Utilisateurs\DroitsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DroitsDossiers
 *
 * @ORM\Table(name="droits_dossiers")
 * @ORM\Entity(repositoryClass="Utilisateurs\DroitsBundle\Repository\DroitsDossiersRepository")
 */
class DroitsDossiers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="base_expert", type="string", length=20)
     */
    private $baseExpert;

    /**
     * @var \Crm\SocieteBundle\Entity\Societe
     * @ORM\ManyToOne(targetEntity="\Crm\SocieteBundle\Entity\Societe")
     * @ORM\JoinColumn(name="dossier_id", referencedColumnName="id")
     * 
     */
    private $dossierId;


    /**
     * @var \Utilisateurs\UserBundle\Entity\Users
     * @ORM\ManyToOne(targetEntity="\Utilisateurs\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * 
     */
    private $userId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set baseExpert
     *
     * @param string $baseExpert
     *
     * @return DroitsDossiers
     */
    public function setBaseExpert($baseExpert)
    {
        $this->baseExpert = $baseExpert;

        return $this;
    }

    /**
     * Get baseExpert
     *
     * @return string
     */
    public function getBaseExpert()
    {
        return $this->baseExpert;
    }

    /**
     * Set dossierId
     *
     * @param \Crm\SocieteBundle\Entity\Societe
     *
     * @return \Crm\SocieteBundle\Entity\Societe
     */
    public function setDossierId(\Crm\SocieteBundle\Entity\Societe $dossierId = null)
    {
        $this->dossierId = $dossierId;

        return $this;
    }

    /**
     * Get dossierId
     *
     * @return \Crm\SocieteBundle\Entity\Societe
     */
    public function getDossierId()
    {
        return $this->dossierId;
    }

    /**
     * Set userId
     *
     * @param \Utilisateurs\UserBundle\Entity\Users
     *
     * @return \Utilisateurs\UserBundle\Entity\Users
     */
    public function setUserId(\Utilisateurs\UserBundle\Entity\Users $userId = null )
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return  \Utilisateurs\UserBundle\Entity\Users
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return DroitsDossiers
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

}

