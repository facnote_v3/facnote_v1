<?php

namespace Utilisateurs\DroitsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DroitsModules
 *
 * @ORM\Table(name="droits_modules")
 * @ORM\Entity(repositoryClass="Utilisateurs\DroitsBundle\Repository\DroitsModulesRepository")
 */
class DroitsModules
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Utilisateurs\UserBundle\Entity\Users
     * @ORM\ManyToOne(targetEntity="\Utilisateurs\UserBundle\Entity\Users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * 
     */
    private $userId;

    /**
     * @var \Utilisateurs\DroitsBundle\Entity\Modules
     * @ORM\ManyToOne(targetEntity="Modules")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     * 
     */
    private $moduleId;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param  \Utilisateurs\UserBundle\Entity\Users
     *
     * @return  \Utilisateurs\UserBundle\Entity\Users
     */
    public function setUserId( \Utilisateurs\UserBundle\Entity\Users $userId = null )
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return  \Utilisateurs\UserBundle\Entity\Users
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set moduleId
     *
     * @param \Utilisateurs\DroitsBundle\Entity\Modules
     *
     * @return \Utilisateurs\DroitsBundle\Entity\Modules
     */
    public function setModuleId(\Utilisateurs\DroitsBundle\Entity\Modules $moduleId = null)
    {
        $this->moduleId = $moduleId;

        return $this;
    }

    /**
     * Get moduleId
     *
     * @return \Utilisateurs\DroitsBundle\Entity\Modules
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return DroitsModules
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

