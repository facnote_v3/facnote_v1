<?php

namespace Utilisateurs\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="Utilisateurs\UserBundle\Repository\UsersRepository")
 */
class Users 
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=80, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=250, nullable=true)
     */
    private $password;


    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Utilisateurs\UserBundle\Entity\Role
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="roleID", referencedColumnName="id")
     * 
     */
    private $roleid;


    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=10, nullable=false)
     */
    private $base;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dernier_connexion", type="datetime", nullable=true)
     */
    private $dernierConnexion;

    /**
     * @var \Entities\EntityBundle\Entity\EntPersone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntPersone")
     * @ORM\JoinColumn(name="id_persone", referencedColumnName="id")
     * 
     */
    private $idPersone;

    /**
     * @var \Entities\EntityBundle\Entity\EntEmail
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntEmail")
     * @ORM\JoinColumn(name="id_email", referencedColumnName="id")
     * 
     */
    private $idEmail;

    /**
     * @var \Entities\EntityBundle\Entity\EntTelephone
     * @ORM\ManyToOne(targetEntity="\Entities\EntityBundle\Entity\EntTelephone")
     * @ORM\JoinColumn(name="id_telephone", referencedColumnName="id")
     * 
     */
    private $idTelephone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="supprimer", type="boolean", nullable=false)
     */
    private $supprimer;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Users
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Users
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }


    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     *
     * @return Users
     */
    public function setSupprimer($supprimer)
    {
        $this->supprimer = $supprimer;

        return $this;
    }

    /**
     * Get supprimer
     *
     * @return boolean
     */
    public function getSupprimer()
    {
        return $this->supprimer;
    }

     /**
     * Set roleid
     *
     * @param \Utilisateurs\UserBundle\Entity\Role $roleid
     * @return \Utilisateurs\UserBundle\Entity\Role
     */
    public function setRoleid(\Utilisateurs\UserBundle\Entity\Role $roleid = null)
    {
        $this->roleid = $roleid;

        return $this;
    }

    /**
     * Get roleid
     *
     * @return \Utilisateurs\UserBundle\Entity\Role 
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    private $plainPassword;

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function __construct()
    {
        $this->active = true;
        $this->supprimer = false;
        //$this->salt = md5(uniqid(null, true));
    }


    /**
     * Set base
     *
     * @param string $base
     *
     * @return Users
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }


    /**
     * Set dernierConnexion
     *
     * @param \DateTime $dernierConnexion
     *
     * @return Users
     */
    public function setDernierConnexion($dernierConnexion)
    {
        $this->dernierConnexion = $dernierConnexion;

        return $this;
    }

    /**
     * Get dernierConnexion
     *
     * @return \DateTime
     */
    public function getDernierConnexion()
    {
        return $this->dernierConnexion;
    }

    public function __toString() {
        return $this->nom . ' ' . $this->prenom;
    }    


    /**
     * Set idPersone
     *
     * @param \Entities\EntityBundle\Entity\EntPersone $idPersone
     *
     * @return \Entities\EntityBundle\Entity\EntPersone
     */
    public function setIdPersone(\Entities\EntityBundle\Entity\EntPersone $idPersone = null)
    {
        $this->idPersone = $idPersone;

        return $this;
    }

    /**
     * Get idPersone
     *
     * @return \Entities\EntityBundle\Entity\EntPersone
     */
    public function getIdPersone()
    {
        return $this->idPersone;
    }

    /**
     * Set idEmail
     *
     * @param \Entities\EntityBundle\Entity\EntEmail $idEmail
     *
     * @return \Entities\EntityBundle\Entity\EntEmail
     */
    public function setIdEmail(\Entities\EntityBundle\Entity\EntEmail $idEmail=null)
    {
        $this->idEmail = $idEmail;

        return $this;
    }

    /**
     * Get idEmail
     *
     * @return \Entities\EntityBundle\Entity\EntEmail
     */
    public function getIdEmail()
    {
        return $this->idEmail;
    }

    /**
     * Set idTelephone
     *
     * @param \Entities\EntityBundle\Entity\EntTelephone $idTelephone
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function setIdTelephone(\Entities\EntityBundle\Entity\EntTelephone $idTelephone = null)
    {
        $this->idTelephone = $idTelephone;

        return $this;
    }

    /**
     * Get idTelephone
     *
     * @return \Entities\EntityBundle\Entity\EntTelephone
     */
    public function getIdTelephone()
    {
        return $this->idTelephone;
    }

      


}
