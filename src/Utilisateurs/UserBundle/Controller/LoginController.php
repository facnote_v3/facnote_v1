<?php

namespace Utilisateurs\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Utilisateurs\UserBundle\Entity\Users;
use Utilisateurs\UserBundle\Form\UserResetType;
use Tracabilite\TraceBundle\Services\Trace;
use Symfony\Component\HttpFoundation\Cookie;

class LoginController extends Controller
{
	/**
     * Affiche la page de connexion .
     *
     */
    public function indexAction()
    {
        $session = new Session();
        $response = $this->render('@UtilisateursUser/Login/login.html.twig', array());
        $response->setEtag(md5($response->getContent()));
        return $response;
    }


    /**
     * Gérer la connexion des utilisateurs.
     *
     */
    public function loginCheckAction(Request $request)
    {
        
        if($request->isMethod('POST')) {
            $session = new Session();

          $username = $request->get('username');
          $password = $request->get('password');
          
          $url =  $this->generateUrl('login_homepage');
          $em = $this->getDoctrine()->getManager();
          $users = $em->getRepository('UtilisateursUserBundle:Users')->checkConnect($username, $password);
          dump($users);
            if(count($users) ) {
                $nbrCompte = count($users);
                if($nbrCompte == 1 AND $users[0]['roleID'] == '3' ) { // Compte client
                    $userConnect = $users[0];
                    $session->set("userConnect",$userConnect);
                    $session->set('app_base',$session->get('userConnect')['base']);
                    $session->set('user_id',$session->get('userConnect')['id']);
                    $session->set('user_role',$session->get('userConnect')['roleID']);
                    $session->set('raison_sociale',$session->get('userConnect')['raisonSociale']);
                    $userObj = $em->getRepository('UtilisateursUserBundle:Users')->find($session->get('user_id'));
                    //dump($session);exit;
                    $userObj->setDernierConnexion(new \DateTime());
                    $em->persist($userObj);
                    $em->flush();
                    $this->container->get('tracabilite.trace')->setTrace('Connexion','Connexion utilisateur '.$userConnect['email']);

                    if($session->has('app_base_admin')) $session->remove('app_base_admin');
                    if($session->has('app_base_expert')) $session->remove('app_base_expert');

                    // test 1er connexion
                    $connexionsUsers =  $em->getRepository('TracabiliteTraceBundle:Trace')->findBy(array('userId' => $userConnect['id'] ));
                    $nbrConnectionsUsers = count($connexionsUsers);

                    $nbrConnectionsUsers = 1;
                    
                    if($nbrConnectionsUsers > 1) 
                        return $this->redirect($this->generateUrl('utilisateurs_user_homepage'));
                    else 
                    return $this->redirect($this->generateUrl('utilisateurs_user_homepage'));

                } else if($users[0]['roleID'] == '2' ) { 

                    //echo "compte expert ";exit;
                    $baseComptable = $users[0]['base'];
                    $userConnect = $users[0];
                    $session->set("userConnect",$userConnect);
                    $session->set('app_base',$session->get('userConnect')['base']);
                    $session->set('app_base_expert',$baseComptable);
                    $session->set('raison_sociale',$session->get('userConnect')['raisonSociale']);

                    $session->set('user_id',$session->get('userConnect')['id']);
                    $session->set('user_role',$session->get('userConnect')['roleID']);
                    $userObj = $em->getRepository('UtilisateursUserBundle:Users')->find($session->get('user_id'));
                    //dump($session);exit;
                    $userObj->setDernierConnexion(new \DateTime());
                    $em->persist($userObj);
                    $em->flush();

                    // test 1er connexion
                    $connexionsUsers =  $em->getRepository('TracabiliteTraceBundle:Trace')->findBy(array('userId' => $userConnect['id'] ));
                    $nbrConnectionsUsers = count($connexionsUsers);

                     $nbrConnectionsUsers = 1;
                    
                    if($nbrConnectionsUsers == 1) 
                        return $this->redirect($this->generateUrl('crm_societe_homepage'));
                    else
                    return $this->redirect($this->generateUrl('crm_societe_homepage'));


                    // Selection bases de comptable 
                    //$dossiers = $em->getRepository('CrmSocieteBundle:Societe')->findDossiersComptable($baseComptable);
                    //dump($dossiers);exit;
                    //$response = $this->render('@UtilisateursUser/Login/loginExpert.html.twig', array('dossiers' => $dossiers));
                    //$response->setEtag(md5($response->getContent()));
                    //return $response;


                }  else if($users[0]['roleID'] == '1') { 

                   $userConnect = $users[0];
                    $session->set("userConnect",$userConnect);
                    $session->set('app_base',$session->get('userConnect')['base']);
                    
                    if($userConnect['base'] != 'FA0001')
                        return $this->redirect($this->generateUrl('login_homepage'));

                    $session->set('user_id',$session->get('userConnect')['id']);
                    $session->set('user_role',$session->get('userConnect')['roleID']);
                    $session->set('app_base_admin',$session->get('userConnect')['base']);
                    $session->set('raison_sociale',$session->get('userConnect')['raisonSociale']);
                    $userObj = $em->getRepository('UtilisateursUserBundle:Users')->find($session->get('user_id'));
                    //dump($session);exit;
                    $userObj->setDernierConnexion(new \DateTime());
                    $em->persist($userObj);
                    $em->flush();
                    $this->container->get('tracabilite.trace')->setTrace('Connexion','Connexion utilisateur '.$userConnect['email']);

                    // test 1er connexion
                    $connexionsUsers =  $em->getRepository('TracabiliteTraceBundle:Trace')->findBy(array('userId' => $userConnect['id'] ));
                    $nbrConnectionsUsers = count($connexionsUsers);
                    //echo 'nbr connexion = '.$nbrConnectionsUsers;exit;
                     $nbrConnectionsUsers = 1;
                    
                    if($nbrConnectionsUsers == 1) 
                        return $this->redirect($this->generateUrl('utilisateurs_user_homepage'));
                    else
                    return $this->redirect($this->generateUrl('utilisateurs_user_homepage'));
                        
                } else {
                    $this->get('session')->set('flashMessageLoginError', "Nom d'utilisateur ou mot de passe incorrect ");
                    return $this->redirect($this->generateUrl('login_homepage'));
                }
            } else {
               $this->get('session')->set('flashMessageLoginError', "Nom d'utilisateur ou mot de passe incorrect ");
               return $this->redirect($this->generateUrl('login_homepage'));
            }
        }
        $response = $this->render('@UtilisateursUser/Login/login.html.twig', array());
        $response->setEtag(md5($response->getContent()));
        return $response;
    }


    /**
     * Gérer la déconnexion des utilisateurs.
     *
    */
    public function logoutAction(){
        $session = new Session();
        
        $userConnect = $session->get("userConnect");
        $this->container->get('tracabilite.trace')->setTrace('Connexion','Déconnexion utilisateur '.$userConnect['email']);

        $session->remove("userConnect");
        $session->remove("app_base");
        $session->remove("user_id");
        $session->remove("user_role");
        $session->remove("app_base_admin");
        $session->remove("app_base_expert");

        
        return $this->redirect($this->generateUrl('login_homepage'));
    }


    /**
     * 
     * Gère la fonctionnalité "Mot de passe Oublié".
     * 
     */
    public function resetPasswordAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        if($request->isMethod('POST')) {

            $_username=$request->get("user_email");
            $user = $em->getRepository('UtilisateursUserBundle:Users')->findOneBy(array('email'=>$_username));
            if(!$user || $user->getActive() == 0 ) {
                return $this->render('@UtilisateursUser/Login/reset_password.html.twig',
                                     array('error'=>'1'));
            }

            $this->container->get('tracabilite.trace')->setTrace('Connexion','Envoi émail pour la modification de mot de pass '.$_username);

            $message = \Swift_Message::newInstance()
                ->setContentType('text/html')
                ->setCharset('utf-8')
                ->setReplyTo('support@facnote')
                ->setSubject('FacNote : Modification du mot de passe ')
                ->setFrom(array('support@facnote' => 'FacNote'))
                ->setTo($_username)
                ->setBody(
                    $this->renderView(
                        '@UtilisateursUser/Login/reset_password_email.html.twig', array(
                            //'username' => $_username,
                            'resetPath' => $request->getScheme() . '://' . $request->getHttpHost(). $this->generateUrl('login_reset_get', array('username' => $_username)),
                        )
                    )
                )
            ;
            $this->get('mailer')->send($message);

            return $this->render('@UtilisateursUser/Login/reset_password_info.html.twig', array(
                        'username' => $_username,
                        'info'=>1,
                    ));

        } else {
            return $this->render('@UtilisateursUser/Login/reset_password.html.twig');
        }

    }


    /**
     * 
     * Gère la fonctionnalité "Mot de passe Oublié".
     *
     * @param string $username Nom d'utilisateur
     */
    public function getNewPasswordAction(Request $request, $username) {

        $em = $this->getDoctrine()->getManager();

        if(isset($username)){
            $user = $em->getRepository('UtilisateursUserBundle:Users')->findOneBy(array('email'=>$username));
            if(!$user) {
                throw $this->createNotFoundException("utilisateur non trouvé");
            }
        } else {
            $user = new User();
        }

        $form = $this->createForm('Utilisateurs\UserBundle\Form\UserResetType', $user);


        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {

                $user = $em->getRepository('UtilisateursUserBundle:Users')->findOneBy(array('email'=>$username));
                
                if ($plainPassword = $user->getPlainPassword()) {
                    $user->setPassword(md5($plainPassword));
                }

                $this->container->get('tracabilite.trace')->setTrace('Connexion','Modification de mot de pass '.$username);

                $em->persist($user);
                $em->flush();
                $em->clear();

                return $this->render('@UtilisateursUser/Login/reset_password_info.html.twig', array(
                            'username' => $username,
                            'info' => 2,
                        ));
            }
        }

        $response = $this->render('@UtilisateursUser/Login/reset_password_confirm.html.twig', array(
            'form' => $form->createView(),
            'username' => $username
        ));

        return $response;
    }

 

 public function checkAppBaseAction($appBase) {

    $session = new Session();        
    $session->set('app_base',$appBase);
    $em = $this->getDoctrine()->getManager();
    $societe = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($appBase,0,3);
    $raisonSociale = $societe[0]['raisonSociale'];
    $session->set('raison_sociale',$raisonSociale);
    $session->set('user_role',3);

    $this->container->get('tracabilite.trace')->setTrace('Connexion','Connexion expert '.$session->get('userConnect')['email']. ' sur la base client '.$appBase);

    return $this->redirect($this->generateUrl('ecritures_ecritures_general'));

 } 


 public function backAdminAction() {

    $session = new Session();
    $appBase =  $session->get('app_base_admin');  
    $session->set('app_base',$appBase);
    $em = $this->getDoctrine()->getManager();
    $societe = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($appBase,0,3);
    $raisonSociale = $societe[0]['raisonSociale'];
    $session->set('raison_sociale',$raisonSociale);
    $session->set('user_role',1);
    $session->set('app_base_admin',$appBase);

    $this->container->get('tracabilite.trace')->setTrace('Connexion','Connexion admin '.$session->get('userConnect')['email']. ' sur la base client '.$appBase);

    return $this->redirect($this->generateUrl('crm_societe_homepage'));

 }

 public function backExpertAction() {

    $session = new Session(); 
    $appBase =  $session->get('app_base_expert');        
    $session->set('app_base',$appBase);
    $em = $this->getDoctrine()->getManager();
    $societe = $em->getRepository('CrmSocieteBundle:Societe')->findAllSocieteByBase($appBase,0,3);
    $raisonSociale = $societe[0]['raisonSociale'];
    $session->set('raison_sociale',$raisonSociale);
    $session->set('user_role',2);
    $session->set('app_base_expert',$appBase);
    

    $this->container->get('tracabilite.trace')->setTrace('Connexion','Connexion expert '.$session->get('userConnect')['email']. ' sur la base client '.$appBase);

    return $this->redirect($this->generateUrl('crm_societe_homepage'));

 } 



}

?>