<?php

namespace Utilisateurs\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Utilisateurs\UserBundle\Entity\Users;
use Utilisateurs\UserBundle\Form\MonCompteType;
use Utilisateurs\UserBundle\Services\AuthUser;
use Tracabilite\TraceBundle\Services\Trace;


class MonCompteController extends Controller
{
    /**
     * Affiche la page principale du module de gestion des utilisateurs.
     *
     */
    public function indexAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        $id_user = $session->get('user_id');

        //dump($session);exit;


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UtilisateursUserBundle:Users')->find($id_user);
        $infosUser = array(
                            'nomUser' => $user->getIdPersone()->getNom(),
                            'prenomUser' => $user->getIdPersone()->getPrenom(),
                            'telephoneUser' => $user->getIdTelephone()->getNumero(),
                            'EmailUser' => $user->getIdEmail()->getMail(),
                            'idPersone' => $user->getIdPersone()->getId(),
                            'idTelephone' => $user->getIdTelephone()->getId(),
                            'idEmail' => $user->getIdEmail()->getId() 
                            );
        $form = $this->createForm('Utilisateurs\UserBundle\Form\MonCompteType', $user, array(
            'app_base' => $app_base,
            'infos_user' => $infosUser
        ));
        //dump($user);exit;

        // Les 5 derniers activités
        $activites = $em->getRepository('TracabiliteTraceBundle:Trace')->findLastActivites($app_base,$id_user,5);
        //dump($activites);exit;
        $activitesArray = array();
        foreach ($activites as $activite) {
            //print_r($activite);exit; 
            $dateAction = $activite['date'];
            $dateActivite = $dateAction->format('d/m/Y H:i:s');

            $activitesArray[] = $dateActivite." Module <strong> ".$activite['module']."</strong>   ".preg_replace("/id\s(.*)\d$/", "", $activite['commentaire']);

        }
        //dump($activitesArray);exit;

        $response = $this->render('@UtilisateursUser/MonCompte/index.html.twig', array(
            'userId' => $id_user,
            'form' => $form->createView(),
            'classActive' => 'profil',
            'infosUser' => $infosUser,
            'activites' => $activitesArray
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }


     
}
