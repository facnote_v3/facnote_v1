<?php

namespace Utilisateurs\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Utilisateurs\UserBundle\Entity\Users;
use Utilisateurs\UserBundle\Form\UsersType;
use Utilisateurs\UserBundle\Services\AuthUser;
use Tracabilite\TraceBundle\Services\Trace;
use Entities\EntityBundle\Services\ServiceEntities;


class UserController extends Controller
{
    /**
     * Affiche la page principale du module de gestion des utilisateurs.
     *
     */
    public function indexAction()
    {
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');


        //if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');

        //$response->headers->getCookie('');


        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UtilisateursUserBundle:Users')->findAllUserByBase($app_base);

        $response = $this->render('@UtilisateursUser/Users/index.html.twig', array(
            'users' => $users,
            'classActive' => 'users'
        ));

        $response->setEtag(md5($response->getContent()));
        return $response;
    }


     /**
     * Gère l'ajout et l'édition des utilisateurs/users.
     * 
     * <p>
     * <b>addAction()</b> peut être appelé en GET pour afficher le formulaire (création ou modification) ou en POST pour insérer un nouveau user<br />
     * 
     * </p>
     * 
     */

    public function addAction(Request $request, $id = null) {
        
        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        
        //if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');


        $infosUser = array('idPersone'=>'','nomUser'=>'','prenomUser'=>'','telephoneUser'=>'','idTelephone'=>'','EmailUser'=>'','idEmail'=>'');
        $_editMod = 0;
        $userId = '';
        $roleUserId = 3;
        $em = $this->getDoctrine()->getManager();
        if(isset($id)) { // Edition
            $user = $em->getRepository('UtilisateursUserBundle:Users')->find($id);
            if(!$user) {
                $this->get('session')->set('flashMessageUser', "Collaborateur non trouvé !");
            }
            $roleUserId = $user->getRoleid()->getId();
            $_editMod = 1;
            $userId = $id;
            $infosUser = array(
                            'nomUser' => $user->getIdPersone()->getNom(),
                            'prenomUser' => $user->getIdPersone()->getPrenom(),
                            'telephoneUser' => $user->getIdTelephone()->getNumero(),
                            'EmailUser' => $user->getIdEmail()->getMail(),
                            'idPersone' => $user->getIdPersone()->getId(),
                            'idTelephone' => $user->getIdTelephone()->getId(),
                            'idEmail' => $user->getIdEmail()->getId() 
                            );
           // dump($infosUser);exit;
        } else {
            $user = new Users();
        }


        $form = $this->createForm('Utilisateurs\UserBundle\Form\UsersType', $user, array(
            'app_base' => $app_base,
            'infos_user' => $infosUser
        ));


    
        if($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {

                $UserExist = 0;
                $login_user = $form->getData()->getLogin();
                $loginExist = $em->getRepository('UtilisateursUserBundle:Users')->LoginExist($id, $app_base, $login_user);

                if( $loginExist > 0 ) $UserExist = 1;

                if( $UserExist == 1 ) {
                    $this->get('session')->set('flashMessageUserError', "Un compte est déjà enregistré avec cet email ".$login_user);
                    if(isset($id)) {
                        return $this->redirect($this->generateUrl('utilisateurs_user_edit' ,array(
                            'id' => $id)));
                    }
                    else return $this->redirect($this->generateUrl('utilisateurs_user_add'));
                }

                $user->setBase($app_base);
                
                $old_password = $user->getPlainPassword();
                $new_password = $form->getData()->getPlainPassword();

                if( $old_password != md5($new_password) AND strlen($new_password) > 5  ) {
                    $password = md5($new_password);
                    $user->setPassword($password);
                }

                $data = array(
                    'nomUser' =>  $form->get("nom")->getData(),
                    'prenomUser' => $form->get("prenom")->getData(),
                    'telephoneUser' => $form->get("telephonne")->getData(),
                    'idPersone' => $request->get("idPersone"),
                    'idEmail' => $request->get("idEmail"),
                    'idTelephone' => $request->get("idTelephone")
                );
                // add persone 
                 $idPersone = $this->container->get('service.entities')->addPersone($data['idPersone'],$data['nomUser'],$data['prenomUser'],'','');
                // add email
                 $idEmail = $this->container->get('service.entities')->addEmail($data['idEmail'],$login_user,'collaborateur');
                // add telephone
                 $idTelephone = $this->container->get('service.entities')->addPhone($data['idTelephone'],$data['telephoneUser'],'fixe');

                 $user->setIdPersone($idPersone);
                 $user->setIdTelephone($idTelephone);
                 $user->setIdEmail($idEmail);
                

                $em->persist($user);
                $em->flush();

                $userId = $user->getId();
                $flashMsg = "Utilisateur ajouté avec succès "; // Ajout (par défaut)
                if(isset($id)) { // Edition
                    $flashMsg = "Utilisateur modifié avec succès !";
                }
                $this->get('session')->set('flashMessageUser', $flashMsg);

                // Enregistrer trace ajout/modification collaborateurs
                if(isset($id)) {
                    $this->container->get('tracabilite.trace')->setTrace('Collaborateurs','Modification collaborateur '.$user->getLogin().' id = '.$userId);
                } else {
                    $this->container->get('tracabilite.trace')->setTrace('Collaborateurs','Ajout nouveau collaborateur '.$user->getLogin().' id = '.$userId);
                }


                return $this->redirect($this->generateUrl('utilisateurs_user_homepage'));
            }
        }
    
        return $this->render('@UtilisateursUser/Users/add.html.twig', array(
                'editMod' => $_editMod,
                'userId' => $userId,
                'roleUserId' => $roleUserId,
                'form' => $form->createView(),
                'classActive' => 'users',
                'infosUser' => $infosUser,
        ));
    }
    /**
     * Supprime Un utilisateur/users.
     *
     */
    public function deleteAction($id) {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UtilisateursUserBundle:Users')->find($id);
        if(!$user) {
             $this->get('session')->set('flashMessageUser', "Collaborateur non trouvé !");
        }

        try {

            $this->container->get('tracabilite.trace')->setTrace('Collaborateurs','Suppression de collaborateur '.$user->getLogin().' id = '.$id);
            //$em->remove($user);
            $user->setSupprimer(true);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            //return new Response(-1);
            $this->get('session')->set('flashMessageUserError', "Problème de Suppression de collaborateur");
        }
        
        $flashMsg = "Utilisateur supprimé avec succès "; 
        $this->get('session')->set('flashMessageUser', $flashMsg);

        return $this->redirect($this->generateUrl('utilisateurs_user_homepage'));
    }

    /**
    * Export CSV de collaborateurs
    *
    */
    public function CSVExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');

        //if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');


        $em = $this->getDoctrine()->getManager();

        $users = $users = $em->getRepository('UtilisateursUserBundle:Users')->findAllUserByBase($app_base);

        $output = "NOM;PRENOM;EMAIL;TELEPHONE;ROLE;ACTIVE";
        $output .="\r\n";

        foreach ($users as $user) {
            $isActive = "OUI";
            if($user['active'] == 0)$isActive = "NON";
            $output .= $user['nom'].";".$user['prenom'].";".$user['email'].";".$user['telephonne'].";".$user['roleUser'].";".$isActive;
            $output .="\r\n";
        }

        $this->container->get('tracabilite.trace')->setTrace('Collaborateurs','Exporter la liste des collaborateurs en csv ');

        $output = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $output);
        $path = $this->get('kernel')->getRootDir() . '/../web/upload/'.$app_base.'/csv/';
        $_docName = 'collaborateurs-export.csv';
        $_fileName = $path . $_docName;
        $f = fopen ($_fileName,'w');
        fwrite($f, $output);
        fclose($f);
        
        // Telechargement
        if(file_exists($_fileName)) {
            @ob_end_clean();
            @ini_set('zlib.output_compression', '0');
        
            // date courante
            $maintenant=gmdate('D, d M Y H:i:s').' GMT';
        
            // envoi des en-têtes nécessaires au navigateur
            header('Content-Type: application/csv'); // charset=utf-8
            header('Content-Disposition: attachment; filename="'.$_docName.'"');
        
            // Internet Explorer nécessite des en-têtes spécifiques
            if(preg_match('/msie|(microsoft internet explorer)/i', $_SERVER['HTTP_USER_AGENT']))
            {
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
            }
            else header('Pragma: no-cache');
            header('Last-Modified: '.$maintenant);
            header('Expires: '.$maintenant);
            header('Content-Length: '.strlen(file_get_contents($_fileName)));
            readfile($_fileName);
            
            @unlink($_fileName);
        }
        
        $response->setEtag(md5($response->getContent()));
        return $response;
    }


    /**
     * Exporter la liste des collaborateurs dans un fichier pdf .
     *
     */
    public function PDFExportAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));

        $session = new Session();        
        $app_base = $session->get('app_base');
        
        //if($session->get('user_role') == 2 AND $session->has('app_base_expert') ) $app_base = $session->get('app_base_expert');


        $em = $this->getDoctrine()->getManager();

        $users = $users = $em->getRepository('UtilisateursUserBundle:Users')->findAllUserByBase($app_base);

                $html = $this->renderView('@UtilisateursUser/Users/users_pdf.html.twig', array(
                    'users' => $users
                ));

        $this->container->get('tracabilite.trace')->setTrace('Collaborateurs','Exporter la liste des collaborateurs en pdf ');

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="collaborateurs-export.pdf"',
                'orientation'           => 'Landscape'
            )
        );

    }


    public function comingSoonAction() {

        if($this->container->get('utilisateurs_user.auth')->ckeckConnectUser() == 0 )
            return $this->redirect($this->generateUrl('login_homepage'));


        $response = $this->render('@UtilisateursUser/Users/coming_soon.html.twig',array() );

        $response->setEtag(md5($response->getContent()));
        return $response;

    }

   
}
