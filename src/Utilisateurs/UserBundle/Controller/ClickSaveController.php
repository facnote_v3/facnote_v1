<?php

namespace Utilisateurs\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tracabilite\TraceBundle\Services\Trace;


//use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Classe contrôleur permettant l'enregistrement des données  suite à l'événement Click&Save.<br />
 *
 * <p>
 * Lorsque l'utilisateur clique sur une cellule d'une liste (HTML), cette cellule se transforme en zone de saisie pour entrer du texte.
 * </p>
 * 
 * 
 */
class ClickSaveController extends Controller
{
	/**
     * 
     * Enregistre une donnée saisie suite à l'événement Click & Save.
     * 
     * <p>
     * <b>clickSaveAction</b> est appelée uniquement en AJAX:<br />
     * Lorsque l'utilisateur clique sur une cellule d'une liste (HTML), cette cellule se transforme en zone de saisie pour entrer du texte.
     * </p>
     * 
     * @return \Symfony\Component\HttpFoundation\Response contenant la réponse (Succès, Erreur, ou un code particulier en fonction de la zone modifiée)
     * 
     */
	public function clickSaveAction(Request $request) {
		//$request = $this->getRequest();
		$_entity = $request->request->get('entity');
		$_key = $request->request->get('key');
		$_field = trim($request->request->get('field'));
		$_id = $request->request->get('id');
		$_value = trim($request->request->get('value'));

		// history
		$_label = $request->request->get('label');
		$_fieldType = $request->request->get('fieldType');
		//if($_value == '') $_value = null;

		//$_errorCode = 0;
		$_returnData = array();
		$_returnData['errCode'] = 0;
		$_returnData['data'] = '';
		if($_entity != '' && $_key != '' && $_field != '') {
			$em = $this->getDoctrine()->getManager();
			$conn = $this->get('database_connection');

			$_currentChassisVal = '';

        	$sql = 'UPDATE `'.$_entity.'` SET '.$_field.' = ? WHERE '.$_key.' = ?';

        	$this->container->get('tracabilite.trace')->setTrace(ucfirst($_entity),'Modification '.$_entity.' champs = '.$_field.' id = '.$_id);
        	
        	try {
        		$stmt = $conn->prepare($sql);
        		$stmt->bindValue(1, $_value);
        		$stmt->bindValue(2, $_id);
        		//echo $sql;exit;
				$stmt->execute();

				// table produit 
	        	if($_entity == 'produit' AND ( $_field == 'prixHt' OR $_field == 'tauxTva' )  ) {
	        		// ttc a partir de ht :  [Montant HT] x (1 + ([Taux TVA] / 100))=[Montant TTC]
	        		$sql = 'UPDATE produit SET  prixTtc = prixHt  * (1 + (tauxTva / 100 ) )  WHERE id = ? ';
	        		$stmt = $conn->prepare($sql);
	        		$stmt->bindValue(1, $_id);
	        		$stmt->execute();
	        	}


			} catch (\Doctrine\DBAL\DBALException $e) {
            	$_returnData['errCode'] = 1;
        	}

		}

        return new Response(json_encode($_returnData));
	}

	public function clickSaveSelectAction(Request $request) {

		$_tableSelect = $request->request->get('tableSelect');
		$_entity = $request->request->get('entity');
		$_value = $request->request->get('value');
		$_field = $request->request->get('field');
		$_fieldSelect = $request->request->get('selectField');
		$_key = $request->request->get('key');
		$_objID = $request->request->get('objID');

		//echo "field = ".$_field." fieldSelect:".$_fieldSelect;exit;
		$elements = array();

		if($_tableSelect != '' && $_value != '' && $_field != '' && $_fieldSelect != '' && $_tableSelect != 'taux_tva' && $_tableSelect != 'unite'   ) {
            
			$em = $this->getDoctrine()->getManager();
			$conn = $this->get('database_connection');
			$sql = ' SELECT id,'.$_fieldSelect.' as value FROM `'.$_tableSelect.'`  ';
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			$elements=$stmt->fetchAll();

			
		}

		$response = $this->render('@UtilisateursUser/ClickSave/select.html.twig', array(
	            'elements' => $elements,
	            'nameSelect' => $_field,
	            'tableSelect' => $_tableSelect,
	            'entity' => $_entity,
	            'field' => $_field,
	            'key' => $_key,
	            'objID' => $_objID,
	            'fieldLabel' => '',
	        ));

        $response->setEtag(md5($response->getContent()));
        return $response;




	}

}