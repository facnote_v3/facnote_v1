<?php

namespace Utilisateurs\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class UserResetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les mots de passe ne correspondent pas',
                    'options' => array('attr' => array('class' => 'form-control form-control-rounded')),
                    'required' => true,
                    'first_options'  => array('attr' => array('placeholder' => 'Nouveau mot de passe', 'class' => 'form-control form-control-rounded')),
                    'second_options' => array('attr' => array('placeholder' => 'Confirmation mot de passe', 'class' => 'form-control form-control-rounded')),
                )
            );    
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Utilisateurs\UserBundle\Entity\Users',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'intention' => 'userReset_item',

        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'utilisateurs_userbundle_users';
    }

    
    /**
     * @return string
     */
    public function getName()
    {
        return 'users';
    }


}
