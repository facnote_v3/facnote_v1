<?php

namespace Utilisateurs\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MonCompteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $infosUser = $options['infos_user'];

         $builder->add('nom', TextType::class, array(
                                            'required' => true,
                                            'label' => 'Nom *',
                                            'mapped' => false,
                                            'data' => $infosUser['nomUser'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
               ->add('prenom', TextType::class, array(
                                            'required' => true,
                                            'label' => 'Prénom *',
                                            'mapped' => false,
                                            'data' => $infosUser['prenomUser'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('telephonne', TextType::class, array(
                                            'required' => false,
                                            'label' => 'Téléphone',
                                            'mapped' => false,
                                            'data' => $infosUser['telephoneUser'],
                                            'attr' => array('class' => 'form-control')
                                        )
                )
                ->add('login', TextType::class, array(
                                                'required' => true,
                                                'label' => 'Login ( Email )*',
                                                'attr' => array('class' => 'form-control')
                                            )
                )
                ->add('plainPassword', RepeatedType::class, array(
                                                'type' => PasswordType::class,
                                                'invalid_message' => ' The password fields must match.',
                                                'options' => array('attr' => array('class' => 'form-control password-field')),
                                                'required' => false,
                                                'first_options'  => array('label' => 'Mot de passe *'),
                                                'second_options' => array('label' => 'Mot de passe (confirmation) *'),
                    )
                );
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Utilisateurs\UserBundle\Entity\Users',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'intention' => 'user_item',
            'app_base' => null,
            'infos_user' => array(),

        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'utilisateurs_userbundle_users';
    }

    
    /**
     * @return string
     */
    public function getName()
    {
        return 'users';
    }


}
