<?php
namespace Utilisateurs\UserBundle\Services;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthUser
{   
    /**
     * Vérifier la connexion d'un utilisateur      
     * Return 0 si l'utilisateur n'est connecté ou la session de connexion est vide
     * Return 1 si l'user est connecté 
    */
    public function ckeckConnectUser()
    {
        $session = new Session();
        $userConnect = 1;
        if(! $session->has('userConnect')) {
            $userConnect = 0;
        } else {
            // Vérifier que la base de l'user connecté et une vrai base facnote
            if(! preg_match("/^FA\d\d\d\d$/", $session->get('app_base') ) ) {
                $userConnect = 0;
            }
        }
        return $userConnect;
    }
}