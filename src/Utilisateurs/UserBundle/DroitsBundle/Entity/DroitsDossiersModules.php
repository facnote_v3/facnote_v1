<?php

namespace Utilisateurs\DroitsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DroitsDossiersModules
 *
 * @ORM\Table(name="droits_dossiers_modules")
 * @ORM\Entity(repositoryClass="Utilisateurs\DroitsBundle\Repository\DroitsDossiersModulesRepository")
 */
class DroitsDossiersModules
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="base_expert", type="string", length=255)
     */
    private $baseExpert;

    /**
     * @var \Crm\SocieteBundle\Entity\Societe
     * @ORM\ManyToOne(targetEntity="\Crm\SocieteBundle\Entity\Societe")
     * @ORM\JoinColumn(name="dossier_id", referencedColumnName="id")
     * 
     */
    private $dossierId;

    /**
     * @var \Utilisateurs\DroitsBundle\Entity\Modules
     * @ORM\ManyToOne(targetEntity="Modules")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     * 
     */
    private $moduleId;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set baseExpert
     *
     * @param string $baseExpert
     *
     * @return DroitsDossiersModules
     */
    public function setBaseExpert($baseExpert)
    {
        $this->baseExpert = $baseExpert;

        return $this;
    }

    /**
     * Get baseExpert
     *
     * @return string
     */
    public function getBaseExpert()
    {
        return $this->baseExpert;
    }

    /**
     * Set dossierId
     *
     * @param \Crm\SocieteBundle\Entity\Societe
     *
     * @return \Crm\SocieteBundle\Entity\Societe
     */
    public function setDossierId(\Crm\SocieteBundle\Entity\Societe $dossierId = null)
    {
        $this->dossierId = $dossierId;

        return $this;
    }

    /**
     * Get dossierId
     *
     * @return \Crm\SocieteBundle\Entity\Societe
     */
    public function getDossierId()
    {
        return $this->dossierId;
    }

    /**
     * Set moduleId
     *
     * @param \Utilisateurs\DroitsBundle\Entity\Modules
     *
     * @return \Utilisateurs\DroitsBundle\Entity\Modules
     */
    public function setModuleId(\Utilisateurs\DroitsBundle\Entity\Modules $moduleId = null)
    {
        $this->moduleId = $moduleId;

        return $this;
    }

    /**
     * Get moduleId
     *
     * @return \Utilisateurs\DroitsBundle\Entity\Modules
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return DroitsDossiersModules
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

