<?php

namespace Utilisateurs\DroitsBundle\Repository;

/**
 * DroitsModulesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DroitsModulesRepository extends \Doctrine\ORM\EntityRepository
{
	public function getDroitsModules($baseApp) {

		$em = $this->getEntityManager();
        $dq = $em->createQueryBuilder('d');
        $dq->select('d.id,u.base,IDENTITY(d.moduleId) as moduleID,IDENTITY(d.userId) as userID, d.active')
            ->from('UtilisateursDroitsBundle:DroitsModules', 'd')
            ->join('d.userId','u')
            ->where('u.base = :baseApp')
            ->groupBy('d.id')
            ->orderBy('d.id', 'ASC')
            ->setParameter('baseApp', $baseApp);
        $query = $dq->getQuery();
        //echo $query->getSQL();exit;
        return $query->getResult();
	}

	public function updateDroitsModules($moduleId,$userId,$droit) {

		$con = $this->getEntityManager()->getConnection();
        $active = 0;
        if($droit == 'true' ) $active = 1;
        if($moduleId > 0 ) {
            $sqlDroits = "SELECT active FROM droits_modules WHERE  module_id = '".$moduleId."' AND user_id = '".$userId."' ";
            //echo $sqlDroits;
            $stmt = $con->executeQuery($sqlDroits);
            $resultDroits = $stmt->fetchAll();
            if(isset($resultDroits[0]['active']) ) {
                $sql = "UPDATE droits_modules SET active = '".$active."' WHERE module_id = '".$moduleId."' AND user_id = '".$userId."' ";
            } else {
                $sql = "INSERT INTO droits_modules ( module_id, user_id, active) VALUES ( '".$moduleId."', '".$userId."', '".$active."');  ";
            }
            //echo $sql;
            $stmt = $con->prepare($sql);
            $stmt->execute();
            return true;
        } else {
            if($moduleId == 'all') {

                $sqlDelete = "DELETE FROM droits_modules WHERE user_id = '".$userId."' ";
                //echo $sqlDelete.'<br>';
                $stmt = $con->prepare($sqlDelete);
                $stmt->execute();
                $sql_insert = "INSERT INTO droits_modules (module_id, user_id, active) (SELECT id as module_id, '".$userId."' as user_id, '".$active."' as active FROM modules )";
                //echo $sql_insert.'<br>';
                $stmt = $con->prepare($sql_insert);
                $stmt->execute();

            }
        }

	}
}
